type t

(** Performs replacement, selecting which individuals from the list of parents
 *  and offspring should be chosen to form the membership, of a given size, of
 *  the next population. Returns a pair, containing a list of surviving
 *  individuals, and a list of a discarded individuals. *)
val replace : t -> Solution.t list -> Solution.t list -> (Solution.t list * Solution.t list)

(** Given a list of parents, and a list of offspring, this method returns a
 *  list containing the individuals who are candidates to be included in the
 *  next population; this is used to restrict evaluation to only those
 *  individuals whose test case results are needed *)
val candidates : t -> Solution.t list -> Solution.t list -> Solution.t list

(** Produces a descriptor for the given selection method *)
val descriptor : t -> string

(** Constructs a search algorithm from a JSON definition *)
val from_json : int -> Yojson.Basic.json -> t

(** Replaces population with its offspring *)
(*
module Generational : sig
  type t
  val make : unit -> t
  val from_json : Yojson.Basic.json -> int -> 
end
*)
