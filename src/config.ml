open Yojson

(** Contains program-wide configuration information. *)
type configuration = {
  (** The directory that this configuration file belongs to. *)
  mutable cfg_dir           : string;

  (** The random seed used for this run. *)
  mutable random_seed       : int; 

  (** A flag indicating whether the program under repair is Valgrind.
   * Necessary to deal with all of Valgrind's querks. *)
  mutable is_valgrind       : bool;

  (* TESTING OPTIONS *)
  (** The name of the script resonsible for executing the test suite. *)
  mutable test_script           : string;
  mutable test_command          : string;
}

type t = configuration

(** Load configuration file settings from a JSON description. *)
let from_json fdir def =
  let open Yojson.Basic.Util in

  (* Random seed *)
  let seed = match def |> member "seed" with
    | `Int s -> s
    | _ -> int_of_float (Unix.time ())
  in

  (* Test settings. *)
  let test_script = match def |> member "test_script" with
    | `String s -> s
    | _ -> Filename.concat fdir "test.sh"
  in
  let test_command = match def |> member "test_command" with
    | `String s -> s
    | _ ->  "__TEST_SCRIPT__ __EXE_NAME__ __TEST_NAME__ " ^
            "__PORT__ __SOURCE_NAME__ 1>/dev/null 2>/dev/null"
  in

  (* Valgrind *)
  let is_valgrind = match def |> member "is_valgrind" with
    | `Bool b -> b
    | _ -> false
  in

  (* Put everything together! *)
    { cfg_dir = fdir;
      random_seed = seed;
      test_script = test_script;
      test_command = test_command;
      is_valgrind = is_valgrind;
    }

(** Initialises GenProg using the provided configuration *)
let initialise (cfg : configuration) : unit =
  Random.init cfg.random_seed

let mock () =
  {
    cfg_dir = "";
    random_seed = 0;
    is_valgrind = false;
    test_script = "";
    test_command = ""
  }

let dir cfg = cfg.cfg_dir
let is_valgrind cfg = cfg.is_valgrind
let seed cfg = cfg.random_seed
