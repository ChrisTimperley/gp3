open Solution

(** Represents the different ways in which the search may terminate *)
type t = 
  | Unexpected of string
  | ReachedGenerationLimit
  | ReachedEvaluationLimit
  | ReachedUniqueVisitLimit
  | ExhaustedEdits
  | ReachedTimeLimit
  | ReachedIterationLimit
  | FoundAcceptableSolution of solution

(** Converts a termination event into a human-readable string *)
let to_s termination = match termination with
  | Unexpected(msg) -> Printf.sprintf "Unexpected error occurred: %s" msg
  | ReachedIterationLimit -> "Maximium number of iterations reached"
  | ReachedUniqueVisitLimit -> "Maximium number of unique visits reached"
  | ReachedTimeLimit -> "Maximum wall-clock time reached"
  | ExhaustedEdits -> "Exhausted all edits within search space"
  | ReachedGenerationLimit -> "Maximum number of generations reached"
  | ReachedEvaluationLimit -> "Maximum number of evaluations reached"
  | FoundAcceptableSolution(s) ->
     Printf.sprintf "Acceptable solution found: %s" (describe_genome s)

(** Converts a termination event into a JSON document *)
let to_json termination =
  let open Yojson.Basic in
  match termination with
  | Unexpected(msg) ->
      `Assoc([("type", `String("unexpected")); ("message", `String(msg))])
  | ExhaustedEdits ->
      `Assoc([("type", `String("exhausted_edits"))])
  | ReachedUniqueVisitLimit ->
      `Assoc([("type", `String("reached_unique_visit_limit"))])
  | ReachedTimeLimit ->
      `Assoc([("type", `String("reached_time_limit"))])
  | ReachedTimeLimit ->
      `Assoc([("type", `String("reached_time_limit"))])
  | ReachedGenerationLimit ->
      `Assoc([("type", `String("reached_generation_limit"))])
  | ReachedEvaluationLimit ->
      `Assoc([("type", `String("reached_evaluation_limit"))])
  | FoundAcceptableSolution(s) ->
      `Assoc([
        ("type", `String("found_acceptable_solution"));
        ("solution", `String(describe_genome s))
      ])
