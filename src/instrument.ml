(**
 *
 *
 * TODO:
 * - Integrate globinit.
 * - line 675
 *)

open Cil
open Utility
open Config
open Toposort
open Printf
open Test
open Debug

(** Used to store the coverage information for a single test case.
 *  Represented by a map from program SIDs to number of visits.
 *  In the case that only unique visits are counted, a mapping to
 *  one indicates a statement was visited, and a mapping to zero
 *  indicates it was not. *)
type test_case_coverage = int IntMap.t

(** Used to store the coverage information for an entire test suite. *)
type test_suite_coverage = (test_case * test_case_coverage) list

(* These are CIL variables describing C standard library functions like
 * 'fprintf'. We use them when we are instrumenting the file to
 * print out statement coverage information for fault localization. *)  
(**/**)
let void_t = Formatcil.cType "void *" [] 

(* For most functions, we would like to use the prototypes as defined in the
   standard library used for this compiler. We do this by preprocessing a simple
   C file and extracting the prototypes from there. Since this should only
   happen when actually computing fault localization, the prototypes are lazily
   cached in the va_table. fill_va_table is called by the coverage visitor to
   fill the cache (if necessary) and retrieve the cstmt function along with the
   list of function declarations. *) 

(**
 * TODO: Hide this from the rest of the program. We should only be able to use
 * accessors.
 *)

let va_table = Hashtbl.create 10

(** TODO: This needs a different name. It does much more than just filling the
 *  VA table. *)
let fill_va_table = ref 
  ((fun () -> failwith "fill_va_table uninitialized") : unit -> 
  (string -> Cil.location -> (StringMap.key * Cil.formatArg) list -> Cil.stmt) *
           Cil.global list StringMap.t)

let uniq_array_va = ref
  (makeGlobalVar "___coverage_array" (Formatcil.cType "char *" []))
let uniq_int_va = ref
  (makeGlobalVar "___coverage_array_already_memset" (Cil.intType))

(** A list of functions which should not be instrumented. *)
let do_not_instrument_these_functions =
  [ "fflush"; "memset"; "fprintf"; "fopen"; "fclose"; "vgPlain_fmsg";
    "vgPlain_memset" ]

(**
 * Returns a string representation of some provided coverage information.
 *)
let describe_test_case_coverage (coverage : test_case_coverage) : string =
  let flattened = IntMap.fold (fun sid freq flattened ->
    let desc = Printf.sprintf "%d: %d" sid freq in
      desc::flattened
  ) coverage [] in
    String.concat "\n" (List.rev flattened)

(**
 * Returns a string representation of some provided coverage information.
 *)
let describe_test_suite_coverage (coverage : test_suite_coverage) : string =
  let flattened = List.map (fun cov ->
    let test_case, test_case_coverage = cov in
      Printf.sprintf "Coverage (%s):\n%s"
        (test_case_name test_case)
        (describe_test_case_coverage test_case_coverage)
  ) coverage in
    String.concat "\n\n" flattened

(**
 * Calculates the name of the coverage file for a given test.
 *)
let coverage_file_name (test_case : test_case) : string =
  Printf.sprintf "%s.cov" (test_case_name test_case)

(**
 * Visitor for computing statement coverage (for a "weighted path"); walks over
 * the C program AST and modifies it so that each statement is preceeded by a
 * 'printf' that writes that statement's number to the .path file at run-time.
 * Determines prototypes for instrumentation functions (e.g. fprintf) that are
 * not present in the original code.
 * 
 * FIXME: multithreaded and uniq coverage are not going to play nicely here in
   terms of memset.

 * @param variant rep the cilrep object being visited; used to preprocess headers
 * @param prototypes mapref StringMap to fill with missing prototypes
 * @param coverage_outname path filename
 * @param found_fmsg valgrind-specific boolean reference
*) 

class covVisitor
  (cfg        : Config.t)
  (program    : Program.t)
   prototypes
  (cov_fn     : string)
  (found_fmsg : bool ref)
  (multi_threaded : bool)
  (unique     : bool)
= 
object
  inherit nopCilVisitor

  (* Inject found_fsmg into here? *)

  val mutable declared = false

  (** Used to create a statement.
   *  This definitely should not be in here. *)
  val cstmt = fst (!fill_va_table ())

  (**
   * Replaces CIL-provided prototypes (which are probably wrong) with the
   * ones we extracted from the system headers; but keep user-provided
   * prototypes, since the user may have had a reason for specifying them.
   *
   * TODO: Put this into a separate function.
   **)

  method vglob (g : Cil.global) =

    (** Checks if a named variable of a given has a missing prototype. *)
    let missing_proto (name : string) (vtyp : Cil.typ) : bool =
      match vtyp with
      | TFun(_, _, _, tattrs) ->
        List.exists (fun tattr ->
          match tattr with
          | Attr("missingproto", _) -> true
          | _ -> false
        ) tattrs
      | _ -> false
    in

      (** Extract and store all the prototypes from the program on the first
       *  invocation of this method. *)
      if not declared then begin
        declared <- true;
        prototypes := snd (!fill_va_table ());
      end;

      (** Replace prototypes. *)
      match g with
      | GVarDecl(vi, _) when (StringMap.mem vi.vname !prototypes) ->
        if missing_proto vi.vname vi.vtype then begin
          ChangeDoChildrenPost([], fun gs -> gs)
        end else begin
          prototypes := StringMap.remove vi.vname !prototypes;
          ChangeDoChildrenPost([g], fun gs -> gs)
        end
      | _ -> ChangeDoChildrenPost([g], fun gs -> gs)

  method vblock b =
    ChangeDoChildrenPost(b, (fun b ->
      let result = List.map (fun stmt ->

        (** Check this statement has a valid SID. **)
        if stmt.sid > 0 then begin
          let str = 
            if cfg.is_valgrind then 
              sprintf "FMSG: (%d)\n" stmt.sid 
            else
              sprintf "%d\n" stmt.sid
          in
          let print_str = 
            if cfg.is_valgrind then 
              "vgPlain_fmsg(%g:str);"
            else 
              "fprintf(fout, %g:str);\nfflush(fout);\n"
          in

          (* Wrap the coverage statement in a uniqueness checking statement if
           * unique coverage is enabled. *)
          let print_str = 
            if unique then 
              "if ( uniq_array[%d:index] == 0 ) {\n" ^
                print_str^
                "uniq_array[%d:index] = 1; }\n"
            else
              print_str
          in

          (* If multi-threading is enabled, have the program use locking when
           * accessing the coverage file. *)
          let print_str = 
            if multi_threaded then 
              "fout = fopen(%g:fout_g, %g:a_arg);\n"^print_str^"fclose(fout);\n"
            else 
              print_str 
          in

          (* Put together the coverage statement for this given statement and
           * place it before the original. *)
          let newstmt = cstmt print_str !currentLoc
            [
              ("uniq_array",  Fv(!uniq_array_va));
              ("fout_g",      Fg(cov_fn));
              ("index",       Fd(stmt.sid));
              ("str",         Fg(str))
            ]
          in
            [ newstmt ; stmt ]

        (* If the statement has no valid SID, then simply leave it be. *)
        end else [stmt]
      ) b.bstmts in

      (* Flatten the results into a list of instrumented statements for this
       * block before replacing the body of the block with them. *)
      let block = 
        { b with bstmts = List.flatten result } 
      in
        block
    ))

  (** If valgrind is being repaired, check if the given variable declaration is
   *  the vgPlain_fsmg variable. *)
  method vvdec vinfo = 
    if vinfo.vname = "vgPlain_fmsg" then
      found_fmsg := true;
    DoChildren

  (**
   * Checks if a given function should be instrumented, and proceeds to do so
   * if that is the case.
   *)
  method vfunc (f : Cil.fundec) =

    (* If we're repairing Valgrind, check that we haven't found the vgPlain_msg
     * variable before trying to instrument this function. *)
    if !found_fmsg then begin

      (* Check if the function shouldn't be instrumented. *) 
      if List.mem f.svar.vname do_not_instrument_these_functions then begin
        debug "cilRep: WARNING: definition of fprintf found at %s:%d\n"
          f.svar.vdecl.file f.svar.vdecl.line;
        debug "\tcannot instrument for coverage (would be recursive)\n";
        SkipChildren

      (* MULTI-THREADING. *)
      end else if not multi_threaded then begin
        let uniq_instrs =
          if unique then
            let memset_str =
              if cfg.is_valgrind then
                "vgPlain_memset(uniq_array, 0, sizeof(uniq_array))"
              else
                "memset(uniq_array, 0, sizeof(uniq_array))"
            in
            Printf.sprintf "if(uniq_int == 0) {\n uniq_int = 1;\n %s;\n}\n" memset_str
          else ""
        in
        let stmt_str =
          if cfg.is_valgrind then
            uniq_instrs
          else 
            "if (fout == 0) {\n fout = fopen(%g:fout_g,%g:wb_arg);\n" ^ uniq_instrs^"}"
        in
        let ifstmt = cstmt stmt_str !currentLoc
          [("uniq_array", Fv(!uniq_array_va));("fout_g",Fg cov_fn); ("uniq_int", Fv(!uniq_int_va))]
        in
          ChangeDoChildrenPost(f,
            (fun f -> f.sbody.bstmts <- ifstmt :: f.sbody.bstmts; f))

      (* If all the function can be instrumented, proceed to instrument its
       * children. *)
      end else DoChildren

    (* Otherwise skip the instrumentation of this function. *)
    end else SkipChildren
end 

(**
 * Non-destructively instruments a single CIL program file, so that its
 * coverage information can be extracted and recorded to a specified coverage
 * file.
 *
 * @param cfg         The current configuration.
 * @param program     The (unmodified) program the file belongs to.
 * @param file        The program file that is to be instrumented.
 * @param num_atoms   The number of atoms in the program this file belongs to.
 * @param cov_fn      The (complete) name of the output coverage file.
 *
 * @return  An instrumented form of the given program file.
 *)
let instrument_file
  ?g:(globinit=false)
  (cfg        : Config.t)
  (program    : Program.t)
  (file       : Program.File.t)
  (num_atoms  : int)
  (cov_fn     : string)
  (multi_threaded : bool)
  (unique     : bool)
: Program.File.t =
  (**
   * TODO: Handle [stmt_count] variable.
   *)
  let open Cil in
  let file = Program.File.copy file in

  (* If unique coverage is enabled then create the global coverage array. *)
  let uniq_globals = 
    if unique then begin
      let array_typ = 
        Formatcil.cType "char[%d:siz]" [("siz", Fd(num_atoms + 1))] 
      in
      let init_zero_info = {init = Some(makeZeroInit Cil.intType)} in
        uniq_array_va := makeGlobalVar "___coverage_array" array_typ;
        uniq_int_va := makeGlobalVar "___coverage_array_already_memset" Cil.intType;
        [ GVarDecl(!uniq_array_va, !currentLoc); 
          GVarDecl(!uniq_int_va, !currentLoc); 
          GVar(!uniq_int_va, init_zero_info, !currentLoc) ]
    end else []
  in
 
  (* Add global coverage array to this file if it hasn't been added to any
   * others yet *) 
  let new_globals = 
    if not globinit then
      List.rev 
        (List.fold_left
           (fun acc glob ->
             match glob with
               GVarDecl(va, loc) -> GVarDecl({va with vstorage = Extern}, loc) :: acc
             | _ -> acc
           ) [] uniq_globals)
    else uniq_globals
  in
  let cil_file = Program.File.as_cil_file file in
    cil_file.globals <- new_globals @ cil_file.globals;

  (** Prepend missing prototypes to instrumentation code. *)
  let prototypes = ref StringMap.empty in
  let cov_visit = if cfg.is_valgrind then
      new covVisitor cfg program prototypes cov_fn (ref false) multi_threaded unique
    else 
      new covVisitor cfg program prototypes cov_fn (ref true) multi_threaded unique
  in
    Program.File.visit file cov_visit;

    (* TODO: Separate this from the instrumentation code. *)
    (* As long as the program under repair isn't valgrind, inject each of the
     * missing prototypes into the program before using Toposort to correct
     * the order of the globals within the program. *)
    if not cfg.is_valgrind then begin
      cil_file.globals <-
        StringMap.fold (fun _ protos accum ->
          protos @ accum
        ) !prototypes cil_file.globals;
      begin
        try
          cil_file.globals <- toposort_globals cil_file.globals;
        with MissingDefinition(s) ->
          let error_msg = sprintf "cilRep: toposorting failure (%s)!\n" s in
            failwith error_msg
      end
    end;

    (** Return the fully instrumented file. *)
    file

(**
 * Non-destructively instruments a given CIL program to allow its coverage
 * information to be computed for purposes of fault localisation.
 *)
let instrument cfg program cov_fn multi_threaded unique =
  let num_stmts = Program.size program in
  let init_globals = ref true in
  let files = List.map (fun f ->
    let f =
      instrument_file ~g:!init_globals cfg program f num_stmts cov_fn multi_threaded unique
    in
      init_globals := false;
      f
    ) (Program.files program)
  in
    Program.with_files program files

(** Used to initialise the VA table; should be called on start-up. *)
let setup_va_table_function cfg pp : unit =
  fill_va_table := (fun () ->

  (** A list of the names of functions used for coverage that we wish to
   *  acquire the prototypes for. *) 
  let vnames =
    [ "fclose"; "fflush"; "fopen"; "fprintf"; "memset"; "vgPlain_fmsg";
      "_coverage_fout" ; "vgPlain_memset" ]
  in

  (** If the global VA table is empty, then construct it. *)
  (** TODO: Is ".length" is fastest way to compute this in OCaml? *)
  if Hashtbl.length va_table = 0 then begin

    (** Create a new temporary C file using IO and String functionalities, whose
     *  prototypes we wish to acquire. *)
    let src_fn, chan = Filename.open_temp_file "tmp" ".c" in
    Printf.fprintf chan "#include <stdio.h>\n";
    Printf.fprintf chan "#include <string.h>\n";
    Printf.fprintf chan "FILE * _coverage_fout;\n";
    Printf.fprintf chan "int main() { return 0; }\n"; 
    close_out chan;

    (** Create another temporary C file and write a pre-processed version of
     *  the above file to it. *)
    let pp_fn = Filename.temp_file "tmp" ".c" in
    
    (** Attempt to pre-process the file. *)
    if Program.Preprocessor.preprocess pp cfg src_fn pp_fn then begin
      try

        (** Use CIL to parse the pre-processed file. *)
        let ast =
          try Utility.cil_parse pp_fn
          with e ->
            debug "cilrep: fill_va_table: Frontc.parse: %s\n" (Printexc.to_string e);
            raise e
        in

        (** Check if CIL encountered any errors during parsing.
         *  TODO: Why aren't we killing the program here? *)
        if !Errormsg.hadErrors then
          Errormsg.parse_error
            "instrument: fill_va_table: failure while preprocessing stdio header file declarations\n";

        (** Extract the prototypes from the parsed file. *)
        iterGlobals ast (fun g ->
          match g with
          | GVarDecl(vi, _) | GVar(vi, _, _) when List.mem vi.vname vnames ->
            let decls = ref [] in
            let visitor = object (self)
              inherit nopCilVisitor

              method private depend t =
                match t with
                | TComp(ci, _) -> decls := GCompTagDecl(ci,locUnknown) :: !decls
                | TEnum(ei, _) -> decls := GEnumTagDecl(ei,locUnknown) :: !decls
                | _ -> ()

              method vtype t =
                match t with
                | TNamed(_) ->
                  ChangeDoChildrenPost(unrollType t, fun t -> self#depend t; t)
                | _ ->
                  self#depend t; DoChildren
            end in
            let _ = visitCilGlobal visitor g in
            let decls = g::!decls in
              Hashtbl.add va_table vi.vname (vi, decls, true)
          | _ -> ()
        )

      (** TODO: Where could this happen?
       *  - Surely this is covered by the try statement above? *)
      with Frontc.ParseError(msg) ->
      begin
          debug "instrument: fill_va_table: %s\n" msg;
          Errormsg.hadErrors := false
      end
    end;

    (** Destroy the temporary files now that we're done with them.
     *  TODO: Surely we want to this within a faux try-finally construct? *)
    Sys.remove src_fn;
    Sys.remove pp_fn;
  end;

  (** Extract the static arguments introduced by the IO functions. *)
  let static_args = List.fold_left (fun lst x ->
      let is_fout = x = "_coverage_fout" in
      if not (Hashtbl.mem va_table x) then begin
        let vi = makeVarinfo true x void_t in
        Hashtbl.add va_table x (vi, [GVarDecl(vi, locUnknown)], is_fout)
      end;
      let name = if is_fout then "fout" else x in
      let vi, _, _ = Hashtbl.find va_table x in
        (name, Fv vi)::lst
    ) [("wb_arg", Fg("wb")); ("a_arg", Fg("a"));] vnames
  in

  (** Compose a statement constructor with access to the static arguments
   *  introduced by the IO functions. *)
  (** TODO: As this isn't problem-specific, we should just have a dummy
   *  function at the top of the file that we replace during initialisation. *)
  let cstmt
    (stmt_str : string)
    (location : Cil.location)
    (args : (string * Cil.formatArg) list)
  : Cil.stmt =
    let stmt_str = "{" ^ stmt_str ^ "}" in
    let args = args @ static_args in
    let lookup = fun _ -> failwith "no new varinfos!" in
      Formatcil.cStmt stmt_str lookup location args
  in

  (** Create a string map from the names of the IO functions that we're
   *  interested in to their *)
  let global_decls = List.fold_left (fun decls name ->
    match Hashtbl.find va_table name with
    | (_, gs, true) -> StringMap.add name gs decls
    | _             -> decls
    ) StringMap.empty vnames

  (** Return the statement constructor and the map of IO function
    * declarations. *)
  in
    cstmt, global_decls
  )
