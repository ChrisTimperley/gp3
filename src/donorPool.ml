open Utility
open Debug

type t = int list

let syntatic_equality_reduction analysis =
  let stmt_str_ht = Hashtbl.create (Analysis.size analysis) in
  let _ = IntMap.iter (fun sid info ->
    let str = Analysis.Stmt.to_s info in
      Hashtbl.replace stmt_str_ht str sid
  ) (Analysis.stmt_infos analysis) in
  let sids = Hashtbl.fold (fun _ sid sids -> sid::sids) stmt_str_ht [] in
    sids

let build analysis coverage only_executed only_positive line_range =
  debug "building donor pool...\n";
  let _ = debug "-- initial size: %d\n" (Analysis.size analysis) in

  (* (a) Weak equivalency reduction *)
  (* Generate set of statements, find syntatically unique statements, extract
   * their SIDs *)
  let contents = match Analysis.syntatic_equality_checking analysis with
    | false -> Analysis.sids analysis
    | true ->
      let old_size = Analysis.size analysis in
      let contents = syntatic_equality_reduction analysis in
      let reduction = old_size - (List.length contents) in
      let _ =
        debug "-- performed syntatic equality reduction (reduced by %d)\n" reduction
      in
        contents
  in

  (* (b) Removal of empty statements *)
  let contents =
    Analysis.stmt_info_subset (Analysis.stmt_infos analysis) contents
  in
  let contents = match Analysis.empty_checking analysis with
    | false -> contents
    | true ->
      let old_size = IntMap.cardinal contents in
      let contents = IntMap.filter (fun _ info ->
        not (Analysis.Stmt.is_empty info)
      ) contents in
      let reduction = old_size - (IntMap.cardinal contents) in
      let _ =
        debug "-- removed empty statements (reduced by %d)\n" reduction
      in
        contents
  in

  (* Restrict to range of lines *)
  let line_checker lnum = match line_range with
    | None -> lnum != -1
    | Some(line_from, line_to) ->
        (lnum >= line_from) && (lnum <= line_to)
  in
  let contents =
    IntMap.filter (fun _ info ->
      let lnum = Analysis.Stmt.line_no info in
        line_checker lnum
    ) contents
  in

  (* (c) Restrict to executed statements *)
  let contents = List.map fst (IntMap.bindings contents) in
  let contents = match only_executed with
    | false -> contents
    | true ->
        let old_size = List.length contents in
        let contents =
          List.filter (fun s -> Coverage.is_executed coverage s) contents
        in
        let reduction = old_size - (List.length contents) in
        let _ =
          debug "-- restricted to executed statements (reduced by %d)\n" reduction
        in
          contents
  in

  (* (d) Restrict to (exclusively) positively executed statements *)
  let contents = match only_positive with
    | false -> contents
    | true ->
        let old_size = List.length contents in
        let contents =
          List.filter (fun s -> not (Coverage.is_executed_by_negative coverage s)) contents
        in
        let reduction = old_size - (List.length contents) in
        let _ =
          debug "-- restricted to positively executed statements (reduced by %d)\n" reduction
        in
          contents

  (* Strip the SIDs of each statement within the donor pool, and all of the
   * SIDs of their children -- Performed in Operations module *)
  in
    debug "-- final size: %d\n" (List.length contents);
    debug "built donor pool\n";
    contents

let from_json analysis coverage jsn =
  let open Yojson.Basic.Util in
  let jsn = match jsn with
    | `Assoc(_) -> jsn
    | `Null -> `Assoc([])
    | _ -> failwith "illegal 'donor_pool' parameter"
  in
  let only_executed = true in
  let only_positive = match jsn |> member "only_positive" with
    | `Bool(b) -> b
    | `Null -> false
    | _ -> failwith "illegal 'only_positive' parameter"
  in
  let line_range = match jsn |> member "lines" with
    | `Assoc(asc) ->
        let st = `Assoc(asc) |> member "from" |> to_int in
        let ed = `Assoc(asc) |> member "to" |> to_int in
          Some((st, ed))
    | `Null -> None
    | _ -> failwith "illegal lines parameter"
  in
  let line_range = None in
    build analysis coverage only_executed only_positive line_range

let contents pool = pool
