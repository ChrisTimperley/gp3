(**
 * Test schemes are responsible for selecting a sub-set of the test suite to
 * use to perform the evaluation (for all individuals) at each evaluation
 * stage.
 *
 * TODO: Could add another set of modules for ordering tests?
 *)
open Utility
open Debug

(* Base class used by all test schemes *)
class virtual base_cls = object (self)
  method virtual choose : Test.Statistics.t -> Test.test_case list
end

(* Module type used by all test schemes *)
module type Scheme = sig
  type t
  val from_json : Problem.t -> Yojson.Basic.json -> base_cls
end

type t = base_cls

(**
 * This testing scheme selects a sub-set of positive cases from the test suite
 * at random to use as a representative measure at each generation, along with
 * all of the negative test cases. The size of this sub-set is predetermined.
 *)
module Sample : Scheme = struct
  class cls size positive negative = object (self)
    inherit base_cls as super
    method choose _ =
      let positive =
        if    size < (List.length positive) then (Utility.sample_n positive size)
        else  positive
      in
        (negative @ positive)
  end

  type t = cls

  let make problem size =
    let _ = Debug.debug "--- constructing sample test scheme\n" in
    let positive = Test.TestSet.elements (Problem.positive_tests problem) in
    let negative = Test.TestSet.elements (Problem.negative_tests problem) in
      new cls size positive negative

  let from_json p def =
    let open Yojson.Basic.Util in
    let size = match def |> member "size" with
      | `Int(s) -> s
      | `Float(fraction) ->
          truncate (ceil ((float (Problem.num_positive_tests p)) *. fraction))
      | _ ->
          truncate (ceil ((float (Problem.num_positive_tests p)) *. 0.1))
    in
    (* ensure the sample size doesn't exceed number of positive test cases *)
    let size = min size (Problem.num_positive_tests p) in
      make p size
end

(** This testing scheme operates on the entire test suite. Tests are
 *  ordered by their observed frequency of failure. *)
module All = struct
  class cls tests = object (self)
    inherit base_cls as super
    method choose stats =
      Test.Statistics.order_by_fail_rate stats tests
  end

  type t = cls

  let make problem =
    let _ = Debug.debug "--- constructing use-all test scheme\n" in
    let tests = Test.TestSet.elements (Problem.tests problem) in
      new cls tests

  let from_json p def = make p
end

let from_json p def : t =
  debug "-- constructing test scheme\n";
  let open Yojson.Basic.Util in
  let def = match def with
    | `Null -> `Assoc([])
    | _ -> def
  in
  match def |> member "type" with
  | `String "all" -> All.from_json p def
  | `String "sample" | _ -> Sample.from_json p def

let choose scheme stats = scheme#choose stats
