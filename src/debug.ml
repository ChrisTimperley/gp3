(**
 * Since OCaml really doesn't like mixing format4 with other types, I've
 * decided to move all debugging related code into this file and to
 * regrettably implement debugging settings as global variables once again.
 * Ultimately this saves a lot of hassle when you're trying to debug code
 * without access to a configuration.
 *)

let debug fmt =
  let printer (s : string) : unit =
    output_string stdout s;
    flush stdout
  in
    Printf.ksprintf printer fmt

let abort fmt =
  debug "\nABORT:\n\n";
  debug fmt
