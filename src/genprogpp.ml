let () =
  (* Create a faux manifest, containing the provided file *)
  let cfg = Config.mock () in
  let fn = Sys.argv.(1) in
  let fn_base = Filename.basename fn in
  let manifest = Program.Manifest.make (Filename.dirname fn) [fn_base] in
  let program = Program.load manifest in
  let file = match Program.file program fn_base with
    | Some(f) -> f
    | None -> raise (Failure "file not found")
  in
    print_string (Program.File.to_s file cfg);
    ()
