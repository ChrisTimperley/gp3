(**
 * This file is simply used to declare the representation type. Used to avoid
 * circular dependencies elsewhere.
 *
 * TODO: Representations should implement a `build_program` function.
 *)
open Intermediate

type representation =
  | Patch of Edit.t list
type t = representation

(** Returns the list of fixes contained within a given solution *)
let fixes p =
  match p with
  | Patch(fixes) -> fixes

(** Converts an instance of a given representation into a JSON string *)
let to_json r =
  match r with
  | Patch(fixes) -> PatchRepresentation.to_json fixes

(** Converts a given representation to an intermediate. *)
(*
let representation_to_intermediate (r : representation) : intermediate =
  match r with
  | Patch(fixes) -> fixes_to_intermediate fixes

(** Converts a given intermediate into a patch. *)
let intermediate_to_patch (inter : intermediate) : representation =
  Patch(intermediate_to_fixes inter)
*)

(** Returns the size of this representation instance *)
let size r =
  match r with
  | Patch(fixes) -> List.length fixes

(**
 * Returns a string-based hash of a given representation instance, in its
 * canonical form. By first normalising the given representation, we reduce
 * the number of results cache misses.
 *)
let hash r problem =
  match r with
  | Patch(fixes) -> PatchRepresentation.hash fixes

(** Transforms a representation instance into a program *)
let to_program representation problem =
  match representation with
  | Patch(fixes) -> PatchRepresentation.to_program fixes problem

(**
 * Compiles a representation instance to a set of source code files and an
 * executable in the specified output directory. See Compile.build_executable
 * for details about return type.
 *)
let compile representation problem out_dir =
  match representation with
  | Patch(fixes) -> PatchRepresentation.compile fixes problem out_dir

(** Returns a list of the SIDs for all statements modified by a given
 *  representation *)
let modified_statement_ids rep =
  match rep with
  | Patch(fixes) -> PatchRepresentation.modified_statement_ids fixes
