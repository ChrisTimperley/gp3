type t

(** Generates the coverage information for a given program and test suite,
 *  using a provider compiler *)
val build : Config.t -> Program.t -> Test.TestSet.t -> string -> Program.Compiler.t -> bool -> bool -> string -> t

(** Computes the coverage information for a given program and test suite,
 *  using a provided compiler and following instructions provided by a set of
 *  JSON parameters *)
val from_json : Config.t -> Program.t -> Test.TestSet.t -> string -> Program.Compiler.t -> Yojson.Basic.json -> t

(**
 * Determines whether a given SID is visited during the execution of a
 * particular test from its coverage information.
 *)
val visits : t -> Test.test_case -> int -> bool

(** Returns the set of test cases covered by a statement, given by its SID *)
val tests_covered_by_statement : t -> int -> Test.TestSet.t

(**
 * Returns the union of test cases that cover any of a given list of statements,
 * provided by their SIDs.
 *)
val tests_covered_by_statements : t -> int list -> Test.TestSet.t

(** Determines whether a statement with a given SID is executed by an test
 *  case within the coverage *)
val is_executed : t -> int -> bool

(** Determines whether a statement with a given SID is executed by any of
 *  the negative test cases within the coverage *)
val is_executed_by_negative : t -> int -> bool

(** Returns the set of statements executed by failing test cases within the
 *  coverage. *)
val stmts_covered_by_failure : t -> Utility.IntSet.t 

(** Returns the set of statements executed by all of the failing test cases *)
val stmts_covered_by_all_failures : t -> Utility.IntSet.t
