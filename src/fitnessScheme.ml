(**
 * Fitness schemes are responsible for taking a list of provided individuals
 * and assigning them a fitness based upon their performance on a given set
 * of tests, and possibly relative to one another.
 *
 * NOTE: if a test result is absent, we assume the result to be redundant
 * and treat it as a passing test.
 *)
open Utility
open Debug

(* Base class used by all fitness schemes *)
class virtual base_cls = object (self)

  method virtual calculate : (Solution.t * Test.test_case list) list -> unit

  (**
   * Calculates the fitness of all determinable (solution, tests) pairs
   * in a given list
   **)
  method compute sol_tests =
    self#calculate sol_tests
end

(* Module type used by all fitness schemes *)
module type Scheme = sig
  type t
  val from_json : Problem.t -> Yojson.Basic.json -> base_cls
end

(** Underlying data type used by fitness schemes *)
type t = base_cls

(**
 * The step fitness scheme assigns a fitness of zero to candidates who fail to
 * pass a positive test case (within their sample). Candidates who pass all of
 * the positive test cases within their sample are assigned a fitness of one.
 * The results of negative test case executions are ignored.
 *)
module Step : Scheme = struct
  class cls = object (self)
    inherit base_cls as super
    method private calculate_one sol tests =

      (* If the solution failed to compile, return a fitness of zero *)
      let score =
        match Solution.compile_success sol with
        | Some(false) -> 0.0
        | _ ->
        begin
          List.fold_left (fun score t ->
            match (t, (Solution.test_result sol t)) with
            | (Test.Positive(_), Some(false)) -> 0.0
            | _ -> score
          ) 1.0 tests
        end
      in
      let genome_s = Solution.describe_genome sol in
        Solution.assign_fitness sol score;
        debug "-- calculated fitness [%s]: %f\n" genome_s score
    method calculate sol_tests =
        List.iter (fun (sol, tests) -> self#calculate_one sol tests) sol_tests
  end

  type t = cls
  let make () : t = new cls
  let from_json p def = make ()
end

(**
 * The weighted fitness scheme aggregates the results for each of the provided
 * test cases into a single scalar value by summing their weighted values,
 * where a failure is given a zero-weighting, and the weight of passing cases
 * may differ for positive and negative test cases.
 *)
module Weighted : Scheme = struct
  class cls pos_weight neg_weight = object (self)
    inherit base_cls as super
    method private calculate_one sol tests =
      let score =
        match Solution.compile_success sol with
        | Some(false) -> 0.0
        | _ ->
        begin
          List.fold_left (fun score t ->
            match (t, (Solution.test_result sol t)) with
            | (Test.Positive(_), Some(true)) | (Test.Positive(_), None) ->
                score +. pos_weight
            | (Test.Negative(_), Some(true)) ->
                score +. neg_weight
            | _ -> score
          ) 0.0 tests
        end
      in
      let genome_s = Solution.describe_genome sol in
        Solution.assign_fitness sol score;
        debug "-- calculated fitness [%s]: %f\n" genome_s score
    method calculate sol_tests =
        List.iter (fun (sol, tests) -> self#calculate_one sol tests) sol_tests
  end

  type t = cls

  let make pos neg : t = (new cls pos neg)

  let from_json p def =
    let open Yojson.Basic.Util in
    let pos_w = match def |> member "positive_weight" with
      | `Float w -> w
      | _ -> 1.0
    in
    let neg_w = match def |> member "negative_weight" with
      | `Float w -> w
      | _ -> 10.0
    in
      (make pos_w neg_w)
end

module PosCount : Scheme = struct
  class cls = object (self)
    inherit base_cls as super
    method private calculate_one sol tests =
      let score =
        match Solution.compile_success sol with
        | Some(false) -> 0.0
        | _ ->
        begin
          List.fold_left (fun score t ->
            match (t, (Solution.test_result sol t)) with
            | (Test.Positive(_), Some(true)) | (Test.Positive(_), None) ->
                score +. 1.0
            | _ -> score
          ) 0.0 tests
        end
      in
      let genome_s = Solution.describe_genome sol in
        Solution.assign_fitness sol score;
        debug "-- calculated fitness [%s]: %f\n" genome_s score
    method calculate sol_tests =
        List.iter (fun (sol, tests) -> self#calculate_one sol tests) sol_tests
  end

  type t = cls
  let make = new cls
  let from_json p def = make
end

let from_json p def : t =
  debug "-- constructing fitness scheme\n";
  let open Yojson.Basic.Util in
  let def = match def with
    | `Null -> `Assoc([])
    | _ -> def
  in
  match def |> member "type" with
  | `String("step") -> Step.from_json p def
  | `String("pos_count") -> PosCount.from_json p def
  | `String("weighted") | _ -> Weighted.from_json p def

let compute t sol_tests = t#compute sol_tests
