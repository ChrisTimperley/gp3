(**
 * The intermediate data type represents a patch as a combination of:
 * - the set of original statements deleted from the program.
 * - a mapping from each original statement (by SID) to its replacement.
 * - a mapping from each original statement (by SID) to an ordered list of the
 *   SIDs appended after that statement.
 *
 * Intermediate to patch operation will have a weird affect on crossover; we
 * need to use an intermediate crossover operator instead.
 *)
open Utility
open Debug
open PatchRepresentation

type intermediate = {
  deletions : IntSet.t;
  replacements : int IntMap.t;
  insertions : (int list) IntMap.t
}

(** Generates an empty intermediate (unmodified program). *)
let empty_intermediate () : intermediate =
  {
    deletions = IntSet.empty;
    replacements = IntMap.empty;
    insertions = IntMap.empty
  }

(** Determines whether there are no statements at a given SID. *)
let no_stmts_at_sid (inter : intermediate) (sid : int) : bool =
  IntSet.mem sid inter.deletions

(** Checks whether a deletion can be performed at a given SID. *)
let can_delete_at_sid (inter : intermediate) (sid : int) : bool =
  not (IntSet.mem sid inter.deletions)

(** Counts the number of statements at a given SID for a provided intermediate. *)
let num_stmts_at_sid (inter : intermediate) (sid : int) : int =
  match IntSet.mem sid inter.deletions with
  | true -> 0
  | false -> begin
      let num_insertions = match IntMap.mem sid inter.insertions with
        | true -> List.length (IntMap.find sid inter.insertions)
        | false -> 0
      in
        1 + num_insertions
  end

(**
 * Replaces the statement residing at the nth slot of a given SID for a
 * provided candidate solution with the supplied surrogate statement.
 *)
let replace_nth_at_sid (inter : intermediate) (sid : int) (n : int) (surrogate : int) : intermediate =

  (* replace the root statement *)
  if n == 0 then begin
    let replacements = IntMap.add sid surrogate inter.replacements in
      { inter with replacements = replacements }

  (* replace an appended statement *)
  end else begin
    let sid_insertions = IntMap.find sid inter.insertions in
    let sid_insertions = replace_nth sid_insertions surrogate (n - 1) in
    let insertions = IntMap.add sid sid_insertions inter.insertions in
      { inter with insertions = insertions }
  end

(**
 * Inserts a given surrogate into a provided intermediate at the nth slot at
 * a specified SID.
 *)
let insert_nth_at_sid (inter : intermediate) (sid : int) (n : int) (surrogate : int) : intermediate =

  (* prepend before first statement at this SID *)
  if n == 0 then begin
    let prev_fst = if IntMap.mem sid inter.replacements
      then IntMap.find sid inter.replacements
      else sid 
    in
    let sid_insertions = match IntMap.mem sid inter.insertions with 
      | false -> []
      | true -> IntMap.find sid inter.insertions
    in
    let sid_insertions = prev_fst::sid_insertions in
    let insertions = IntMap.add sid sid_insertions inter.insertions in
    let deletions = IntSet.remove sid inter.deletions in
    let replacements = IntMap.add sid surrogate inter.replacements in
      { deletions = deletions; replacements = replacements; insertions = insertions }

  (* append after an existing statement *)
  end else begin
    let sid_insertions = match IntMap.mem sid inter.insertions with
      | false -> []
      | true -> IntMap.find sid inter.insertions
    in
    let sid_insertions = insert_at sid_insertions surrogate (n - 1) in
    let insertions = IntMap.add sid sid_insertions inter.insertions in
      { inter with insertions = insertions }
  end

(** Deletes the nth statement residing at a given SID within a provided intermediate. *)
let delete_nth_at_sid (inter : intermediate) (sid : int) (n : int) : intermediate =

  (* deleting first statement *)
  if n == 0 then
    match IntMap.mem sid inter.insertions with
    (* if this is the last remaining statement, mark this SID as destroyed. *)
    | false -> begin
        let replacements = IntMap.remove sid inter.replacements in
        let deletions = IntSet.add sid inter.deletions in
          { inter with replacements = replacements; deletions = deletions }
      end
    (* otherwise replace the first statement with the top insertion. *)
    | true -> begin
        let hd::tl = IntMap.find sid inter.insertions in
        let replacements = IntMap.add sid hd inter.replacements in
        let insertions = match tl with
          | [] -> IntMap.remove sid inter.insertions
          | _ -> IntMap.add sid tl inter.insertions
        in
          { inter with insertions = insertions; replacements = replacements }
      end

  (* deleting all other statements *)
  else begin
    let sid_insertions = IntMap.find sid inter.insertions in
    let sid_insertions = remove_nth sid_insertions (n - 1) in
    let insertions = match sid_insertions with
      | [] -> IntMap.remove sid inter.insertions
      | _ -> IntMap.add sid sid_insertions inter.insertions
    in
      { inter with insertions = insertions }
  end

(** Converts a sequence of fixes to an intermediate. *)
let fixes_to_intermediate fixes =
  let rec aux fixes inter =
    match fixes with
    | [] -> inter
    | fx::rst -> begin
      match fx with
      | Edit.Delete(s1) -> begin
          (* destroy SID and undo all operations at that SID *)
          let deletions = IntSet.remove s1 inter.deletions in
          let replacements = IntMap.remove s1 inter.replacements in
          let insertions = IntMap.remove s1 inter.insertions in
          let inter = {
            deletions = deletions;
            replacements = replacements;
            insertions = insertions
          } in
            aux rst inter
        end
      | Edit.Replace(s1, s2) -> begin
          (* ensure s1 isn't destroyed *)
          if IntSet.mem s1 inter.deletions then
            aux rst inter
          else
            aux rst { inter with replacements = (IntMap.add s1 s2 inter.replacements) }
        end
      | Edit.Insert(s1, s2) -> begin
          (* ensure s1 isn't destroyed *)
          if IntSet.mem s1 inter.deletions then
            aux rst inter
          else begin
            let s1_ins =
              if IntMap.mem s1 inter.insertions then
                IntMap.find s1 inter.insertions
              else
                []
            in
              aux rst { inter with insertions = (IntMap.add s1 (s2::s1_ins) inter.insertions) }
          end
        end
    end
  in

  (* build intermediate from patch sequentially *)
  aux fixes (empty_intermediate ())

(** Converts a given intermediate to a canonical sequence of fixes. *)
let intermediate_to_fixes inter =

  (* adds a sequence of insertion statements into a given patch to produce a
   * statement that is followed by the statements with the SIDs provided to
   * this function, in the order that they were provided. *)
  let rec inserter at insertions fixes =
    match insertions with
    | [] -> fixes
    | nxt::rst -> inserter at rst (Edit.Insert(at,nxt)::fixes)
  in

  (* write deletions to patch first *)
  let fixes = IntSet.fold (fun sid fxs ->
    (Edit.Delete(sid))::fxs) inter.deletions [] in

  (* write appends, in the correct order (so they are reversed in the patch) *)
  let fixes = IntMap.fold (fun sid insertions fxs ->
    inserter sid (List.rev insertions) fxs
  ) inter.insertions fixes in

  (* write replacements *)
  let fixes = IntMap.fold (fun s1 s2 fxs ->
    (Edit.Replace(s1, s2))::fxs) inter.replacements fixes in

    fixes
