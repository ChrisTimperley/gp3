(*
 * TestCase.name
 * TestCase.from_name
 * TestCase.run
 *)

(**
 * This file provides definitions of test cases, test case results and test
 * suites, and contains the code responsible for overseeing their execution.
 *)
open Debug
open Cil
open Utility
open Config
open Printf

(** Used to hold the type and ID of a given test case. *)
type test_case =
  | Positive of int
  | Negative of int

(** Define an arbitary ordering over test cases. *)
module OrderedTest =
struct
  type t = test_case
  let compare (t1 : test_case) (t2 : test_case) : int =
    match t1, t2 with
    | Positive(_), Negative(_) -> -1
    | Negative(_), Positive(_) -> 1
    | Positive(x), Positive(y) | Negative(x), Negative(y) -> compare x y
end
module TestSet = Set.Make(OrderedTest)
module TestMap = Map.Make(OrderedTest)

(** Calculates the name of a given test case. *)
let test_case_name (t : test_case) : string =
  match t with
  | Positive i -> "p" ^ (string_of_int i)
  | Negative i -> "n" ^ (string_of_int i)

(** Converts a test case name to a test case. *)
let test_case_from_name (n : string) : test_case =
  match String.get n 0  with
  | 'p' -> Positive(int_of_string (Str.string_after n 1))
  | 'n' -> Negative(int_of_string (Str.string_after n 1))

(** Stores information about an individual test case execution. *)
type test_case_result =
  | Passed
  | Failed

(** Determines if a given test case execution was passed successfully. *)
let test_case_passed (res : test_case_result) : bool =
  match res with
  | Passed  -> true
  | Failed  -> false 

let passed result =
  match result with
  | Passed -> true
  | Failed -> false

let partition (tests : test_case list) : (test_case list) * (test_case list) =
  let rec aux q pos neg = match q with
    | [] -> (pos, neg)
    | Positive(t)::rst -> aux rst (Positive(t)::pos) neg
    | Negative(t)::rst -> aux rst pos (Negative(t)::neg)
  in
    aux tests [] []

(**
 * Calculate the command to execute a given test case.
 *
 * @param cfg       The current configuration.
 * @param exe_name  The name of the executable for the program being run.
 * @param src_dir   The directory where the executable file belongs.
 * @param test      The test case that is to be run.
 *
 * @return  A tuple containing the command to run for the given test case and
 *  the file to which the result will be written by the test script.
 *)
let calculate_test_case_command
  (cfg      : configuration)
  (exe_name : string)
  (src_dir  : string)
  (test     : test_case)
: string =
  let port = generate_port () in
  let base_command = cfg.test_command in
  let exe_name = Filename.concat src_dir exe_name in
  let cmd = replace_in_string base_command
    [ 
      "__TEST_SCRIPT__", cfg.test_script;
      "__EXE_NAME__", exe_name;
      "__TEST_NAME__", (test_case_name test);
      "__SOURCE_NAME__", src_dir;
      "__PORT__", (string_of_int port);
    ] 
  in
    cmd

let command test cfg exe_name src_dir =
  calculate_test_case_command cfg exe_name src_dir test

(* deferred *)
let execute test cfg exe_name src_dir =
  let cmd = command test cfg exe_name src_dir in
  let p = popen ~stdout:(UseDescr(dev_null)) ~stderr:(UseDescr(dev_null))
        "/bin/bash" ["-c"; cmd]
  in
    p.pid

(**
 * This function is called after a test has been run, and is used to interpret
 * the test's process exit status to determine if the test passed.
 *
 * @param status        Process status for the executed test case.
 *
 * @return Result of the test case execution.
*)
let read_test_case_result (status : Unix.process_status) : test_case_result =
    match status with
    | Unix.WEXITED(0) -> Passed
    | _ -> Failed

(**
 * (Non-blocking) Executes a given test case using a compiled program described
 * by the provided parameters. Returns the PID and an anonymous function,
 * capable of blocking and returning the result for that particular test case
 * execution; if this anonymous function isn't called, the program won't block.
 *)
let run test cfg exe_name src_dir : int * (unit -> test_case_result) =
  let cmd = command test cfg exe_name src_dir in
  let p =
    popen ~stdout:(UseDescr(dev_null)) ~stderr:(UseDescr(dev_null))
      "/bin/bash" ["-c"; cmd]
  in
  let pid = p.pid in
  let fetcher () = read_test_case_result (snd (Unix.waitpid [] pid)) in
    (pid, fetcher)

(**
 * This function executes a single test case for a compiled program residing
 * in a given directory.
 *
 * @param cfg       The current configuration.
 * @param exe_name  The name of the executable file for the program.
 * @param src_dir   The directory in which the executable file resides.
 * @param test      The test case that should be executed.
 *
 * @return A [test_case_result] data structure describing the nature of the
 *  success or failure of the provided test case.
 *
 * @raise Fail("Unix.fork") if the call to [Unix.system] fails. 
 *)

let run_test_case
  (cfg      : configuration)
  (exe_name : string)
  (src_dir  : string)
  (test     : test_case)
: test_case_result  =
  let cmd = calculate_test_case_command cfg exe_name src_dir test in
  let status = Stats2.time "test" system cmd in
    read_test_case_result status

(**
 * Runs a suite of test cases for a given program. 
 *
 * TODO: Surely we could achieve this better using promises?
 *
 * @param cfg       The current configuration.
 * @param exe_name  The name of the executable file for this program.
 * @param src_dir   The directory where the compiled executable resides.
 * @param tests     The test suite to evaluate the program on.
 *
 * @raise Fail("internal test case fail") may fail if test_case or
 * internal_test_case does, such as by running into a system call error
 * (e.g., [Unix.fork]), or if any of its own Unix system calls (such as
 * [create_process] or [wait]) fail.
 *)

let rec run_test_cases
  (cfg          : configuration)
  (exe_name     : string)
  (src_dir      : string)
  (tests        : TestSet.t)
  (in_parallel  : int)
: test_case_result StringMap.t =
  
  (* If we're not going to run them in parallel, then just run them
   * sequentially in turn. *) 
  if in_parallel <= 1 || (TestSet.cardinal tests) < 2 then 
    TestSet.fold (fun t res ->
      let tn = test_case_name t in
      let tr = run_test_case cfg exe_name src_dir t in
        StringMap.add tn tr res
    ) tests StringMap.empty

  (* Surely we're missing out a little here? *)
  else if (TestSet.cardinal tests) > in_parallel then begin
    let first, rest = split_nth (TestSet.elements tests) in_parallel in
    let results = ref (run_test_cases cfg exe_name src_dir (TestSet.of_list first) in_parallel) in
      StringMap.iter (fun tn r ->
        results := StringMap.add tn r !results 
      ) (run_test_cases cfg exe_name src_dir (TestSet.of_list rest) in_parallel);
      !results

  (* Check if we've cached the result of a given test. *) 
  end else begin

    (* Create a map to hold the results of each test case. *)
    let results = ref StringMap.empty in

    (* Keep a count of the number of test cases yet to finish execution. *)
    let wait_for_count = ref 0 in

    (* Create a hash-table to store the results of a given test case run. *)
    let result_ht = Hashtbl.create 255 in

    (* Create a hash-table to store the test associated with a given
     * process ID. *)
    let pid_to_test_ht = Hashtbl.create 255 in
      
      (* Invoke each of the test cases via the command line. *)
      TestSet.iter (fun t ->

        (* Could check for existing result in cache. *)
        incr wait_for_count; 
        
        let cmd = calculate_test_case_command cfg exe_name src_dir t in 
        let p = Stats2.time "test" (fun () ->
          popen ~stdout:(UseDescr(dev_null)) ~stderr:(UseDescr(dev_null))
            "/bin/bash" ["-c"; cmd]
        ) () in
          Hashtbl.replace pid_to_test_ht p.pid t
      ) tests;

      (* Wait for each of the test cases to finish executing. *)
      Stats2.time "wait (for parallel tests)" (fun () ->
        while !wait_for_count > 0 do
          try
            match Unix.wait () with
            | pid, status ->
              let test = Hashtbl.find pid_to_test_ht pid in
              let result = 
                read_test_case_result status in
                decr wait_for_count;
                Hashtbl.replace result_ht test result
          with e ->
            wait_for_count := 0;
            (*debug "run_test_cases: wait: %s\n" (Printexc.to_string e)*)
        done
      ) ();

      (* Extract the results from the results hash table and inject into the
       * results map. *)
      TestSet.iter (fun test ->
        let name = test_case_name test in
        let status =
          try
            Hashtbl.find result_ht test
          with _ ->
            debug "run_test_cases: %s assumed failed\n" name;
            Failed
        in
          results := StringMap.add name status !results
      ) tests;

      (* Return the completed results map. *)
      !results
  end

(** Used to maintain statistics about the evaluation of test cases *)
module Statistics =
struct
  type t = {
    tests : test_case list;
    passing : (test_case, int) Hashtbl.t;
    failing : (test_case, int) Hashtbl.t;
    pass_rate: (test_case, float) Hashtbl.t
  }

  let make tests =
    let num_tests = TestSet.cardinal tests in
    let stats = {
        tests = TestSet.elements tests;
        passing = Hashtbl.create num_tests;
        failing = Hashtbl.create num_tests;
        pass_rate = Hashtbl.create num_tests;
      }
    in
    let _ =
      TestSet.iter (fun test ->
        let (passes, fails, prate) = match test with
          | Positive(_) -> (1, 0, 1.0)
          | Negative(_) -> (0, 1, 0.0)
        in
          Hashtbl.add stats.passing test passes;
          Hashtbl.add stats.failing test fails;
          Hashtbl.add stats.pass_rate test prate
      ) tests
    in
      stats

  let passed stats test =
    Hashtbl.find stats.passing test

  let failed stats test =
    Hashtbl.find stats.failing test

  let executed stats test =
    (failed stats test) + (passed stats test)

  let pass_rate stats test =
    Hashtbl.find stats.pass_rate test

  let fail_rate stats test =
    1.0 -. (pass_rate stats test)

  (* Returns a list of the tests within the suite, ordered by their failure
   * rate, from the most to the least frequent.
   *
   * NOTE: if OCaml didn't use merge-sort, it would be faster with certain
   * sorting algorithms to update the list of tests on each request (since
   * the list will remain mostly ordered) *)
  let order_by_fail_rate stats tests =
    let compare x y = compare (pass_rate stats x) (pass_rate stats y) in
      List.fast_sort compare tests

  let update stats test result =
    (* update the passing or failing counter *)
    let tbl = match result with
      | Passed -> stats.passing
      | Failed -> stats.failing
    in
    let count = Hashtbl.find tbl test in
    let _ = Hashtbl.replace tbl test (count + 1) in

    (* recompute the pass rate *)
    let passed = (float_of_int (passed stats test)) in
    let failed = (float_of_int (failed stats test)) in
    let pass_rate = passed /. (passed +. failed) in
    let _ = Hashtbl.replace stats.pass_rate test pass_rate in
      stats

  let info stats test =
    ((passed stats test), (failed stats test), (executed stats test))

  let to_json stats =
    let rows = Hashtbl.fold (fun test passed rows ->
      let test_name = test_case_name test in
      let failed = Hashtbl.find stats.failing test in
      let executed = passed + failed in
      let success_rate = (float_of_int passed) /. (float_of_int executed) in
      let row = 
        (test_name, `Assoc([
          ("passed",        `Int(passed));
          ("failed",        `Int(failed));
          ("executed",      `Int(executed));
          ("success_rate",  `Float(success_rate))
        ]))
      in
        row::rows
    ) stats.passing [] in
      `Assoc(rows) 
end
