open Debug
open Utility

module SIDSet = IntSet
module SIDMap = IntMap

module SyntaxScope = struct
  type t = SS_break | SS_continue
  let to_s ss = match ss with
    | SS_break -> "break"
    | SS_continue -> "continue"

  module Set = Set.Make(struct
    type k = t
    type t = k
    let compare = compare
  end)
end

module Function = struct
  type t = {
    id        : int;        (** VID of this function *)
    dec       : Cil.fundec; (** the CIL declaration of this function *)
    file_name : string;     (** name of the file containing this function *)
    globals   : IntSet.t;   (** VIDs of global variables in scope *)
    locals    : IntSet.t    (** VIDs of local variables in this function *)
  }

  let build dec file globals =
    let open Cil in
    let locals = List.fold_left (fun locals vi -> IntSet.add vi.vid locals)
      IntSet.empty (dec.sformals @ dec.slocals)   
    in
      { file_name = (Program.File.relative_path file);
        dec = dec;
        id = dec.svar.vid;
        globals = globals;
        locals = locals }

  let id func = func.id
  let file_name func = func.file_name
  let dec f = f.dec
  let globals func = func.globals
  let locals func = func.locals
  let visible_vars func = IntSet.union func.globals func.locals

  let to_json func =
    let open Yojson.Basic.Util in
    let globals =
      List.map (fun x -> `Int(x)) (IntSet.elements (globals func))
    in
    let locals =
      List.map (fun x -> `Int(x)) (IntSet.elements (locals func))
    in
    let visible_vars =
      List.map (fun x -> `Int(x)) (IntSet.elements (visible_vars func))
    in
      `Assoc([
        ("id",            `Int(id func));
        ("file_name",     `String(file_name func));
        ("globals",       `List(globals));
        ("locals",        `List(locals));
        ("visible_vars",  `List(visible_vars));
      ])
end

module Stmt = struct
  type t = {
    id              : int;              (** the SID of this statement *)
    stmt            : Cil.stmt;         (** the CIL statement this analysis belongs to *)
    func            : Function.t;       (** analysis of parent function *)
    location        : Cil.location;     (** location of statement in AST *)
    used_vars       : SIDSet.t;         (** IDs of variables used by this statement *)
    declared_labels : StringSet.t;      (** set of labels declared up to this statement *)
    live_before     : IntSet.t;         (** set of variables live before atom *)
    live_after      : IntSet.t;         (** set of variables live after atom *)
    syntax_needed   : SyntaxScope.Set.t; (** break/continue required *)
    syntax_allowed  : SyntaxScope.Set.t; (** can break/continue be used here? *)
    unique_appends  : IntSet.t;         (** SIDS of statements that can be uniquely appended here *)
  }
  
  let build stmt func =
    let open Cil in

    (** Find the vars used by this statement *)
    let used_vars = ref IntSet.empty in
    let var_ref_visitor = object
      inherit nopCilVisitor
      method vvrbl va = 
        used_vars := IntSet.add va.vid !used_vars;
        SkipChildren
    end in
    let _ = visitCilStmt var_ref_visitor stmt in
    let used_vars = !used_vars in

    (** Find the location of this statement *)
    let location = get_stmtLoc stmt.skind in

    (** Put everything together, using temporary values where appropriate *)
      { func = func;
        id = stmt.sid;
        stmt = stmt;
        used_vars = used_vars;
        declared_labels = StringSet.empty;
        syntax_needed = SyntaxScope.Set.empty;
        syntax_allowed = SyntaxScope.Set.empty;
        live_before = IntSet.empty;
        live_after = IntSet.empty;
        unique_appends = IntSet.empty;
        location = location; }

  let sid s = s.id
  let stmt s = s.stmt
  let skind s =
    let open Cil in
      s.stmt.skind
  let location s = s.location
  let file_name s = Function.file_name s.func
  let func_info s = s.func
  let used_vars s = s.used_vars
  let declared_labels s = s.declared_labels
  let syntax_needed s = s.syntax_needed
  let syntax_allowed s = s.syntax_allowed
  let live_before s = s.live_before
  let live_after s  = s.live_after

  let line_no s =
    let open Cil in
    let skind = skind s in
      (get_stmtLoc skind).line 

  let with_labels s labels = { s with declared_labels = labels }
  let with_syntax_scope s needed allowed =
    { s with syntax_needed = needed; syntax_allowed = allowed }
  let with_liveness s before after =
    { s with live_before = before; live_after = after }

  let is_modifiable s =
    let open Cil in
    match skind s with
    | Instr _ | Return _ | If _ | Loop _ -> true
    | Goto _ | Break _ | Continue _ | Switch _ | Block _
    | TryFinally _ | TryExcept _ -> false
  let is_empty s =
    let open Cil in
    match s.stmt.skind with
    | Instr([]) -> true
    | _ -> false
  let to_s s =
    Pretty.sprint ~width:78 (Cil.d_stmt () s.stmt)
  let visible_vars s =
    Function.visible_vars s.func

  (** Determines whether two statements are syntatically equivalent *)
  let syntatically_equivalent s1 s2 =
    (to_s s1) = (to_s s2)

  let to_json s =
    let open Yojson.Basic.Util in
    let syntax_needed =
      List.map SyntaxScope.to_s (SyntaxScope.Set.elements (syntax_needed s))
    in
    let syntax_needed = List.map (fun x -> `String(x)) syntax_needed in
    let syntax_allowed =
      List.map SyntaxScope.to_s (SyntaxScope.Set.elements (syntax_allowed s));
    in
    let syntax_allowed = List.map (fun x -> `String(x)) syntax_allowed in
    let used_vars = 
      List.map (fun v -> `Int(v)) (IntSet.elements (used_vars s))
    in
    let visible_vars =
      List.map (fun v -> `Int(v)) (IntSet.elements (visible_vars s))
    in
    let live_before =
      List.map (fun v -> `Int(v)) (IntSet.elements (live_before s))
    in
    let live_after =
      List.map (fun v -> `Int(v)) (IntSet.elements (live_after s))
    in
    let declared_labels =
      List.map (fun x -> `String(x)) (StringSet.elements (declared_labels s))
    in
      `Assoc([
        ("sid", `Int(sid s));
        ("file_name", `String(file_name s));
        ("function_id", `Int(Function.id (func_info s)));
        ("used_vars", `List(used_vars));
        ("visible_vars", `List(visible_vars));
        ("declared_labels", `List(declared_labels));
        ("syntax_needed", `List(syntax_needed));
        ("syntax_allowed", `List(syntax_allowed));
        ("live_before", `List(live_before));
        ("live_after", `List(live_after));
        ("is_modifiable", `Bool(is_modifiable s));
        ("is_empty", `Bool(is_empty s));
        ("as_string", `String(to_s s))
      ])
end

type t = {
  statements : Stmt.t IntMap.t;
  functions : Function.t IntMap.t;
  variables : Cil.varinfo IntMap.t;
  scope_checking : bool;
  syntatic_equality_checking : bool;
  empty_checking : bool;
  liveness_checking : bool;
  label_checking : bool;
  untyped_returns_checking : bool;
  enclosing_loop_checking : bool;
}

(** Finds the syntax scope settings for each statement in the program.
 *  Returns an updated map of statement analyses. *)
let collect_syntax_scopes file stmts =
  let open Cil in
  let stmts = ref stmts in
  let visitor = object
    inherit nopCilVisitor

    val mutable allowed = SyntaxScope.Set.empty
    val mutable needed  = SyntaxScope.Set.empty

    method vfunc fd =
      allowed <- SyntaxScope.Set.empty;
      needed  <- SyntaxScope.Set.empty;
      DoChildren

    method vstmt s =
      let old_allowed = allowed in
      let old_needed  = needed in

        begin match s.skind with
        | Switch _   ->
            allowed <- SyntaxScope.Set.add SyntaxScope.SS_break allowed
        | Loop _     ->
            allowed <- SyntaxScope.Set.add SyntaxScope.SS_break allowed;
            allowed <- SyntaxScope.Set.add SyntaxScope.SS_continue allowed
        | _ -> ()
        end ;

        needed <- SyntaxScope.Set.empty;
        ChangeDoChildrenPost(s, fun s ->
          begin match s.skind with
            | Break _    ->
                needed <- SyntaxScope.Set.add SyntaxScope.SS_break needed
            | Continue _ ->
                needed <- SyntaxScope.Set.add SyntaxScope.SS_continue needed
            | Switch _ ->
                needed <- SyntaxScope.Set.remove SyntaxScope.SS_break needed
            | Loop _ ->
                needed <- SyntaxScope.Set.remove SyntaxScope.SS_break needed;
                needed <- SyntaxScope.Set.remove SyntaxScope.SS_continue needed
            | _ -> ()
          end;
          allowed <- old_allowed;

          (* Update statement analysis *)
          let info = IntMap.find s.sid !stmts in
          let info = Stmt.with_syntax_scope info needed allowed in
            stmts := IntMap.add s.sid info !stmts;
            needed <- SyntaxScope.Set.union old_needed needed;
            s
        )
  end in
    ignore(Program.File.visit file visitor);
    !stmts

(** Computes a liveness analysis of all the statements in a given file, before
 *  adding the results to a given map of statement analyses and returning the
 *  result *)
let collect_liveness file stmts =
  let open Cil in
  let stmts = ref stmts in
  (* Record the original SID of each statement using labels *)
  let label_visitor = object
    inherit nopCilVisitor
    method vstmt s =
      let new_label = Label(Printf.sprintf " %d" s.sid, locUnknown, false) in
        s.labels <- new_label::s.labels;
        DoChildren
  end in
  let liveness_visitor = object
    inherit nopCilVisitor

    val mutable failed = false

    (* Compute the liveness information for each function sequentially *)
    method vfunc f = 
      Cfg.clearCFGinfo f;
      ignore(Cfg.cfgFun f);
      ( try 
          Errormsg.hadErrors := false; 
          Liveness.computeLiveness f; 
          failed <- false;
          DoChildren
          with e ->
            (*debug "cilRep: liveness failure for %s: %s\n" f.svar.vname (Printexc.to_string e);*) 
            failed <- true;
            DoChildren
      ) 

    method vstmt s = 
      match s.labels with
      | (Label(first,_,_))::rest when first <> "" && first.[0] = ' ' ->
        let sid = my_int_of_string first in 
        let before, after =
          if failed then IntSet.empty, IntSet.empty
          else
            let get_liveness sid liveness =
              Usedef.VS.fold (fun vi ids -> IntSet.add vi.vid ids)
                liveness IntSet.empty
            in
            let after = get_liveness sid (Liveness.getPostLiveness s) in
            let before = get_liveness sid (Liveness.getLiveness s) in
              (before, after)
        in
        (* Update statement analysis *)
        let info = IntMap.find sid !stmts in
        let info = Stmt.with_liveness info before after in
          stmts := IntMap.add s.sid info !stmts;
          DoChildren
      | _ -> DoChildren
  end in
  let file = Program.File.copy file in
    ignore(Program.File.visit file label_visitor);
    ignore(Program.File.visit file liveness_visitor);
    !stmts

(** Finds the set of all declared labels for each statement in a given file.
 *  Returns an updated map of statement analyses. *)
let collect_labels file stmts =
  let open Cil in
  let stmts = ref stmts in
  let visitor = object
    inherit nopCilVisitor

    val mutable seen = StringSet.empty

    (** Reset observed labels for each function *)
    method vfunc func =
      seen <- StringSet.empty;
      DoChildren

    method vstmt s =
      let external_names = seen in
        ChangeDoChildrenPost(s, fun s ->
          (* Update the set of observed labels *)
          seen <-
            List.fold_left (fun names -> function
              | Label(name, _, _) -> StringSet.add name names
              | _ -> names
            ) seen s.labels;

          (* Update statement analysis *)
          let info = IntMap.find s.sid !stmts in
          let labels = StringSet.diff seen external_names in
          let info = Stmt.with_labels info labels in
            stmts := IntMap.add s.sid info !stmts;
            s
        )
  end in
    ignore(Program.File.visit file visitor);
    !stmts

(** Constructs a partial analysis for a given file within a program. Adds the
 *  results to a pair of partially constructed function and statement analyses,
 *  and returns the result *)
let build_file file functions stmts =
  let open Cil in
  let cil_file = (Program.File.as_cil_file file) in

  (* Generate the function analyses *)
  (* Find each function in this file, and the set of all globals that are
   * visible from each *)
  let (_, fdefs) = List.fold_left (fun (seen, funglobs) g -> match g with
      | GVarDecl(vi, _) | GVar(vi, _, _) ->
          ((SIDSet.add vi.vid seen), funglobs)
      | GFun(fd, _) ->
          let funglob = (fd, seen) in
          let seen = SIDSet.add fd.svar.vid seen in
            (seen, funglob::funglobs)
      | _ -> (seen, funglobs)
    ) (SIDSet.empty, []) cil_file.globals
  in
  let functions =
    List.fold_left (fun functions (def, seen) ->
      let info = Function.build def file seen in
        IntMap.add (Function.id info) info functions
    ) functions fdefs
  in

  (** Generate the (initial) statement analyses *)
  let stmts = ref stmts in
  let _ =
    IntMap.iter (fun _ f ->
      let visitor = object
        inherit nopCilVisitor
        method vstmt stmt =
          let stmt = Stmt.build stmt f in
            stmts := IntMap.add (Stmt.sid stmt) stmt !stmts;
            DoChildren
      end in
        ignore(visitCilFunction visitor (Function.dec f))
    ) functions
  in

  (** Perform further analyses *)
  let stmts = collect_labels file !stmts in
  let stmts = collect_syntax_scopes file stmts in
  let stmts = collect_liveness file stmts in
    (functions, stmts)

let build program scope_checking empty_checking syntatic_equality_checking
  enclosing_loop_checking liveness_checking label_checking untyped_returns_checking
=
  let open Cil in
  let vars = List.fold_left (fun vars var ->
      IntMap.add var.vid var vars
    ) (IntMap.empty) (Program.variables program)
  in 
  let (functions, stmts) = List.fold_left (fun (functions, stmts) file ->
    build_file file functions stmts
  ) (IntMap.empty, IntMap.empty) (Program.files program) in
  let _ = debug "- number of statements: %d\n" (IntMap.cardinal stmts) in
    { syntatic_equality_checking = syntatic_equality_checking;
      scope_checking = scope_checking;
      empty_checking = empty_checking;
      liveness_checking = liveness_checking;
      label_checking = label_checking;
      untyped_returns_checking = untyped_returns_checking;
      enclosing_loop_checking = enclosing_loop_checking;
      functions = functions;
      statements = stmts;
      variables = vars }

let from_json program jsn =
  let open Yojson.Basic.Util in
  let jsn = match jsn with
    | `Null -> `Assoc([])
    | `Assoc(_) -> jsn
    | _ -> failwith "Illegal 'analysis' parameter; expected JSON object"
  in
  let syntatic_equality_checking = match jsn |> member "syntatic_equality" with
    | `Bool(b) -> b
    | `Null -> true
    | _ -> failwith "Illegal 'analysis.syntatic_equality' parameter; expected bool"
  in
  let empty_checking = match jsn |> member "empty_checking" with
    | `Bool(b) -> b
    | `Null -> true
    | _ -> failwith "Illegal 'analysis.empty_checking' parameter; expected bool"
  in
  let liveness_checking = match jsn |> member "liveness" with
    | `Bool(b) -> b
    | `Null -> true
    | _ -> failwith "Illegal 'analysis.liveness' parameter; expected bool"
  in
  let scope_checking = match jsn |> member "scope" with
    | `Bool(b) -> b
    | `Null -> true
    | _ -> failwith "Illegal 'analysis.scope' parameter; expected bool"
  in
  let label_checking = match jsn |> member "label" with
    | `Bool(b) -> b
    | `Null -> true
    | _ -> failwith "Illegal 'analysis.label' parameter; expected bool"
  in
  let enclosing_loop_checking = match jsn |> member "enclosing_loop" with
    | `Bool(b) -> b
    | `Null -> true
    | _ -> failwith "Illegal 'analysis.enclosing_loop' parameter; expected bool"
  in
  let untyped_returns_checking = match jsn |> member "untyped_returns" with
    | `Bool(b) -> b
    | `Null -> true
    | _ -> failwith "Illegal 'analysis.untyped_returns' parameter; expected bool"
  in
    build program scope_checking empty_checking syntatic_equality_checking
      enclosing_loop_checking liveness_checking label_checking untyped_returns_checking

let syntatic_equality_checking analysis = analysis.syntatic_equality_checking
let empty_checking analysis = analysis.empty_checking
let scope_checking analysis = analysis.scope_checking
let liveness_checking analysis = analysis.liveness_checking
let untyped_returns_checking analysis = analysis.untyped_returns_checking
let label_checking analysis = analysis.label_checking
let enclosing_loop_checking analysis = analysis.enclosing_loop_checking

let stmt_info analysis sid =
  IntMap.find sid analysis.statements

let stmt_infos analysis = analysis.statements

let stmt analysis sid =
  Stmt.stmt (stmt_info analysis sid)

let stmts analysis =
  let stmts = List.map snd (IntMap.bindings analysis.statements) in
    List.map Stmt.stmt stmts

let sids analysis =
  List.map fst (IntMap.bindings analysis.statements)

let func_info analysis fid =
  IntMap.find fid analysis.functions

let var_info analysis vid =
  IntMap.find vid analysis.variables

let var_name analysis vid =
  (var_info analysis vid).vname

let size analysis =
  IntMap.cardinal analysis.statements

(** Determines whether the variables required are contained within the set of
 *  variables available. This check is performed according to the settings of
 *  the provided analysis, and may make use of one of three different
 *  equivalency schemes:
 *
 * (1) No checking
 * (2) Exact checking; each required variable must appear in the set of
 *    available variables
 * (3) Name checking; names of required variables must appear in the set of the
 *    available variables, although they need not refer to the same variable
 *)
let check_available_vars analysis required available =
  let open Cil in
  let no_check var = true in
  let exact_check var = IntSet.mem var available in
  let name_check available var =
    let vname = (var_info analysis var).vname in
      StringSet.mem vname available 
  in 
  let checker =
    match (analysis.scope_checking, analysis.syntatic_equality_checking) with
    | (false, _) -> no_check
    | (true, false) -> exact_check
    | (true, true) ->
        let available = IntSet.fold (fun v available ->
            StringSet.add ((var_info analysis v).vname) available
          ) available StringSet.empty
        in
          name_check available
  in
    IntSet.for_all checker required

(** Determines whether a given donor statement is within the scope of some
 *  given context, given by a destination statement *)
let in_scope_at analysis destination donor =
  let available = Stmt.visible_vars destination in
  let required = Stmt.used_vars donor in
    check_available_vars analysis required available

let can_insert_at_with analysis sid_at sid_from before =
  let info_at = stmt_info analysis sid_at in
  let info_from = stmt_info analysis sid_from in

  (* (1)Do not insert break/continue if there is no enclosing loop *)
  let enclosing_loop_check () = begin
    let needed  = Stmt.syntax_needed info_from in
    let allowed = Stmt.syntax_allowed info_at in
      SyntaxScope.Set.subset needed allowed
  end in

  (* (2) Do not insert duplicate labels *)
  let label_check () = begin
    true
  end in

  (* (3) Ensure values aren't returned in untyped functions *)
  let untyped_return_check () = begin
    let open Cil in
    match Stmt.skind info_from with 
    | Return(eo, _) -> begin 
      let func_info = Stmt.func_info info_at in
      let fundec = Function.dec func_info in
        match (eo, fundec.svar.vtype) with
        | (None, TFun(tau,_,_,_)) -> isVoidType tau
        | (Some(e), TFun(t2,_,_,_)) -> 
          let t1 = typeOf e in
            (isScalarType t1 = isScalarType t2) &&
            (isPointerType t1 = isPointerType t2) 
        | _, _ -> false 
    end 
    | _ -> true
  end in

  (* (4) Ensure dead code isn't introduced *)
  let dead_code_check () = begin
    let open Cil in
    let liveness =
      if before then (Stmt.live_before info_at) else (Stmt.live_after info_at)
    in
    match Stmt.skind info_from with
    | Instr[ Set((Var(va),_), rhs, loc) ] ->
        check_available_vars analysis (IntSet.singleton va.vid) liveness
    | _ -> true
  end in

  (* Perform checks *)
    ((not analysis.enclosing_loop_checking) || (enclosing_loop_check ())) &&
    ((not analysis.label_checking) || (label_check ())) &&
    ((not analysis.untyped_returns_checking) || (untyped_return_check ())) &&
    ((not analysis.liveness_checking) || (dead_code_check ()))

let can_delete_at analysis sid =
  let stmt_info = stmt_info analysis sid in
    (Stmt.is_modifiable stmt_info) && 
    ((not analysis.empty_checking) || (not (Stmt.is_empty stmt_info)))

(** Returns a sub-set of a statement info map, containing all the statements
 *  whose keys belong to a provided list of SIDs *)
let stmt_info_subset infos sids =
  List.fold_left (fun ss sid ->
    IntMap.add sid (IntMap.find sid infos) ss
  ) (IntMap.empty) sids

let viable_appends analysis sid donors =
  let stmt_info = stmt_info analysis sid in
  (* Check the statement is modifiable *)
  let candidates = match Stmt.is_modifiable stmt_info with
    | true -> stmt_info_subset analysis.statements donors
    | false -> IntMap.empty
  in
  (* If empty checking is enabled, don't allow appends at this statement if
   * it is empty. *)
  let candidates = match (analysis.empty_checking, (Stmt.is_empty stmt_info)) with
    | (true, true) -> IntMap.empty
    | _ -> candidates
  in
  (* Scope checking *)
  let candidates = IntMap.filter (fun _ donor_info ->
    in_scope_at analysis stmt_info donor_info
  ) candidates in
  (* Insertion checking *)
  let candidates = IntMap.filter (fun sid' _ ->
    can_insert_at_with analysis sid sid' false
  ) candidates in
    List.map fst (IntMap.bindings candidates)

let viable_replacements analysis sid donors =
  let stmt_info = stmt_info analysis sid in
  (* Check the statement is modifiable *)
  let candidates = match Stmt.is_modifiable stmt_info with
    | true -> stmt_info_subset analysis.statements donors
    | false -> IntMap.empty
  in
  (* Scope checking *)
  let candidates = IntMap.filter (fun _ donor_info ->
    in_scope_at analysis stmt_info donor_info
  ) candidates in

  (* Check that the statement can be inserted at the destination address *)
  let candidates = IntMap.filter (fun sid' _ ->
    can_insert_at_with analysis sid sid' true
  ) candidates in

  (* Prevent replacement by syntatic equivalents *)
  let candidates = IntMap.filter (fun sid donor_info ->
    not (Stmt.syntatically_equivalent stmt_info donor_info)
  ) candidates in
    List.map fst (IntMap.bindings candidates)

let viable_edits analysis sid donors =
  let deletions = match can_delete_at analysis sid with
    | true -> [Edit.Delete(sid)]
    | false -> []
  in
  let appends = viable_appends analysis sid donors in
  let appends = List.map (fun dsid -> Edit.Insert(sid, dsid)) appends in
  let replacements = viable_replacements analysis sid donors in
  let replacements = List.map (fun dsid -> Edit.Replace(sid, dsid)) replacements in
    deletions @ appends @ replacements

let to_json analysis =
  let open Yojson.Basic.Util in
  let statements = IntMap.fold (fun sid stmt jsn ->
    ((string_of_int sid), (Stmt.to_json stmt))::jsn
  ) analysis.statements [] in
  let functions = IntMap.fold (fun fid func jsn ->
    ((string_of_int fid), (Function.to_json func))::jsn
  ) analysis.functions [] in
  let variables =
    IntMap.fold (fun vid _ jsn ->
      ((string_of_int vid), `String(var_name analysis vid))::jsn
  ) analysis.variables []
  in
    `Assoc([
      ("variables",   `Assoc(variables));
      ("statements",  `Assoc(statements));
      ("functions",   `Assoc(functions))
    ]) 
