(**
 * TODO: re-enable patch normalisation
 *)
open Debug
open Utility

(** Converts a given patch to a string description. *)
let patch_to_string p =
  String.concat "\n" (List.map Edit.to_s p)

let patch_to_short_string p =
  String.concat ";" (List.map Edit.to_s p)

(** Constructs a fix from a given line. *)
let load_fix_from_line s =
  match String.get s 0 with
  | 'd' ->
      let s = Str.bounded_split (Str.regexp_string " ") s 2 in
      let sid = int_of_string (List.nth s 1) in
        Edit.Delete(sid)
  | 'r' -> 
      let s = Str.bounded_split (Str.regexp_string " ") s 3 in
      let sid = int_of_string (List.nth s 1) in
      let id = int_of_string (List.nth s 2) in
        Edit.Replace(sid, id)
  | 'a' ->
      let s = Str.bounded_split (Str.regexp_string " ") s 3 in
      let sid = int_of_string (List.nth s 1) in
      let id = int_of_string (List.nth s 2) in
        Edit.Insert(sid, id)
  | _   -> failwith "Unrecognised fix type identifier." 

(** Loads an AST patch from a string description. *)
let from_string s =
  let lines = Str.split (Str.regexp_string ";") s in
    List.map load_fix_from_line lines

(**
 * Loads an AST patch from a file at a given location.
 *
 * Works by creating a line stream for the given file, parsing each line as
 * an individual edit, using load_fix_from_line, and then using the resulting
 * list to construct a patch.
 *)
let load_patch_from_file f =
  let input : in_channel = open_in f in
  let line_stream : string Stream.t = line_stream_of_channel input in
  try
    let fix_list = map_stream load_fix_from_line line_stream in
      close_in input;
      fix_list
  with e ->
    close_in input;
    raise e

(** Returns a list of the SIDs of all statements modified by a patch,
 *  given as a list of edits *)
let modified_statement_ids p =
  List.map (Edit.sid) p

(** (Non-destructively) applies a sequence of fixes to a given program *)
let apply patch program analysis =
  let program = ref (Program.copy program) in
  let execute_fix fx = begin
    match fx with
    | Edit.Delete sid ->
        Operations.delete analysis !program sid
    | Edit.Replace (sid, i) ->
        Operations.replace analysis !program sid (Analysis.stmt analysis i)
    | Edit.Insert (sid, i) ->
        Operations.append analysis !program sid (Analysis.stmt analysis i)
   end
  in
    List.iter (fun fx -> execute_fix fx) patch;
    !program

(** Compiles a provided patch to a specified location *)
let compile patch problem out_dir =
  let patched = apply patch (Problem.program problem) (Problem.analysis problem) in
    Program.Compiler.compile (Problem.compiler problem) patched out_dir    

(** Produces the normal form of a given patch. *)
(*
let normalise fix_s fixes =
 
  (* Group the fixes by the SIDs at which they operate, and also record their
   * position within the patch. *)
  let groups = ref IntMap.empty in 
  List.iteri (fun i f ->
    let location = Edit.sid f in
    let g = if IntMap.mem location !groups then
        IntMap.find location !groups
      else []
    in
      groups := IntMap.add location ((i, f)::g) !groups
  ) fixes;

  (* Find the (affected) children for each SID. *)
  let sids = IntSet.of_list (List.map fst (IntMap.bindings !groups)) in
  let children = IntSet.fold (fun sid children ->
    let c = IntSet.inter sids (FixSpace.child_stmts fix_s sid) in
      IntMap.add sid c children
  ) sids IntMap.empty in

  (* Processes all the fixes at a given SID (in reverse order), returning the
   * list of active fixes at that address (in normal order). *)
  let rec process_fixes
    (sid : int)
    (q : (int * Edit.t) list)
    (p : (int * Edit.t) list)
  : (int * Edit.t) list =
    match q with
    | [] -> p
    | (nxt_i, nxt_f)::rst -> begin
      match (nxt_f : Edit.t) with

      (* If replacement occurs, discard all fixes at this SID before this
       * operation, and destroy all changes made to each of its children
       * (before this operation). *)
      | Edit.Replace _ ->
          IntSet.iter (fun c ->
            let cg = IntMap.find c !groups in
            let cg = List.filter (fun (cfi, cf) -> cfi > nxt_i) cg in
              groups := IntMap.add c cg !groups
          ) (IntMap.find sid children);
          ((nxt_i, nxt_f)::p)

      (* If this statement is a delete, then for now we can no longer
       * address it or its children.
       *
       * WARNING: This is only the original GenProg behaviour. *)
      | Edit.Delete _ ->
          IntSet.iter (fun c ->
            groups := IntMap.add c [] !groups
          ) (IntMap.find sid children);
          [(nxt_i, nxt_f)]

      (* All other operations require no special treatment. *)
      | _ -> process_fixes sid rst ((nxt_i, nxt_f)::p)
    end
  in

  (* Order the group, such that those at the start of the program come first.
   * This ensures that parents are processed before their children, potentially
   * saving time. *)
  IntMap.iter (fun sid g ->
    groups := IntMap.add sid (process_fixes sid (List.rev g) []) !groups
  ) (!groups);

  (* Form a canonical patch by adding each group from left to right. *)
  List.concat (List.map (fun sid -> List.map snd (IntMap.find sid !groups)) (IntSet.elements sids))
*)

(** Produces a JSON description of a given patch. *)
let to_json fixes =
  "'" ^ patch_to_short_string fixes ^ "'"

(** Produces a string-based hash of a given patch representation *)
let hash fixes = patch_to_short_string fixes

(** Converts this patch into a complete program for a given problem *)
let to_program patch problem =
  let program = Program.copy (Problem.program problem) in
    apply patch program (Problem.analysis problem)
