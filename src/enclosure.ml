(** Transforms given enclosure information into a JSON format. *)
let enclosure_to_json (enclosure : int IntMap.t) : string =
  let flattened = IntMap.fold (fun sid pid flattened ->
    let s = Printf.sprintf "'%d':%d" sid pid in
      s::flattened
  ) enclosure [] in
  let flattened = String.concat "," (List.rev flattened) in
    "{" ^ flattened ^ "}"

(**
 * Returns a list of the SIDs of the children of a statemented with some
 * specified SID for a given problem.
 *)
let child_stmts fix_s sid =
  IntSet.of_list (IntMap.find sid fix_s.children)

(** Computes a map containing the list of the immediate children at each SID
 *  within a given program, using its enclosure information and a list of its
 *  SIDs. *)
let compute_immediate_children sids enclosure =
  
  (* Begin by assuming each SID has no children. *)
  let ic = ref IntMap.empty in
  List.iter (fun sid -> ic := IntMap.add sid [] !ic) sids;

  (* Process each entry in the enclosure information. *)
  IntMap.iter (fun sid pid ->
    if pid <> 0 then begin
      let pid_ic = IntMap.find pid !ic in
      let pid_ic = sid::pid_ic in
        ic := IntMap.add pid pid_ic !ic
    end
  ) enclosure;

  (* Return the computed map. *)
  !ic

(**
 * Computes a map containing a list of the children (recursively) for each
 * statement in a given program, indexed by their SIDs.
 *)
let compute_children
  (sids : int list)
  (ic : (int list) IntMap.t)
: (int list) IntMap.t =
  let rec aux (q : int list) (p : int list) : int list =
    match q with
    | [] -> p
    | nxt::rst ->
        let nxt_ic = IntMap.find nxt ic in
          aux (nxt_ic @ rst) (nxt::p)
  in
  List.fold_left (fun c sid ->
    let ic_sid = IntMap.find sid ic in
      IntMap.add sid (aux ic_sid []) c
  ) IntMap.empty sids 

(** Computes the enclosure information for a given (pre-processed) program. *)
let compute_enclosure program =
  let enclosure = ref IntMap.empty in

  (* This visitor walks across a given CIL file and updates the enclosure
   * information for each of its statements. *)
  let visitor = object
    inherit nopCilVisitor
    method vstmt stmt =
      let pid = stmt.sid in
      
      (** Check that this statement has a valid SID. *)
      if pid > 0 then begin

        (* If there is no entry for this statement in the map, add one. *)
        if not (IntMap.mem pid !enclosure) then begin
          enclosure := IntMap.add pid 0 !enclosure
        end;

        (* Compute the list of statements immediately enclosed by this one. *)
        let enclosed_stmts = match stmt.skind with
          | If(_, b1, b2, _) | TryFinally(b1, b2, _) | TryExcept(b1, _, b2, _) ->
            b1.bstmts @ b2.bstmts
          | Switch(_, b, _, _) | Loop(b, _, _, _) ->
            b.bstmts
          | _ -> []
        in

        (* Filter the list to the list of statements being tracked, before
         * marking each of these statements as being enclosed by this statement. *)
        let enclosed_stmts = List.filter (fun s -> s.sid > 0) enclosed_stmts in 
          List.iter (fun s ->
            enclosure := IntMap.add s.sid pid !enclosure) enclosed_stmts
      end;
     
      (* Process any child statements. *) 
      DoChildren
  end in

    (* Process each of the CIL files in the program and return the resulting
     * enclosure information. *)
    Program.visit program visitor; 
    !enclosure


