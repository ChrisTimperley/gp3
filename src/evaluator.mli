(** Underlying type used to represent evaluator state *)
type t

(** Constructs an evaluator from a given JSON definition *)
val from_json : Config.t -> Problem.t -> ResultsCache.t -> int -> Yojson.Basic.json -> t

(** Constructs an evaluator from provided parameters *)
val make : Config.t -> Problem.t -> int option -> int -> bool -> bool -> bool -> bool -> ResultsCache.t -> t

(** Resets the counters of a given evaluator *)
val reset : t -> unit

(*val unique_visits : t -> int*)


(** Returns the number of unique candidate solutions visited by this evaluator *)
val num_patches : t -> int

(** Returns the number of test case evaluations made by this evaluator *)
val evaluations : t -> int

(** Returns the limit for this evaluator, if it has one at all *)
val limit : t -> int option

(** Produces a summary of the state of a given evaluator *)
val summarise : t -> Yojson.Basic.json

(**
 * Evaluates a provided list of solutions using a given sequence of test cases.
 * Returns an optional termination event.
 *)
val evaluate : t -> Test.Statistics.t -> (Solution.t * Test.test_case list) list -> Termination.t option * Test.Statistics.t
