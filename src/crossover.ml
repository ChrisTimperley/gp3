(**
 * TODO: Refactor - construct an interface for crossover operators
 *)
open Debug
open Config
open Solution
open Utility
open Representation
open PatchRepresentation

type t = (solution list) -> (solution list)

(**
 * Provides a template for a simple crossover operator, based on a provided
 * "cross" function, which takes two parent solutions and returns a list of
 * offspring.
 *
 * It would be fairly easy to extend this to allow an arbitary (but fixed)
 * number of individuals to act as parents, but there isn't much need at the
 * moment.
 *)
let simple_crossover
  (rate   : float)
  (cross  : solution -> solution -> solution list)
: solution list -> solution list =
  let rec aux (q : solution list) (p : solution list) : solution list =
    match q with
    | [] -> p
    | x::[] -> p
    | x::y::z ->
        if (Random.float 1.0) <= rate then
          aux z ((cross x y) @ p)
        else
          aux z (x::y::p)
  in
  let crossover (inputs : solution list) : solution list =
    aux inputs []
  in
    crossover

(** Performs no crossover on the provided individuals. *)
let null_crossover : solution list -> solution list =
  let crossover (inputs : solution list) : solution list =
    inputs
  in
    crossover

(** Performs uniform crossover on a pair of restricted patches *)
let uniform_patch_crossover rate =
  let cross x y =
    let x = Representation.fixes (Solution.representation x) in
    let y = Representation.fixes (Solution.representation y) in

    (* transform into a pair of maps sharing a common set of keys *)
    let (mx, my) = List.fold_left (fun (mx, my) e ->
        let sid = Edit.sid e in
        let mx = IntMap.add sid (Some(e)) mx in
        let my = IntMap.add sid (None) my in
          (mx, my)
      ) ((IntMap.empty), (IntMap.empty)) x
    in
    let (mx, my) = List.fold_left (fun (mx, my) e ->
        let sid = Edit.sid e in
        let my = IntMap.add sid (Some(e)) my in
        let mx = match IntMap.mem sid mx with
          | true -> mx
          | false -> IntMap.add sid (None) mx
        in
          (mx, my)
      ) (mx, my) y
    in

    (* generate a pair of mirror children from the maps *)
    let sids = List.map fst (IntMap.bindings mx) in
    let (ma, mb) = List.fold_left (fun (ma, mb) sid ->
        let ea = IntMap.find sid mx in
        let eb = IntMap.find sid my in
        let (ea, eb) = match Random.bool () with
          | false -> (ea, eb)
          | true -> (eb, ea)
        in
        let ma = IntMap.add sid ea ma in
        let mb = IntMap.add sid eb mb in
          (ma, mb)
      ) ((IntMap.empty), (IntMap.empty)) sids
    in

    (* transform the child maps into fix lists *)
    let a = IntMap.fold (fun sid edit a ->
        match edit with
        | None -> a
        | Some(edit) -> edit::a
      ) ma []
    in
    let b = IntMap.fold (fun sid edit b ->
        match edit with
        | None -> b
        | Some(edit) -> edit::b
      ) mb []
    in

    (* create two new solutions from the fix lists *)
    let a = Solution.make (Patch(a)) in
    let b = Solution.make (Patch(b)) in
      [a; b]

  (* return the crossover function *)
  in
    simple_crossover rate cross

(* *)
(*let one_point_restricted rate =
  let cross x y =

  in
    simple_crossover rate cross
*)

let one_point_patch_crossover (rate : float) : solution list -> solution list =
  let cross (x : solution) (y : solution) : solution list =
    (* Retrieve the patch for each of the solutions. *)
    let px, py = match x.representation, y.representation with
    | Patch(f1), Patch(f2) -> f1, f2
    | _ -> failwith "One-point patch crossover only supports Patch representation."
    in
    (*let _ =
      debug "Crossover: (%s) (%s)\n" (Solution.describe_genome x) (Solution.describe_genome y)
    in*)

    (* Select a crossover point for each patch at random *)
    let lx = match List.length px with
      | 0 -> 0
      | lx -> Random.int (lx + 1)
    in
    let ly = match List.length py with
      | 0 -> 0
      | ly -> Random.int (ly + 1)
    in
    (*debug "-- selected loci: (%d, %d)\n" lx ly;*)

    (* Split each of the solutions into two parts. *)
    let a, b = split_nth px lx in
    let c, d = split_nth py ly in

    (* Combine the four parts into two new patches. *)
    let px = a @ d in
    let py = c @ b in

    (* Create a pair of new representations each of the patches. *)
    let x = Solution.new_solution (Patch(px)) [x.uid; y.uid] in
    let y = Solution.new_solution (Patch(py)) [x.uid; y.uid] in

    (*let _ =
      debug "-- generated: (%s) (%s)\n" (Solution.describe_genome x) (Solution.describe_genome y)
    in*)

    (* Return the new solutions. *)
      [x; y]
  in
    simple_crossover rate cross

(** Builds a crossover function from a given JSON definition. *)
let from_json
  (def : Yojson.Basic.json)
: solution list -> solution list =
  debug "-- constructing crossover operator\n";
  let open Yojson.Basic.Util in
  let def = match def with
    | `Null -> `Assoc([])
    | _ -> def
  in
  match def |> member "type" with
  | `String "null" -> null_crossover
  | `String("uniform") -> begin
      let rate = match def |> member "rate" with
        | `Float(r) -> r
        | _ -> 0.5
      in
        uniform_patch_crossover rate
    end
  | `String "one_point" | _ -> begin
      let rate = match def |> member "rate" with
        | `Float r -> r
        | _ -> 0.5
      in
        one_point_patch_crossover rate
    end
