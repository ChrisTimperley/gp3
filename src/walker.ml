let rec walker evaluator edits time_end num_edits count results =
  let time_now = Unix.gettimeofday () in
  let time_left = match time_end with
    | None -> "[no time limit]"
    | Some(time_end) ->
        let time_left = time_end -. time_now in
          Printf.sprintf "[%f seconds left]" time_left
  in
  match (edits, time_now, time_end) with
  | ([], _, _) ->
      results
  | (_, time_now, Some(time_end)) when time_now >= time_end ->
      results
  | (edit::rst, _, _) ->
      let desc = Edit.to_s edit in
      let _ = print_endline (Printf.sprintf "Evaluating: %s (%d/%d) %s" desc count num_edits time_left) in
      let edit_results = Crawler.EditEvaluator.evaluate evaluator edit in
      let edit_results = (desc, edit_results) in
      let results = edit_results::results in
        walker evaluator rst time_end num_edits (count + 1) results

let walk cfg problem threads time_limit =
  let _ = match time_limit with
    | Some(x) ->
        Debug.debug "Performing random walk with time limit of %f seconds\n" x
    | None ->
        Debug.debug "Performing random walk with no time limit\n"
  in

  (* create a ghost cache to minimise memory usage *)
  let cache = ResultsCache.Ghost.make () in

  (* get a list of candidate SIDs *)
  let coverage = Problem.coverage problem in
  let analysis = Problem.analysis problem in
  let loc = Problem.localisation problem in
  let sids = Localisation.eligible loc in

  let _ = Debug.debug "z\n" in
  let _ = Debug.debug "- %d candidate statements for mutation\n" (List.length sids) in

  (* get a list of donor statements *)
  let donors = DonorPool.contents (Problem.donor_pool problem) in

  (* find deletes *)
  let _ = Debug.debug "- computing single-edit search space\n" in
  let deletes = List.filter (fun sid -> Analysis.can_delete_at analysis sid) sids in
  let deletes = List.map (fun sid -> Edit.Delete(sid)) deletes in

  (* find replacements *)
  let replacements = 
    List.fold_left (fun edits sid ->
      let replacements_at_sid = Analysis.viable_replacements analysis sid donors in
      let replacements_at_sid = List.map (fun donor -> Edit.Replace(sid, donor)) replacements_at_sid in
        edits @ replacements_at_sid
    ) [] sids
  in

  (* find appends *)
  let appends =
    List.fold_left (fun edits sid ->
      let appends_at_sid = Analysis.viable_appends analysis sid donors in
      let appends_at_sid = List.map (fun donor -> Edit.Insert(sid, donor)) appends_at_sid in
        edits @ appends_at_sid
    ) [] sids
  in

  (* create list of edits, shuffle replacements and appends, concat deletes to the front *)
  let edits = replacements @ appends in
  let edits = Utility.shuffle edits in
  let edits = deletes @ edits in
  let num_edits = List.length edits in

  let _ = Debug.debug "- computed single-edit search space: %d edits\n" num_edits in

  (* build an evaluator *)
  let evaluator =
    Crawler.EditEvaluator.make cfg problem threads coverage (ResultsCache.Ghost.downcast cache)
  in

  (* determine the time that the search should terminate at, then crawl! *)
  let time_end = match time_limit with
    | None -> None
    | Some(time_limit) ->
        let time_now = Unix.gettimeofday () in
        let time_end = time_now +. time_limit in
        let _ = Debug.debug "- current time (seconds): %f\n" time_now in
        let _ = Debug.debug "- search scheduled to end at time (seconds): %f\n" time_end in
          Some(time_end)
  in
  let results = walker evaluator edits time_end num_edits 1 [] in
  let _ = Debug.debug "- finished crawling\n" in

  (* write the results to file *)
  let results = `Assoc(results) in
  let fname = Printf.sprintf "%s.walk.json" (Problem.name problem) in
  let out = open_out fname in
    Yojson.Basic.pretty_to_channel out results; 
    close_out out;
    Printf.printf "Wrote fix analysis to %s\n" fname
