type t

(** Selects a specified number of individuals from a provided list of
 *  candidates, according to the selection method *)
val select : t -> Solution.t list -> int -> Solution.t list

(** Produces a descriptor for the given selection method *)
val descriptor : t -> string

(** Constructs a search algorithm from a JSON definition *)
val from_json : Yojson.Basic.json -> t
