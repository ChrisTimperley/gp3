open Utility

(** Fault localisation information is held in an association list of SIDs
 *  and weightings *)
type t = (int * float) list

module Scheme = struct
  type k = t
  class virtual cls_scheme = object (self)
    method virtual compute : Program.t -> Test.TestSet.t -> Coverage.t -> Analysis.t -> k
  end
  type t = cls_scheme

  module Spectrum = struct
    type z = t
    class cls_spectrum metric use_intersection = object(self)
      inherit cls_scheme as super
      method compute program tests coverage analysis =
        let spectrum = FaultSpectrum.build program coverage tests in

        (* optional intersection stage *)
        let sids =
          if use_intersection then Coverage.stmts_covered_by_all_failures coverage
          else Coverage.stmts_covered_by_failure coverage
        in
        let sids = Utility.IntSet.elements sids in

        (* filter out non-modifiables *)
        let sids = List.filter (fun s ->
            Analysis.Stmt.is_modifiable (Analysis.stmt_info analysis s)
          ) sids
        in
          List.map (fun s ->
            let row = FaultSpectrum.row spectrum s in
            let susp = FaultSpectrum.Metric.suspiciousness metric row in
              (s, susp)
          ) sids 
    end
    type t = cls_spectrum
    let downcast s = s
    let make metric use_intersection = new cls_spectrum metric use_intersection
  end

  module Clamped = struct
    type z = t

    let stmts_in_window file line_no window_size =
      let open Cil in
      let func = Program.File.function_of_line_no file line_no in
      let stmts = Program.File.stmts_at_function file func in

      (* find all statements within the window *)
      let window_start = line_no - window_size in
      let window_end = line_no + window_size in
      let rec window remaining contents = match (remaining, contents) with
        | ([], _) -> contents
        | ((stmt::remaining), contents) ->
            let line_no = (get_stmtLoc stmt.skind).line in
            if line_no != -1 && (line_no < window_start || line_no > window_end) then
              window remaining contents
            else
              window remaining (stmt::contents)
      in
      let stmts = window stmts [] in
        stmts

    class cls_clamped use_intersection file_name line_nums window_size = object(self)
      inherit cls_scheme as super
      method compute program tests coverage analysis =
        let open Cil in

        (* get the file with the specified name *)
        let file = match Program.file program file_name with
          | Some(f) -> f
          | None -> failwith (Printf.sprintf "[clamped]: failed to find file with given name in program (%s)" file_name)
        in

        let stmts = List.fold_left (fun stmts line_no ->
          stmts @ (stmts_in_window file line_no window_size)) [] line_nums
        in
        
        (* find the SIDs of the statements within the window *)
        let sids = List.map (fun s -> s.sid) stmts in
        let sids = Utility.IntSet.of_list sids in
      
        (* throw out all statements that can't be edited *)
        let sids = Utility.IntSet.filter (fun sid ->
            Analysis.Stmt.is_modifiable (Analysis.stmt_info analysis sid)
          ) sids
        in

        (* restrict our attention to the statements that were executed by
         * failing test cases. Optionally, restrict our attention exclusively to
         * those that fail all of the (negative) test cases *)
        let failing_stmts =
          if use_intersection then Coverage.stmts_covered_by_all_failures coverage
          else Coverage.stmts_covered_by_failure coverage
        in
        let failing_sids = List.map (fun s -> s.sid) stmts in
        let failing_sids = Utility.IntSet.of_list failing_sids in
        let sids = Utility.IntSet.inter sids failing_sids in

        (* compute a weight for each using the Tarantula metric *)
        let metric = FaultSpectrum.Metric.Tarantula in
        let spectrum = FaultSpectrum.build program coverage tests in
        let sids = Utility.IntSet.elements sids in
        let weights =
          List.map (fun s ->
            let row = FaultSpectrum.row spectrum s in
            let susp = FaultSpectrum.Metric.suspiciousness metric row in
              (s, susp)
          ) sids
        in
          weights
    end
    type t = cls_clamped
    let downcast s = s
    let make use_intersection file_name line_no window_size =
      new cls_clamped use_intersection file_name line_no window_size
  end

  module Uniform = struct
    type z = t
    class cls_uniform use_intersection = object(self)
      inherit cls_scheme as super
      method compute _ _ coverage analysis =

        (* optional intersection stage *)
        let sids =
          if use_intersection then Coverage.stmts_covered_by_all_failures coverage
          else Coverage.stmts_covered_by_failure coverage
        in
        let sids = Utility.IntSet.elements sids in

        (* filter out non-modifiables *)
        let sids = List.filter (fun s ->
            Analysis.Stmt.is_modifiable (Analysis.stmt_info analysis s)
          ) sids
        in
          List.map (fun s -> (s, 1.0)) sids
    end
    type t = cls_uniform
    let downcast s = s
    let make use_intersection = new cls_uniform use_intersection
  end

  let compute scheme program tests coverage analysis =
    scheme#compute program tests coverage analysis 
end

let build program tests coverage analysis scheme =
  let weights = Scheme.compute scheme program tests coverage analysis in
  let weights = List.filter (fun (_, w) -> w > 0.0) weights in
    if weights == [] then
      failwith "Illegal fault localisation generated: no statements left to choose from"
    else begin
      let _ = Debug.debug "-- %d statements in fault localisation\n" (List.length weights) in
        weights
    end

let from_json program tests coverage analysis jsn =
  let open Yojson.Basic.Util in
  let jsn = match jsn with
    | `Null -> `Assoc([])
    | _ -> jsn
  in
  let use_intersection = match jsn |> member "intersection" with
    | `Bool(b) -> b
    | `Null -> false
    | _ -> failwith "Illegal 'use_intersection' parameter; expected name of scheme"
  in
  let scheme : Scheme.t = match jsn |> member "type" with
    | `String("clamped") ->
        let window = match jsn |> member "window" with
          | `Int(i) -> i
          | `Null -> 5
          | _ -> failwith "Illegal 'window' parameter: expected int"
        in
        let file = match jsn |> member "file" with
          | `String(s) -> s
          | _ -> failwith "Illegal 'file' parameter: expected string"
        in
        let lines = match jsn |> member "lines" with
          | `List(l) -> List.map (fun i -> to_int i) l
          | _ -> failwith "Illegal 'lines' parameter: expected list of int"
        in
          Scheme.Clamped.downcast (Scheme.Clamped.make use_intersection file lines window)
    | `String("uniform") ->
        Scheme.Uniform.downcast (Scheme.Uniform.make use_intersection)
    | `String("tarantula") ->
        Scheme.Spectrum.downcast (Scheme.Spectrum.make (FaultSpectrum.Metric.Tarantula) use_intersection)
    | `String("jaccard") ->
        Scheme.Spectrum.downcast (Scheme.Spectrum.make (FaultSpectrum.Metric.Jaccard) use_intersection)
    | `String("ample") ->
        Scheme.Spectrum.downcast (Scheme.Spectrum.make (FaultSpectrum.Metric.Ample) use_intersection)
    | `String("genprog") | `Null ->
        Scheme.Spectrum.downcast (Scheme.Spectrum.make (FaultSpectrum.Metric.GenProg) use_intersection)
    | _ -> failwith "Illegal 'localisation' parameter; expected name of scheme"
  in
    build program tests coverage analysis scheme

let eligible info =
  let eligible = List.filter (fun (sid, w) -> w > 0.0) info in
    List.map fst info

let to_json info =
  let jsn = List.map (fun (sid, w) -> (string_of_int sid, `Float(w))) info in
    `Assoc(jsn)

let sample info =
  fst (choose_one_weighted info)

let remove info sid =
  List.filter (fun (sid', w) -> sid' != sid) info

let num_executed info = List.length info
