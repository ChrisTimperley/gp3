module Kind = struct
  type t =
    | Delete
    | Replace
    | Append
end

type t =
  | Delete  of int
  | Replace of int * int
  | Insert  of int * int

let sid f =
  match f with
  | Delete(sid)     -> sid
  | Insert(sid, _)  -> sid
  | Replace(sid, _) -> sid

let to_s f =
  match f with
  | Delete  sid       -> Printf.sprintf "d %d" sid
  | Replace (sid, id) -> Printf.sprintf "r %d %d" sid id
  | Insert  (sid, id) -> Printf.sprintf "a %d %d" sid id
