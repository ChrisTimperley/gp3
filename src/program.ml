open Debug

module Signature = struct
  type t = Digest.t list
  let make digests = digests
  let digests signature = signature
  let to_s signature = String.concat ";" (List.map Digest.to_hex signature)
end

module File = struct
  type t = {
    relative_path : string;
    cil_file      : Cil.file
  }

  (**
   * This visitor transforms each instruction into its own statement.
   *
   * This allows us to swap each of them individually, rather than swapping
   * blocks of instructions.
   *
   * In order to achieve this, we simply replace each "Instr" statement,
   * initially containing a list of individual instructions, with a sequence of
   * "Instr", each containing a single instruction.
   *)
  class expandInstructionsVisitor = object
    inherit Cil.nopCilVisitor
    method vblock b = 
      let open Cil in
      ChangeDoChildrenPost(b, (fun b ->
        let stmts = List.map (fun stmt ->
          match stmt.skind with
          | Instr([]) -> [stmt] 
          | Instr(first :: rest) -> 
              stmt.skind <- Instr([first]);
              stmt :: List.map (fun instr -> mkStmtOneInstr instr) rest 
          | _ -> [ stmt ] 
        ) b.bstmts in
        let stmts = List.flatten stmts in
          { b with bstmts = stmts } 
      ))
  end

  (**
   * This visitor transforms empty statement lists (e.g., the else branch in if
   * (foo) \{ bar(); \} ) into dummy statements, so that we can modify them later.
   * When building the donor pool, we must be careful to ensure that these dummy
   * statements are not included as potential replacements.
   *)
  class emptyVisitor = object
    inherit Cil.nopCilVisitor
    method vblock b = 
      let open Cil in
      ChangeDoChildrenPost(b,(fun b ->
        if b.bstmts = [] then 
          mkBlock [ mkEmptyStmt () ] 
        else b 
      ))
  end

  (** This visitor extracts all variable declarations from the AST *)
  class varCollectionVisitor vars = object
    inherit Cil.nopCilVisitor
    method vvdec va = 
      let open Cil in
      vars := va::(!vars);
      DoChildren 
  end

  (** Returns a list of all statements belonging to a given function *)
  let stmts_at_function _ func =
    let open Cil in
    let stmts = ref [] in
    let fetch _ stmt _ = (stmts := stmt::(!stmts)) in
    let visitor = new Utility.callbackVisitor fetch in
    let _ = visitCilFunction visitor func in
      !stmts

  (** Returns the starting and ending line number for the body of a given
   *  function within this file *)
  let function_bounds file func =
    let open Cil in
    let stmts = stmts_at_function file func in
    let (min, max) =
      List.fold_left (fun (min, max) stmt ->
        let line_no = match (get_stmtLoc stmt.skind).line with
          | -1 -> None
          | n -> Some(n)
        in
        match (line_no, min, max) with
        | (None, _, _) ->
            (min, max)
        | (Some(line_no), None, None) ->
            (Some(line_no), Some(line_no))
        | (Some(line_no), Some(min), Some(max)) when line_no < min ->
            (Some(line_no), Some(max))
        | (Some(line_no), Some(min), Some(max)) when line_no > max ->
            (Some(min), Some(line_no))
        | _ ->
            (min, max)
      ) (None, None) stmts
    in
    match (min, max) with
    | (Some(min), Some(max)) -> (min, max)
    | _ -> failwith "failed to compute line bounds for function\n"

  let visit file visitor =
    Cil.visitCilFileSameGlobals visitor file.cil_file;
    file

  let variables file =
    let vars = ref [] in
    let _ = visit file (new varCollectionVisitor vars) in
      !vars

  let functions file =
    let open Cil in
      List.fold_left (fun funcs g -> match g with
        | GFun(fd, _) -> fd::funcs
        | _ -> funcs
      ) [] (file.cil_file.globals)

  let stmts file =
    let stmts = ref [] in
    let fetch _ stmt _ = (stmts := stmt::(!stmts)) in
    let _ = visit file (new Utility.callbackVisitor fetch) in
      !stmts

  let function_of_line_no file line_no =
    let rec aux functions =
      match functions with
      | [] -> failwith "no function found at given line number\n"
      | (func::rst) ->
          let (min, max) = function_bounds file func in
          if line_no >= min && line_no <= max then func
          else aux rst
    in
      aux (functions file)

  let stmts_at_line_no file line_no =
    let open Cil in
    List.filter (fun stmt ->
      let loc = Cil.get_stmtLoc stmt.skind in
      let stmt_s = Utility.cil_stmt_to_s stmt in
        Debug.debug "SID [%d] -> Line %d\n" stmt.sid loc.line;
        Debug.debug "%s\n---------\n" stmt_s;
        loc.line == line_no
    ) (stmts file)

  (** Generates the raw CIL AST before post-processing and generating a
   *  File.t *) 
  let load base relative_path =
    let full_path = Filename.concat base relative_path in
    debug "loading file: %s\n" full_path;
    let cil_file = Utility.cil_parse full_path in
    let file = { relative_path = relative_path; cil_file = cil_file } in
    let file = visit file (new expandInstructionsVisitor) in
    let file = visit file (new emptyVisitor) in
      debug "loaded file\n";
      file

  let make path cil = { relative_path = path; cil_file = cil }
  let relative_path file = file.relative_path
  let as_cil_file file = file.cil_file

  let copy file = Utility.copy file

  (** Pre-processes a given file, ready for printing *)
  let prepare_for_write file cfg =
    let open Cil in
    let file = copy file in
    let cil_file = file.cil_file in
    (** Perform Valgrind-specific pre-processing (by CLG) *)
    let cil_file = if Config.is_valgrind cfg then begin
      (* CLG: GIANT HACK FOR VALGRIND BUGS *)
      {cil_file with globals = 
        List.filter (fun g ->
          (** Pretty sure this can be reduced **) 
          match g with
          GVarDecl(vinfo,_) -> (
            match vinfo.vstorage with
            Extern when vinfo.vname = "__builtin_longjmp" -> false
            | _ -> true)
          | _ -> true) cil_file.globals}
      end else cil_file
    in 
    (** Remove `compiler built-in` commented-out function declarations that
     *  otherwise appear at the top of the printed file. *)
    let cil_file = 
      {cil_file with globals = 
        List.filter (fun g ->
          match g with
          | GVarDecl(vi,l) when
            (not !printCilAsIs && Hashtbl.mem Cil.builtinFunctions vi.vname) ->
              false 
          | _ -> true) cil_file.globals}
    in
    let file = { file with cil_file = cil_file } in
      file

  let write_to file cfg out_fn =
    let _ = Utility.ensure_directories_exist out_fn in 
    let file = prepare_for_write file cfg in 
    let chan = open_out out_fn in
    let old_directive_style = !Cil.lineDirectiveStyle in
      Cil.lineDirectiveStyle := None; 
      Cil.iterGlobals file.cil_file (Cil.dumpGlobal Cil.defaultCilPrinter chan);
      Cil.lineDirectiveStyle := old_directive_style;
      close_out chan

  let to_s file cfg =
    let tmp_fn = Filename.temp_file "" ".c" in
    let _ = write_to file cfg tmp_fn in
    let body = Utility.file_to_string tmp_fn in
      Sys.remove tmp_fn;
      body
end

module Manifest = struct
  type t = {
    base  : string;
    paths : string list
  }
  let make base paths = { base = base; paths = paths }

  let read cfg fn =
    let fn = match Filename.is_relative fn with (* find abs. path *)
      | true -> Filename.concat (Config.dir cfg) fn
      | false -> fn
    in
    let base, manifest_name, ext = Utility.split_base_subdirs_ext fn in
    let paths = begin
      match ext with
        | "txt" -> Utility.get_lines fn
        | "c" | "i" | "cu" | "cg" -> [manifest_name ^ "." ^ ext]
        | _ -> 
          failwith (Printf.sprintf "Unexpected file extension %s in parse_files. Permitted: .c, .i, .cu, .cu, .txt" ext)
    end in
      { base = base; paths = paths }

  let num_files manifest = List.length manifest.paths
  let base_path manifest = manifest.base
  let relative_paths manifest = manifest.paths
  let full_paths manifest =
    List.map (fun p -> (Filename.concat manifest.base p)) manifest.paths
end

type t = {
  base_path : string;
  files : File.t list
}

let base_path program = program.base_path
let files program = program.files
let file_names program = List.map (fun f -> File.relative_path f) program.files
let file program fn =
  try
    Some(List.find (fun f -> (File.relative_path f) = fn) program.files)
  with Not_found -> None

let with_files program files =
  { base_path = program.base_path; files = files }

let signature program cfg =
  let digests = List.map (fun f ->
    let f_s = File.to_s f cfg in
      Digest.string f_s
    ) (files program)
  in
    Signature.make digests

let visit program visitor =
  List.iter (fun f -> ignore(File.visit f visitor)) (files program)

let stmts program =
  List.fold_left (fun stmts f -> (File.stmts f) @ stmts) [] (files program)

let sids program =
  let open Cil in
    List.map (fun s -> s.sid) (stmts program)

let size program = List.length (stmts program)

let functions program =
  List.fold_left (fun funcs f -> (File.functions f) @ funcs) [] (files program)

let variables program =
  List.fold_left (fun vars f -> (File.variables f) @ vars) [] (files program)

(** Numbers all the statements within a given program *)
let number_stmts program =
  let open Cil in
  let next_index = ref 1 in
  let number _ stmt _ = begin
    stmt.sid <- !next_index;
    next_index := !next_index + 1
  end in
  let visitor = new Utility.callbackVisitor number in
    visit program visitor;
    program

let load manifest =
  let base = Manifest.base_path manifest in
  let files =
    List.map (fun fn -> File.load base fn) (Manifest.relative_paths manifest)
  in
  let program = { base_path = base; files = files } in
    number_stmts program

let copy program = Utility.copy program

let write_to program cfg out_dir =
  List.iter (fun f ->
    let out_fn = Filename.concat out_dir (File.relative_path f) in
      File.write_to f cfg out_fn
  ) (files program)

module Preprocessor = struct
  type t = {
    compiler_name : string;
    compiler_options : string;
    cmd : string
  }

  let make compiler_name compiler_options cmd =
    { compiler_name = compiler_name;
      compiler_options = compiler_options;
      cmd = cmd }

  let preprocess pp cfg pp_fn out_fn =
    let cmd = Utility.replace_in_string pp.cmd
        [
          "__COMPILER_NAME__",    pp.compiler_name;
          "__COMPILER_OPTIONS__", pp.compiler_options;
          "__OUT_NAME__",         out_fn;
          "__SOURCE_NAME__",      pp_fn;
        ]
    in
    match Utility.system cmd with
      | Unix.WEXITED(0) -> true
      | _ ->
          debug "\tFailed to preprocess file: %s\n" pp_fn;
          false

  let from_json cfg jsn =
    let open Yojson.Basic.Util in
    let jsn = match jsn with
      | `Assoc(_) -> jsn
      | `Null -> `Assoc([])
      | _ -> failwith "failed to parse preprocessor parameters"
    in
    let compiler_name = match jsn |> member "compiler_name" with
      | `String(s) -> s
      | `Null -> "gcc"
      | _ -> failwith "Illegal 'compiler_name' parameter provided for preprocessor"
    in
    let compiler_options = match jsn |> member "compiler_options" with
      | `String(s) -> s
      | `Null -> ""
      | _ -> failwith "Illegal 'compiler_options' parameter provided for preprocessor"
    in
    let cmd = match jsn |> member "command" with
      | `String(s) -> s
      | `Null ->
          "__COMPILER_NAME__ -E __OUT_NAME__ __SOURCE_NAME__ __COMPILER_OPTIONS__"^
          " 2> /dev/null > __OUT_NAME__"
      | _ -> failwith "Illegal `command` parameter provided for preprocessor"
    in
      make compiler_name compiler_options cmd
end

module Compiler = struct
  class virtual cls_compiler = object (self)
    method virtual compile : t -> string -> bool
  end
  type z = t
  type t = cls_compiler

  module Legacy = struct
    type w = t
    class cls_legacy_compiler cfg cmd name options exe_name = object (self)
      inherit cls_compiler as super
      method compile program out_dir =
        (** Write source files to disk *)
        let _ = write_to program cfg out_dir in
        (** Compute the compilation command *)
        let exe_name = Filename.concat out_dir exe_name in
        let src_files =
          List.fold_right (fun fname src_name ->
            let fname = Filename.concat out_dir fname in
              fname ^ " " ^ src_name
          ) (file_names program) ""
        in
        let cmd = Utility.replace_in_string cmd
            [
              "__COMPILER_NAME__",    name;
              "__EXE_NAME__",         exe_name;
              "__SOURCE_NAME__",      src_files;
              "__COMPILER_OPTIONS__", options
            ]
        in
        (** Temporarily execute the command in the same directory as the
         *  provided configuration file. *)
        let cwd = Sys.getcwd () in
        let _ = Sys.chdir (Config.dir cfg) in
        let p =
          Utility.popen ~stdout:(UseDescr(Utility.dev_null)) ~stderr:(UseDescr(Utility.dev_null))
            "/bin/bash" ["-c"; cmd]
        in
        let pid = p.pid in

        (** Find the result of the compilation *)
        let (_, result) = Unix.waitpid [] pid in
        let result = match result with
          | Unix.WEXITED(0) -> true
          | _ -> false
        in

        (** Switch back to the original directory and return the result *)
        let _ = Sys.chdir cwd in
          result
      end
      type t = cls_legacy_compiler
      
      let downcast compiler = compiler

      let make cmd name executable options cfg =
        new cls_legacy_compiler cfg cmd name options executable

      let from_json cfg jsn =
        debug "constructing legacy compiler...\n";
        let open Yojson.Basic.Util in
        let jsn = match jsn with
          | `Null -> `Assoc([])
          | _ -> jsn
        in
        let cmd = match jsn |> member "command" with
          | `String(s) -> s
          | `Null -> "__COMPILER_NAME__ -o __EXE_NAME__ __SOURCE_NAME__ " ^
              "__COMPILER_OPTIONS__ 2>/dev/null >/dev/null"
          | _ -> failwith "Illegal command parameter for legacy compiler"
        in
        let name = match jsn |> member "name" with
          | `String(s) -> s
          | `Null -> "gcc"
          | _ -> failwith "Illegal name parameter for legacy compiler"
        in
        let options = match jsn |> member "options" with
          | `String(s) -> s
          | `Null -> ""
          | _ -> failwith "Illegal options parameter for legacy compiler"
        in
        let executable = match jsn |> member "executable" with
          | `String(s) -> s
          | _ -> failwith "Expected 'executable' parameter for legacy compiler"
        in
          make cmd name executable options cfg
    end

  let compile compiler program out_dir =
    let start_t = Unix.gettimeofday () in
    let result = compiler#compile program out_dir in
    let end_t = Unix.gettimeofday () in
    let run_t = end_t -. start_t in
      (result, run_t)

  let from_json cfg jsn =
    debug "constructing compiler...\n";
    let open Yojson.Basic.Util in
    let jsn = match jsn with
      | `Null -> `Assoc([])
      | _ -> jsn
    in
    match jsn |> member "type" with
      | `String("legacy") | `Null ->
          Legacy.downcast (Legacy.from_json cfg jsn)
      | _ -> failwith "Unrecognised compiler type"
end
