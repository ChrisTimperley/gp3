(** Used to hold function-specific analyses *)
module EditEvaluator : sig
  type t
  val make : Config.t -> Problem.t -> int -> Coverage.t -> ResultsCache.t -> t
  val evaluate : t -> Edit.t -> Yojson.Basic.json
end

val crawl : Config.t -> Problem.t -> int -> unit
