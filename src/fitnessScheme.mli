(** Base type for all fitness scheme instances *)
type t

(** Constructs a fitness scheme from a given JSON definition. *)
val from_json : Problem.t -> Yojson.Basic.json -> t

(**
 * Calculates the fitness for each solution with respect to a list of
 * tests, according to a provided fitness scheme.
 *)
val compute : t -> (Solution.t * Test.test_case list) list -> unit
