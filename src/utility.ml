open Printf
open Yojson
open Config
open Stream
open Str
open Cil
open Debug

module P = Pretty

type json = Yojson.Basic.json

let abspath path =
  if Filename.is_relative path then Filename.concat (Sys.getcwd ()) path
  else path

(** The current active port. *)
let port = ref 808

let cil_parse fn =
  Errormsg.hadErrors := false; (** DO NOT REMOVE! *)
  Frontc.parse fn () 

let dev_null = Unix.openfile "/dev/null" [Unix.O_RDWR] 0o640

(**
 * Generates a fresh port for network-based test suites (e.g. for webserver
 * bugs).
 *)
let generate_port () : int =
  port := (!port + 1);
  if !port > 1600 then
    port := !port - 800;
  !port

(** Converts a given CIL statement to a string. *)
let cil_stmt_to_s (stmt : Cil.stmt) : string =
  P.sprint ~width:78 (Cil.d_stmt () stmt)

(** Helper function for generating ranges *)
let (--) i j = 
  let rec aux n acc =
    if n < i then acc else aux (n-1) (n :: acc)
  in aux j []

(** Returns a list of all the items in a given list before a provided index. *)
let all_before (x : 'a list) (n : int) (inclusive : bool) : 'a list =
  let inclusive_offset = if inclusive then 1 else 0 in
  let rec aux (left : 'a list) (acc : 'a list) (i : int) : 'a list =
    if i >= (n + inclusive_offset) then
      acc
    else begin
      match left with
      | []    -> acc
      | h::t  -> aux t (h::acc) (i + 1)
    end
  in
    List.rev (aux x [] 0)

(** Returns a list of all the items in a given list after a provided index. *)
let all_after (x : 'a list) (n : int) (inclusive : bool) : 'a list = 
  let inclusive_offset = if inclusive then 0 else 1 in
  let rec aux (left : 'a list) (acc : 'a list) (i : int) : 'a list =
    match left with
    | []    -> acc
    | h::t  ->
        if i < (n + inclusive_offset) then
          aux t acc (i + 1)
        else
          aux t (h::acc) (i + 1) 
  in
    List.rev (aux x [] 0)

(** Computes X - Y *)
let list_diff (x : 'a list) (y : 'a list) : 'a list = 
  List.filter (fun xe ->
    List.exists (fun ye -> ye = xe) y
  ) x

(** Roulette selection from a weighted list *)
let choose_one_weighted (lst : ('a * float) list) : 'a * float =
  assert(lst <> []);
  let total_weight = List.fold_left (fun acc (sid,prob) ->
    acc +. prob) 0.0 lst in
    assert(total_weight > 0.0) ;
    let wanted = Random.float total_weight in
    let rec walk lst sofar =
      match lst with
      | [] -> failwith "choose_one_weighted"
      | (sid,prob) :: rest ->
        let here = sofar +. prob in
          if here >= wanted then (sid,prob)
          else walk rest here
    in
      walk lst 0.0

(**
 *
 *
 * See: http://okmij.org/ftp/Haskell/perfect-shuffle.txt
 *)

let shuffle (lst : 'a list) : 'a list =
  let len = List.length lst in
  let original = Array.of_list lst in
  let shuffled = Array.of_list lst in
    for i = 0 to len - 1 do
      let ri = Random.int (len - i) in
      let t = original.(i) in
        shuffled.(i) <- original.(i + ri);
        original.(i) <- original.(i + ri);
        original.(i + ri) <- t
    done;
    Array.to_list shuffled 

(** returns the first N elements of the given list, along with the rest of the
 *  list *) 
let rec first_nth_with_rest lst n =  
  if n < 1 then ([], lst) 
  else match lst with
  | [] -> ([], [])
  | hd::tl ->
      let (tl, rest) = first_nth_with_rest tl (pred n) in
        (hd::tl, rest)

(** returns the first N elements of the given list *) 
let first_nth lst n =
  fst (first_nth_with_rest lst n)

(** return the first N elements of a list and the remainder as well *)
let rec split_nth lst n =  
  if n < 1 then [], lst 
  else match lst with
  | [] -> [], [] 
  | hd :: tl -> 
    let first_part, last_part = split_nth tl (pred n) in
      hd :: first_part, last_part

(** Randomly samples an element from a given list. *)
let sample (lst : 'a list) : 'a = 
  let ln = List.length lst in
  let k = Random.int ln in
    List.nth lst k

(** Randomly samples a specified number of elements from a given list. *)
let sample_n (lst : 'a list) (n : int) : 'a list =
  let shuffled = shuffle lst in
    first_nth shuffled n

(** Parses an address in string form to a list of integers **)
let address_of_string (adr : string) : (int list) =
  List.map int_of_string (Str.split (Str.regexp_string ".") adr)

(** Formats an address as a string **)
let string_of_address (adr : int list) : string =
  String.concat "." (List.map string_of_int adr)

(**
 * Splits a given address into its parent address, and the child index of the
 * given address within its parent.
 *)

let split_address (addr : int list) : (int list) * (int) =
  match List.rev addr with
  | [] | _::[] -> failwith "Failed to split address: too few parts."
  | c_index::r_parent -> (List.rev r_parent), c_index

(** Returns a string describing the type of a given Cil statement **)
let desc_stmt_type (stmt : Cil.stmt) : string =
  match stmt.skind with
  | Instr _       -> "Instr"
  | Return _      -> "Return"
  | Goto _        -> "Goto"
  | Break _       -> "Break"
  | If _          -> "If"
  | Switch _      -> "Switch"
  | Loop _        -> "Loop"
  | Block _       -> "Block"
  | TryFinally _  -> "TryFinally"
  | TryExcept _   -> "TryExcept"
  | _             -> "Unknown" 

(** Extracts all function definitions from a given Cil file **)
(** Surely this can be reformatted to be a little neater? **)
let get_functions (f : Cil.file) : (Cil.fundec list) =
  let l = List.filter (fun g ->
  match g with
  | GFun _ -> true
  | _ -> false) f.globals in
    List.map (fun g -> (match g with
    | GFun (fundec, location) -> fundec
    | _ -> failwith "Failed to extract function definition.")) l;;

(**
 * Inserts a given value into the provided list at some specified location.
 *
 * Throws an error if the index of insertion is invalid.
 *
 * TODO: doctag!
 *)

let insert_at (l : 'a list) (ins : 'a) (at : int) : 'a list =
  if at < 0 then
    failwith "Illegal index: must not be negative."
  else let len = List.length l in
    if at > len
      then failwith "Illegal index: out of bounds of insertion."
    else 
      let rec aux (l : 'a list) (l' : 'a list) (i : int) : 'a list =
      match l with
      | []      -> if i == at then ins::l' else l' 
      | hd::tl  -> aux tl (if i == at then hd::ins::l' else hd::l') (i + 1) in
        List.rev (aux l [] 0)

(**
 * Returns a copy of the given list with the nth-element replaced with some
 * given value.
 *
 * If there is no nth-element, then a copy of the original list is returned.
 *)

let replace_nth (l : 'a list) (r : 'a) (nth : int) : 'a list =
  let rec aux (l : 'a list) (l' : 'a list) (i : int) : 'a list =
  match l with
  | [] -> l'
  | hd::tl -> aux tl (if i == nth then r::l' else hd::l') (i + 1) in
    List.rev (aux l [] 0)

(**
 * Returns a copy of the given list with the nth-element removed.
 *
 * If there is no nth-element, then a copy of the original list is returned.
 *)

let remove_nth (l : 'a list) (nth : int) : 'a list =
  let rec aux (l : 'a list) (l' : 'a list) (i : int) : 'a list =
  match l with
  | [] -> l'
  | hd::tl -> aux tl (if i == nth then l' else hd::l') (i + 1) in
    List.rev (aux l [] 0)  

(**
 * Applies a mapping function a given stream and returns the resulting list.
 *)

let map_stream (fn : 'a -> 'b) (s : 'a Stream.t) : 'b list =
  let res = ref [] in
  Stream.iter (fun v -> res := (fn v)::(!res)) s;
  List.rev !res

(** Returns the contents of each line in a given file in the form of a list. *)
let read_lines (fn : string) : string list = [] 

(**
 * Creates a line stream from a given channel.
 * Credit to: https://ocaml.org/learn/tutorials/streams.html
 *)

let line_stream_of_channel (channel : in_channel) : string Stream.t =
  Stream.from (fun _ ->
    try Some (input_line channel) with End_of_file -> None)

(**
 * This function produces a deep copy of an arbitrary OCaml data structure.
 * Cil.copyFunction does not preserve stmt ids! Don't use it!
 *)

let copy (x : 'a) = 
  let str = Marshal.to_string x [] in
    (Marshal.from_string str 0 : 'a) 

let copy_closures (x : 'a) = 
  let str = Marshal.to_string x [Marshal.Closures] in
    (Marshal.from_string str 0 : 'a) 

let file_to_string (file : string) : string = 
  let b = Buffer.create 255 in 
  try 
    let fin = open_in file in 
      (try while true do
          let line = input_line fin in
            Buffer.add_string b line ; 
            Buffer.add_char b '\n' ; 
        done ; with _ -> begin close_in fin end) ;
      Buffer.contents b 
  with _ -> Buffer.contents b

(**
 * Executes a given command and returns its output (from the standard
 * output) in the form of a string.
 *)

let cmd_with_output (cmd : string) : string = 
  let in_channel = Unix.open_process_in cmd in
  let buffer = Buffer.create 16 in
    begin
    try
      while true do
        Buffer.add_channel buffer in_channel 1
      done
    with End_of_file -> ()
    end;
    let _ = Unix.close_process_in in_channel in
      Buffer.contents buffer

(**
 * Returns the path to a newly created temporary directory.
 * It is the responsibility of the user to delete this directory after
 * it has been used.
 *)

let mktmpdir () : string =
  String.trim (cmd_with_output "mktemp -d")

(** split "filename.dat" into ["filename";"dat"] *) 
let split_ext name =
  try 
    let base = Filename.chop_extension name in
    let ext = String.sub name ((String.length base)+1)
      ((String.length name) - ((String.length base)+1))
    in 
      base,ext
  with _ -> name, ""

(** split "./src/filename.dat" into ["directories/directories",
   "filename";"data"] *)
let split_base_subdirs_ext name =
  try 
    let base = Filename.basename name in
    let basename,ext = split_ext base in
      Filename.dirname name,basename,ext
  with _ -> "", name, ""

(** given "a/b/c.txt", create "a/" and then "a/b/" if they don't already exist *)
let rec ensure_directories_exist filename = 
  match split_base_subdirs_ext filename with
  | "",_,_ | ".",_,_ | "/",_,_ -> () 
  | dirname,_,_ -> 
    ensure_directories_exist dirname ; 
    (try Unix.mkdir dirname 0o755 with _ -> ())

let file_to_string (file : string) : string = 
  let b = Buffer.create 255 in 
    try 
      let fin = open_in file in 
        (try while true do
            let line = input_line fin in
              Buffer.add_string b line ; 
              Buffer.add_char b '\n' ; 
          done ; with _ -> begin close_in fin end) ;
        Buffer.contents b 
    with _ -> Buffer.contents b 

(** [string_to_file file contents] writes [contents] to the file named [file] *)
let string_to_file (file : string) (contents : string) =
  let fout = open_out file in
  output_string fout contents;
  close_out fout

(** @return number of lines in a text file as a float *)
let count_lines_in_file (file : string) : float =
  try 
    let fin = open_in file in 
    let count = ref 0 in
      (try while true do
          let line = input_line fin in
            ignore line ;
            incr count 
        done ; 0. with _ -> begin close_in fin ; float_of_int !count end) 
  with _ -> 0.

let get_lines (filename : string) : string list = 
  let fin = open_in filename in
  let res = ref [] in
    (try
       while true do
         res := (input_line fin) :: !res
       done
     with End_of_file -> close_in fin);
    List.rev !res

let iter_lines filename func = 
  let fin = open_in filename in
  let rec dolines () =
    try
      let line = input_line fin in 
        func line; dolines()
    with End_of_file -> close_in fin
  in
    dolines ()

(** Read an integer from a string with error reporting. *)
let my_int_of_string (str : string) : int =
  try
    let res = ref 0 in
      Scanf.sscanf str " %i" (fun i -> res := i);
      !res
  with _ -> begin
    if String.lowercase str = "true" then 1
    else if String.lowercase str = "false" then 0
    else failwith ("cannot convert to an integer: " ^ str)
  end

(** Replaces all occurrences of a list of sub-strings with their
 *  associated replacement strings and returns the result. *)
let replace_in_string base_string list_of_replacements = 
  List.fold_left (fun acc (literal,replacement) ->
    let regexp = Str.regexp (Str.quote literal) in
      Str.global_replace regexp replacement acc 
  ) base_string list_of_replacements 

(** {6 Subprocess Management} *) 

(** Sends a SIGKILL to a process with a given [pid] *)
let kill_process pid = Unix.kill pid Sys.sigkill

(** Process information returned by [popen]. *)
type process_info = {
    pid  : int ;
    fin  : out_channel option ;
    fout : in_channel option ;
    ferr : in_channel option ;
  }

(** I/O redirection modes for [popen]. *)
type 'a redirect =
  | Keep                        (** keep this process's stdio *)
  | NewChannel                  (** create a new channel and return it *)
  | UseChannel of 'a            (** use the given channel *)
  | UseDescr of Unix.file_descr (** use the given file descriptor *)

(**/**)
(** Keeps track of how many calls were made to [popen]. We want to force the
    garbage collector to run every so often, since our process may be using
    a significant amount of reclaimable memory such that a fork cannot
    succeed. *)
let popen_gc_count = ref 0
(**/**)

(** [popen prog args] runs [prog] with the given arguments in parallel with
    this program. Clients may optionally request a channel to write to the
    subprocess's stdin or read from its stdout or stderr. It is the client's
    responsibility to close these channels when they are no longer needed.

    In many cases [system] provides a simpler interface for invoking
    subprocesses. *)
let popen ?(stdin=Keep) ?(stdout=Keep) ?(stderr=Keep) cmd args =
  let parent_cleanup = ref [] in
  let child_cleanup  = ref [] in
  let parent fd =
    parent_cleanup := (fun () -> Unix.close fd) :: !parent_cleanup
  in
  let child fd src dst =
    child_cleanup :=
      (fun () -> Unix.close fd; Unix.dup2 src dst) :: !child_cleanup
  in
  let fin, stdin =
    match stdin with
    | Keep -> None, Unix.stdin
    | NewChannel ->
      let fd_in, fd_out = Unix.pipe () in
        parent fd_in;
        child fd_out fd_in Unix.stdin;
        Some(Unix.out_channel_of_descr fd_out), fd_in
    | UseChannel(chan) -> None, Unix.descr_of_in_channel chan
    | UseDescr(descr)  -> None, descr
  in
  let fout, stdout =
    match stdout with
    | Keep -> None, Unix.stdout
    | NewChannel ->
      let fd_in, fd_out = Unix.pipe () in
        parent fd_out;
        child fd_in fd_out Unix.stdout;
        Some(Unix.in_channel_of_descr fd_in), fd_out
    | UseChannel(chan) -> None, Unix.descr_of_out_channel chan
    | UseDescr(descr)  -> None, descr
  in
  let ferr, stderr =
    match stderr with
    | Keep -> None, Unix.stderr
    | NewChannel ->
      let fd_in, fd_out = Unix.pipe () in
        parent fd_out;
        child fd_in fd_out Unix.stderr;
        Some(Unix.in_channel_of_descr fd_in), fd_out
    | UseChannel(chan) -> None, Unix.descr_of_out_channel chan
    | UseDescr(descr)  -> None, descr
  in
  incr popen_gc_count ;
  if !popen_gc_count > 1000 then begin
    popen_gc_count := 0;
    Gc.compact ();
  end;
  let pid = Unix.fork () in
  if pid = 0 then begin
    List.iter (fun f -> f()) !child_cleanup;
    let args = Array.of_list (cmd::args) in
    Unix.execvp cmd args
  end else begin
    List.iter (fun f -> f()) !parent_cleanup;
    { pid = pid; fin = fin; fout = fout; ferr = ferr }
  end

(** [system command] executes the given command, waits until it terminates and
    returns its termination status. The string is interpreted by /bin/sh and
    therefore can contain redirections, quotes, variables, etc. *)
let system cmd =
  let p = popen "/bin/sh" [ "-c"; cmd ] in
  let _, status = Unix.waitpid [] p.pid in
  status

(** Executes the system "touch" command on a given file. *)
let touch (fn : string) : unit =
  let _ = system (Printf.sprintf "touch %s" fn) in
    ()

let rm_f fn = ignore (system (Printf.sprintf "rm -f %s" fn))

(**
 * Executes the system "rm" command on a given directory using the "-rf" flags.
 *)
let rm_rf dir =
  ignore (system (Printf.sprintf "rm -rf %s" dir))

(**/**)
module OrderedString =
struct
  type t = string
  let compare = compare
end
module StringMap = Map.Make(OrderedString)
module StringSet = Set.Make(OrderedString)

let concat_string_map
  (m1 : 'a StringMap.t)
  (m2 : 'a StringMap.t)
: 'a StringMap.t =
  let m = ref m1 in
    StringMap.iter (fun l v ->
      m := StringMap.add l v !m
    ) m2;
    !m

module OrderedInt =
struct
  type t = int
  let compare = compare
end
module IntMap = Map.Make(OrderedInt)

let int_map_keys (m : 'a IntMap.t) : int list =
  IntMap.fold (fun k _ kl -> k::kl) m []

module IntSet = Set.Make(OrderedInt)

module OrderedWeights =
struct
  type t = int * float
  let compare (a1,a2) (b1,b2) =
    if a2 = b2 then compare a1 b1
    else compare a2 b2
end
module WeightSet = Set.Make(OrderedWeights)
(**/**)

(**
 * This visitor walks through the AST in depth-first order, invoking a provided
 * callback function with the SID, statement, and name of enclosing function at
 * each of its node.
 *)
class callbackVisitor
  (callback : int -> Cil.stmt -> string -> unit)
=
object
  inherit nopCilVisitor

  (** Used to cache the name of the function of node being visited. *)
  val mutable current_function = "???" 

  method vfunc fd =
    let prev_function = current_function in
    current_function <- fd.svar.vname ;
    ChangeDoChildrenPost(fd, fun fd -> current_function <- prev_function; fd)

  method vstmt s =
    callback s.sid s current_function;
    DoChildren
end

(** Transforms a given association list to a hash table. *)
let assoc_to_hashtbl (asc : ('a * 'b) list) : ('a, 'b) Hashtbl.t =
  let ht = Hashtbl.create (List.length asc) in
  List.iter (fun (pname, pval) -> Hashtbl.add ht pname pval) asc;
    ht
(** Transforms a given hash table to an association list. *)
let hashtbl_to_assoc (ht : ('a, 'b) Hashtbl.t) : ('a * 'b) list =
  Hashtbl.fold (fun pname pval asc -> (pname, pval)::asc) ht []

(** Combines two JSON files into a single file. *)
let rec merge_json (fA: json) (fB : json) : json =

  (* merges the hash table forms of the two files. *)
  let union (pname : string) (pval : json) (ht : (string, json) Hashtbl.t) : (string, json) Hashtbl.t =
    let pval = match Hashtbl.mem ht pname with
      | true -> merge_json (Hashtbl.find ht pname) pval
      | false -> pval
    in
      Hashtbl.replace ht pname pval;
      ht
  in
  match fA, fB with
    | `Assoc(attsA), `Assoc(attsB) -> begin
        let htA = assoc_to_hashtbl attsA in
        let htB = assoc_to_hashtbl attsB in
        let htA = Hashtbl.fold union htB htA in
          `Assoc(hashtbl_to_assoc htA)
      end
    | _, v2 -> v2
