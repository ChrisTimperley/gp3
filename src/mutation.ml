(**
 * This file contains the definitions of the different mutation operators
 * in GenProg.
 *
 * TODO:
 * -  I'm no longer a big fan of the way viable_insertions and viable_replacements
 *    are handled, or specifically, how they require a fault space. For all
 *    purposes we've encountered thus far, there has been no need to create the
 *    fault space to perform mutation. Make this more lightweight.
 *)
open Utility
open Debug

type t = (Solution.t list) -> (Solution.t list)

(**
 * Constructs a simple mutator using a provided Representation.t transformation
 * function.
 *)
let mutator
  (rate       : float)
  (mutate_rep : Representation.t -> Representation.t)
: Solution.t list -> Solution.t list =
  let _ = debug "mutation rate: %f\n" rate in
  let mutate_one s =
    if (Random.float 1.0) <= rate then begin
      let parents = if Solution.is_proto_offspring s then s.parents else [s.uid] in
      let rep = Solution.representation s in
      let rep = mutate_rep rep in
        Solution.new_solution rep parents
    end else s
  in
  let mutate_all all =
    List.map mutate_one all
  in
    mutate_all

(**
 * Attempts to perform a single atomic mutation using a given set of deletion,
 * insertion, and replacement operators, to a provided candidate solution at a
 * given SID.
 *)
let mutate_at_sid
  (cfg            : Config.t)
  (problem        : Problem.t)
  (p_deletion     : float)
  (p_insertion    : float)
  (p_replacement  : float)
  (f_deletion     : Problem.t -> Representation.t -> int -> (bool * Representation.t))
  (f_insertion    : Problem.t -> Representation.t -> int -> (bool * Representation.t))
  (f_replacement  : Problem.t -> Representation.t -> int -> (bool * Representation.t))
  (candidate      : Representation.t)
  (sid            : int)
: Representation.t =

  (* attempts to find a viable mutation at the given SID *) 
  let rec pick (can_delete : bool) (can_replace : bool) (can_insert : bool) : bool * Representation.t =

    (* If we can't delete, replace or insert, then we can't do anything at this
     * address. *)
    if not (can_delete || can_replace || can_insert) then
      (false, candidate)
    else

    (* Calculate the probabilities of deletion, replacement and insertion. *)
    let p_delete = if can_delete then p_deletion else 0.0 in
    let p_replace = if can_replace then p_replacement else 0.0 in
    let p_insert = if can_insert then p_insertion else 0.0 in

    (* Calculate the sum of the probabilities. *)
    (* We could normalise our probabilities after this, but that would be
     * purely academic; there isn't any need. *)
    let p_sum = p_delete +. p_replace +. p_insert in

    (* Adjust the probabilities to cumulative probabilities. *)
    let p_replace = p_replace +. p_delete in
    (*let p_insert = p_insert +. p_replace in*)

    (* Generate a random number within the cumulative distribution,
     * before selecting the fix type at that point within the probability
     * distribution. *)
    let pt = Random.float p_sum in

    (* Attempt to delete. *)
    if can_delete && pt < p_delete then begin
      match f_deletion problem candidate sid with
      | false, _ -> pick false can_replace can_insert
      | true, mutated -> (true, mutated)
    end

    (* Attempt to replace. *)
    else if can_replace && pt < p_replace then begin
      match f_replacement problem candidate sid with
      | false, _ -> pick can_delete false can_insert
      | true, mutated -> (true, mutated)
    end

    (* Attempt to insert. *)
    else begin
      match f_insertion problem candidate sid with
      | false, _ -> pick can_delete can_replace false
      | true, mutated -> true, mutated
    end
  in
 
  (* Attempt to delete the atom at the given address, or to pick a viable
   * atom for insertion or replacement at the given address. *)
    match pick true true true with
      | _, mutated -> mutated

(**
 * Performs a single atomic mutation using a given set of deletion, insertion,
 * and replacement operators, to a given Representation.t at a randomly
 * selected SID.
 *)
let mutate_at_random_sid
  (cfg            : Config.t)
  (problem        : Problem.t)
  (p_deletion     : float)
  (p_insertion    : float)
  (p_replacement  : float)
  (f_deletion     : Problem.t -> Representation.t -> int -> (bool * Representation.t))
  (f_insertion    : Problem.t -> Representation.t -> int -> (bool * Representation.t))
  (f_replacement  : Problem.t -> Representation.t -> int -> (bool * Representation.t))
  (candidate      : Representation.t)
: Representation.t =
  let localisation = Problem.localisation problem in
  let sid = Localisation.sample localisation in
    mutate_at_sid cfg problem p_deletion p_insertion p_replacement f_deletion f_insertion f_replacement candidate sid

(** Implements the original, biased patch mutation semantics of GenProg 2. *)
let biased_patch_mutator
  (cfg            : Config.t)
  (problem        : Problem.t)
  (p_deletion     : float)
  (p_insertion    : float)
  (p_replacement  : float)
  (rate           : float)
: Solution.t list -> Solution.t list =
  let donors = DonorPool.contents (Problem.donor_pool problem) in
  (* Biased deletion. *)
  let f_deletion problem candidate sid : (bool * Representation.t) =
    let fixes = match candidate with
      | Representation.Patch(fixes) -> fixes
    in
    match Analysis.can_delete_at (Problem.analysis problem) sid with
    | false -> (false, candidate)
    | true -> (true,  Representation.Patch(fixes @ [Edit.Delete(sid)]))
  in

  (* Biased replacement. *)
  let f_replacement problem candidate sid : (bool * Representation.t) =
    let fixes = match candidate with
      | Representation.Patch(fixes) -> fixes
    in
    match Analysis.viable_replacements (Problem.analysis problem) sid donors with
    | [] -> (false, candidate)
    | replacements ->
        (true, Representation.Patch(fixes @ [Edit.Replace(sid, (sample replacements))]))
  in

  let f_insertion problem candidate sid : (bool * Representation.t) =
    let fixes = match candidate with
      | Representation.Patch(fixes) -> fixes
    in
    match Analysis.viable_appends (Problem.analysis problem) sid donors with
    | [] -> (false, candidate)
    | appends ->
        (true, Patch(fixes @ [Edit.Insert(sid, (sample appends))]))
  in

  (* Generate the individual mutator for this problem. *)
  let ind_mutate : Representation.t -> Representation.t =
    mutate_at_random_sid cfg problem p_deletion p_insertion p_replacement f_deletion f_insertion f_replacement
  in
    mutator rate ind_mutate

(**
 * Provides a restricted patch mutator, which limits each SID to a single edit,
 * and allows previous edits at a given SID to be overwritten. Essentially,
 * this transforms the variable-length edit sequence representation into a
 * fixed-length vector of optional edit operations.
 *)

let restricted_patch_mutator cfg problem p_deletion p_insertion p_replacement rate =
  let donors = DonorPool.contents (Problem.donor_pool problem) in

  (* Removes any edit that exists at a given SID within a provided patch *)
  let remove_fix_at_sid fixes sid =
    List.filter (fun e -> (Edit.sid e) <> sid) fixes
  in

  (* Helper function for adding a fix into the patch at a given SID *)
  let add_fix_at_sid fixes fix sid = 
    (remove_fix_at_sid fixes sid) @ [fix]  
  in

  (* Restricted deletion *)
  let f_deletion problem candidate sid =
    let fixes = match candidate with
      | Representation.Patch(fixes) -> fixes
    in
    match Analysis.can_delete_at (Problem.analysis problem) sid with
    | false -> (false, candidate)
    | true -> begin
      let fix = Edit.Delete(sid) in
      let fixes = add_fix_at_sid fixes fix sid in
      let candidate = Representation.Patch(fixes) in
        (true, candidate)
    end
  in

  (* Restricted replacement *)
  let f_replacement problem candidate sid =
    let fixes = match candidate with
      | Representation.Patch(fixes) -> fixes
    in
    match Analysis.viable_replacements (Problem.analysis problem) sid donors with
    | [] -> (false, candidate)
    | replacements -> begin
      let fix = Edit.Replace(sid, (Utility.sample replacements)) in
      let fixes = add_fix_at_sid fixes fix sid in
      let candidate = Representation.Patch(fixes) in
        (true, candidate)
    end
  in

  (* Restricted append *)
  let f_insertion problem candidate sid =
    let fixes = match candidate with
      | Representation.Patch(fixes) -> fixes
    in
    match Analysis.viable_appends (Problem.analysis problem) sid donors with
    | [] -> (false, candidate)
    | appends -> begin
      let fix = Edit.Insert(sid, (Utility.sample appends)) in
      let fixes = add_fix_at_sid fixes fix sid in
      let candidate = Representation.Patch(fixes) in
        (true, candidate)
    end
  in

  (* Generate the individual mutator for this problem. *)
  let ind_mutate : Representation.t -> Representation.t =
    mutate_at_random_sid cfg problem p_deletion p_insertion p_replacement f_deletion f_insertion f_replacement
  in
    mutator rate ind_mutate

(**
 * Implements unbiased mutation semantics.
 * -  the given solution is transformed into an intermediate Representation.t.
 * -  an SID is selected at weighted random, according to the fault localisation
 *    information.
 * -  
 **)
(*
let unbiased_patch_mutator
  (cfg            : configuration)
  (problem        : problem)
  (p_deletion     : float)
  (p_insertion    : float)
  (p_replacement  : float)
  (rate           : float)
: solution list -> solution list =

  (* Keep a handy pointer to the fix space for this problem. *)
  let fix_s = problem.fix_space in

  (* Unbiased deletion operator may delete previously appended (or replaced)
   * statements. Note that deleting a replaced statement does not undo that
   * replace, it merely removes the statement. *)
  let f_deletion (fault_s : fault_space) (candidate : Representation.t) (sid : int) : (bool * Representation.t) =
    let inter = Representation.t_to_intermediate candidate in
    if Intermediate.no_stmts_at_sid inter sid then
      (false, candidate)
    else begin
      let n = Random.int (Intermediate.num_stmts_at_sid inter sid) in
      let inter = Intermediate.delete_nth_at_sid inter sid n in
      let candidate = intermediate_to_patch inter in
        (true, candidate)
    end
  in

  (* Unbiased replacement allows previously inserted or replaced statements to
   * be themselves replaced. *)
  let f_replacement (fault_s : fault_space) (candidate : Representation.t) (sid : int) : (bool * Representation.t) =
    let inter = Representation.t_to_intermediate candidate in
    if no_stmts_at_sid inter sid then
      (false, candidate)
    else begin
      let num_stmts = Intermediate.num_stmts_at_sid inter sid in
      let n = Random.int num_stmts in
      let surrogates = match n with
        | 0 -> viable_replacements cfg fault_s fix_s sid
        | _ -> viable_insertions cfg fault_s fix_s sid
      in
      match surrogates with
        | [] -> (false, candidate)
        | _ -> begin
          let inter = Intermediate.replace_nth_at_sid inter sid n (sample surrogates) in
          let candidate = intermediate_to_patch inter in
            (true, candidate)
        end
    end
  in

  (* Unbiased insertion allows statements to be inserted at any position at
   * a given SID (i.e. before any statement, or after any statement). *)
  let f_insertion (fault_s : fault_space) (candidate : Representation.t) (sid : int) : (bool * Representation.t) =
    let inter = Representation.t_to_intermediate candidate in
    let num_stmts = Intermediate.num_stmts_at_sid inter sid in
    let n = Random.int (num_stmts + 1) in
    let surrogates = match n with
      | 0 -> viable_replacements cfg fault_s fix_s sid
      | _ -> viable_insertions cfg fault_s fix_s sid
    in
    match surrogates with
      | [] -> (false, candidate)
      | _ ->  begin
        let inter = Intermediate.insert_nth_at_sid inter sid n (sample surrogates) in
        let candidate = intermediate_to_patch inter in
          (true, candidate)
      end
  in

  (* Generate the individual mutator for this problem. *)
  let ind_mutate : Representation.t -> Representation.t =
    mutate_at_random_sid cfg problem p_deletion p_insertion p_replacement f_deletion f_insertion f_replacement
  in
    mutator rate ind_mutate
*)

(**
 * The edit undo mutator iterates across the edits of a provided candidate
 * patch and removes edits independently, with a specified probability. As the
 * size of the patch grows, so grows the expected number of removals. With a
 * bit of tuning, this can be used as a simple means of bloat control.
 *)
(*
let edit_undo_mutator
  (cfg : configuration)
  (problem : problem)
  (rate : float)
: solution list -> solution list =

  (* Generate the individual mutator for this operator. *)
  let ind_mutate (r : Representation.t) : Representation.t =
    let fixes = match r with
      | Patch fixes -> fixes
    in
    let fixes = List.fold_left (fun fixes fx ->
        if (Random.float 1.0) <= rate then
          fixes
        else
          fx::fixes
      ) [] fixes
    in
    let fixes = List.rev fixes in
      Patch(fixes)
  in
    mutator rate ind_mutate
*)

(**
 * The multi mutator takes a sequence of separate mutation operations and
 * combines them into a single, compound mutation operator.
 *)
let multi_mutator stages =
  let combinator (inputs : Solution.t list) : Solution.t list =
    List.fold_left (fun inputs stage -> stage inputs) inputs stages
  in
    combinator

(**
 * Creates a copy of a given individual, without their fitness information
 * and compiled form. To return an unmodified list of individuals, use
 * the null mutator.
 *)
let identity_mutator () : Solution.t list -> Solution.t list =
  mutator 1.0 (fun r -> r)

(** Performs no modifications to the provided individuals. *)
let null_mutator () : Solution.t list -> Solution.t list =
  let mutator (inputs : Solution.t list) : Solution.t list = inputs in
    mutator

(** Builds a mutation function from a given JSON definition. *)
let rec from_json cfg problem def =
  debug "-- constructing mutation operator: %s\n" (Yojson.Basic.to_string def);
  let open Yojson.Basic.Util in
  let def = match def with
    | `Null -> `Assoc([])
    | _ -> def
  in
  match def |> member "type" with
  | `String "null" -> null_mutator ()
  | `String "identity" -> identity_mutator ()
  | `String "multiple" ->
      let stage_defs = match def |> member "stages" with
        | `List stage_defs -> stage_defs
        | _ -> failwith "Unexpected `stages` property in multiple mutation operator definition."
      in
      let stages =
        List.map (from_json cfg problem) stage_defs
      in
        multi_mutator stages
  (*
  | `String "unbiased_patch" -> begin
      let rate = match def |> member "rate" with
        | `Float r -> r
        | _ -> 0.5
      in
      let p_replacement = match def |> member "p_replacement" with
        | `Float r -> r
        | _ -> 0.33333
      in
      let p_insertion = match def |> member "p_insertion" with
        | `Float r -> r
        | _ -> 0.33333
      in
      let p_deletion = match def |> member "p_deletion" with
        | `Float r -> r
        | _ -> 0.33333
      in unbiased_patch_mutator cfg problem p_deletion p_insertion p_replacement rate
    end
  | `String "undo" -> begin
      let rate = match def |> member "rate" with
        | `Float r -> r
        | _ -> 0.05
      in
        edit_undo_mutator cfg problem rate
    end
  *)
  | `String("restricted") -> begin
      let rate = match def |> member "rate" with
        | `Int(1) -> 1.0
        | `Float(r) -> r
        | _ -> 0.5
      in
      let p_replacement = match def |> member "p_replacement" with
        | `Float r -> r
        | _ -> 0.33333
      in
      let p_insertion = match def |> member "p_insertion" with
        | `Float r -> r
        | _ -> 0.33333
      in
      let p_deletion = match def |> member "p_deletion" with
        | `Float r -> r
        | _ -> 0.33333
      in restricted_patch_mutator cfg problem p_deletion p_insertion p_replacement rate
    end
  | `String("patch") | _ -> begin
      let rate = match def |> member "rate" with
        | `Int(1) -> 1.0
        | `Float(r) -> r
        | _ -> 0.5
      in
      let p_replacement = match def |> member "p_replacement" with
        | `Float r -> r
        | _ -> 0.33333
      in
      let p_insertion = match def |> member "p_insertion" with
        | `Float r -> r
        | _ -> 0.33333
      in
      let p_deletion = match def |> member "p_deletion" with
        | `Float r -> r
        | _ -> 0.33333
      in biased_patch_mutator cfg problem p_deletion p_insertion p_replacement rate
    end
