(**
 * This program accepts the location of a C source code file and the
 * location of a patch file, applies the patch to the given C file, and
 * prints the resulting C file to the standard output.
 *
 * Note that although the new address scheme removes the notion of
 * (edited program) statement IDs from the edit script, these IDs may still
 * come in handy for caching legal edit information.
 *)

open Yojson
open Debug
open Utility

module Mode = struct
  type t =
    | Repair
    | Diagnose
end

let mode = ref `Repair
let input_patch = ref None
let line_no = ref None
let file_name = ref None

(* command-line option parser *)
let parse_command_line_options () : string * Config.t * Yojson.Basic.json =
  let usage_msg = "GenProg 3 for C" in

  (* option parsers *)
  let jsn = ref `Null in
  let parse_threads threads =
    let row = `Assoc([("threads", `Int(threads))]) in
      jsn := merge_json row !jsn
  in
  let parse_seed seed =
    let row = `Assoc([("seed", `Int(seed))]) in
      jsn := merge_json row !jsn
  in
  let parse_output_dir dir =
    let row = `Assoc([("output_dir", `String(dir))]) in
      jsn := merge_json row !jsn
  in
  let parse_patch patch =
    input_patch := Some(patch)
  in
  let parse_mode m =
    let m = match m with
      | "walk" -> `Walk
      | "diagnose" -> `Diagnose
      | "line" -> `Line
      | "crawl" -> `Crawl
      | "repair" -> `Repair
      | "scratch" -> `Scratch
      | "analyse" -> `Analyse
      | "patch" -> `Patch
      | "evaluate" -> `Evaluate
      | _ -> failwith "unrecognised mode"
    in
      mode := m
  in
  let parse_output_name name =
    let row = `Assoc([("output_name", `String(name))]) in
      jsn := merge_json row !jsn
  in
  let parse_multi_file () =
    let row = `Assoc([("multi_file", `Bool(true))]) in
    let row = `Assoc([("problem", row)]) in
      jsn := merge_json row !jsn
  in
  let parse_sample frac =
    let row = `Assoc([("size", `Float(frac))]) in
    let row = `Assoc([("test_scheme", row)]) in
    let row = `Assoc([("algorithm", row)]) in
      jsn := merge_json !jsn row
      (*jsn := merge_json row !jsn*)
  in
  let parse_coverage_dir dir =
    let row = `Assoc([("directory", `String(dir))]) in
    let row = `Assoc([("coverage", row)]) in
      jsn := merge_json row !jsn
  in
  let parse_time_limit tlim =
    let row = `Assoc([("time_limit", `Float(tlim))]) in
      jsn := merge_json row !jsn
  in
  let parse_cache_fn cache_fn =
    let row = `Assoc([("cache_file", `String(cache_fn))]) in
    let row = `Assoc([("problem", row)]) in
      jsn := merge_json row !jsn
  in
  let parse_line_no ln =
    line_no := Some(ln)
  in
  let parse_file_name fn =
    file_name := Some(fn)
  in
  let set_no_cache () =
    let row = `Assoc([("cache_enabled", `Bool(false))]) in
      jsn := merge_json row !jsn
  in

  (* options manifest *)
  let options = [
    ("--mode", Arg.String parse_mode, "the mode that GenProg should be used in
  (e.g. repair, diagnose, crawl, walk, line, patch, evaluate, analyse)");
    ("--multifile", Arg.Unit parse_multi_file, "used to indicate a multi-file problem");
    ("--seed", Arg.Int parse_seed, "the seed that should be used for the random number generator");
    ("--sample", Arg.Float parse_sample, "the fraction of test cases that should be included within the sample");
    ("--threads", Arg.Int parse_threads, "the number of threads that should be used to perform evaluation");
    ("--output_dir", Arg.String parse_output_dir, "the directory to which the summary file should be writen");
    ("--output_name", Arg.String parse_output_name, "the name that should be used for the summary file (including extension)");
    ("--patch", Arg.String parse_patch, "the patch that should be applied to the program");
    ("--cache", Arg.String parse_cache_fn, "path to the cache file that should be used to store results");
    ("--no-cache", Arg.Unit set_no_cache, "instructs GenProg not to use caching");
    ("--coverage", Arg.String parse_coverage_dir, "path to the directory that should be used to store coverage");
    ("--time-limit", Arg.Float parse_time_limit, "time limit");
    ("--line-no", Arg.Int parse_line_no, "line number - only used by line mode");
    ("--file-name", Arg.String parse_file_name, "name of the file - only used by line mode")
  ] in

  (* anonymous argument handler *)
  (* remember the path of the last file provided *)
  let fdir = ref "" in
  let anon_handler (fn : string) : unit =
    debug "-- loading file: %s\n" fn;
    jsn := merge_json !jsn (Yojson.Basic.from_file fn);
    fdir := Filename.dirname fn
  in

  (* parse arguments *)
  let _ = Arg.parse options anon_handler usage_msg in

  (* build the setup specified in the config file *)
  let cfg = Config.from_json !fdir !jsn in

    (* return the complete configuration *)
    (!fdir, cfg, !jsn)

let apply_patch file cfg problem patch_s =
  match patch_s with
  | None -> debug "No patch provided\n"
  | Some(patch_s) -> begin
    let patch = PatchRepresentation.from_string patch_s in
    let patch = Representation.Patch(patch) in
    let (success, time) = Representation.compile patch problem "patched" in
      Printf.printf "Attempted to compile patch to ./patched\n";
      Printf.printf "Successful? %b\n" success;
      Printf.printf "Time: %f seconds\n" time 
  end

(* TODO: typo --- should be find_stmts_at_line *)
let find_stmts_at_stmt problem file_name line_no =
  let open Cil in
  let line_no = match line_no with
    | Some(line_no) -> line_no
    | None -> failwith "no line number provided"
  in
  let file_name = match file_name with
    | Some(fn) -> fn
    | None -> failwith "no file name provided"
  in
  let program = Problem.program problem in
  let file = match Program.file program file_name with
    | Some(f) -> f
    | None -> failwith (Printf.sprintf "[sids_at_line_no]: failed to find file with given name in program (%s)" file_name)
  in
  let stmts = Program.File.stmts_at_line_no file line_no in
  let sids = List.map (fun s -> s.sid) stmts in
  let sids = List.map string_of_int sids in
  let sids = String.concat ", " sids in
    Printf.printf "Statements at line %d [%s]: %s\n" line_no file_name sids

let evaluate_patch file cfg problem cache threads patch_s =
  match patch_s with
  | None -> Debug.debug "No patch provided\n"
  | Some(patch_s) -> begin
      let _ = Debug.debug "Evaluating patch: %s\n" patch_s in
      let patch = PatchRepresentation.from_string patch_s in
      let patch = Representation.Patch(patch) in
      let solution = Solution.make patch in
      let stats = Test.Statistics.make (Problem.tests problem) in
      let tests = Test.TestSet.elements (Problem.tests problem) in
      let evaluator =
        Evaluator.make cfg problem None threads false false false false cache
      in
      let _ = Evaluator.evaluate evaluator stats [(solution, tests)] in
      let _ = Debug.debug "Evaluated patch: %s\n" patch_s in
      let jsn = Solution.to_json solution in
      let jsn = Yojson.pretty_to_string jsn in
        Printf.printf "%s\n" jsn    
    end

(** Useful function for debugging *)
let scratch file cfg problem =
    Printf.printf "nothing to see here\n"

(** Implements the automated repair mode of this tool *)
let repair file cfg problem cache threads seed output_dir output_file_name tlimit =
  let open Yojson.Basic.Util in
  let searcher =
    file |> member "algorithm" |> Search.from_json cfg problem cache threads tlimit
  in
    Search.run searcher problem file seed output_dir output_file_name

let analyse file cfg problem =
  let jsn = Analysis.to_json (Problem.analysis problem) in
  let out = open_out "analysis.json" in
    Yojson.Basic.pretty_to_channel out jsn;
      close_out out;
      debug "Wrote problem analysis to analysis.json\n"

  (**
   * Diagnoses a given problem, pretty printing its details, including the
   * fault localisation to a JSON file at a specified output.
   *) 
  let diagnose file cfg problem =
    let open Yojson.Basic.Util in
    let program = Problem.program problem in
    let tests = Problem.tests problem in
    let num_stmts = Problem.size problem in
    let num_exec = Problem.num_executed problem in
    let fault_loc = Problem.localisation problem in
    let coverage = Problem.coverage problem in
    let spectrum = FaultSpectrum.build program coverage tests in
    let jsn = `Assoc([
      ("localisation",    (Localisation.to_json fault_loc));
      ("spectrum",        (FaultSpectrum.to_json spectrum));
      ("num_statements",  `Int(num_stmts));
      ("num_executed",    `Int(num_exec))
    ]) in
    let out = open_out "diagnosis.json" in
      Yojson.Basic.pretty_to_channel out jsn; 
      close_out out;
      debug "Wrote problem diagnosis to diagnosis.json\n"

  let () =

    (** Build the setup specified in the config file. *)
    let open Yojson.Basic.Util in
    let fdir, cfg, f = parse_command_line_options () in
    debug "Loaded configuration file\n";

    (* Get the RNG seed, or else generate one *)
    let seed = match f |> member "seed" with
      | `Int s -> s
      | _ -> int_of_float (Unix.time ())
    in

    (* Number of threads *)
    let threads = match f |> member "threads" with
      | `Int(t) -> t
      | _ -> 1
    in

    (* Time limit *)
    let time_limit = match f |> member "time_limit" with
      | `Float(t) -> Some(t)
      | `Int(t) -> Some(float_of_int t)
      | _ -> None
    in

    (* Find the output directory for the summary file *)
    let output_dir = match f |> member "output_dir" with
      | `String(s) -> Some(s)
      | _ -> None
    in

    (* Find the desired name, if any, for the summary file *)
    let output_file_name = match f |> member "output_name" with
      | `String(s) -> Some(s)
      | _ -> None
    in
   
    (* initialise GenProg using the configuration file settings *)
    Config.initialise cfg;

    (* load problem details, or throw error if none are provided *)
    let problem = member "problem" f |> Problem.from_json cfg in
    let program = Problem.program problem in

    (** Generate the analysis of this program *)
    let analysis = f |> member "analysis" |> Analysis.from_json program in
    let problem = Problem.with_analysis problem analysis in

    (** Load cache *)
    let cache : ResultsCache.t = match member "cache_enabled" f with
      | `Bool(true) | `Null ->
          let cache = ResultsCache.Live.load (Problem.results_cache_file_location problem) in
            ResultsCache.Live.downcast cache
      | `Bool(false) ->
          let cache = ResultsCache.Ghost.make () in
            ResultsCache.Ghost.downcast cache
      | _ -> failwith "illegal 'cache_enabled' parameter"
    in

    (** Perform sanity checking *)
    let _ = Sanity.check_exn cfg problem cache threads in

    (** Fetch the modified test suite *)
    let tests = Problem.tests problem in

    (** setup the VA table *)
    Instrument.setup_va_table_function cfg (Problem.preprocessor problem);

    (* Generate the coverage information for this problem *)
    let compiler = Problem.compiler problem in
    let exe_name = Problem.executable_name problem in
    let coverage =
      f |> member "coverage" |>
        Coverage.from_json cfg program tests exe_name compiler
    in
    let problem = Problem.with_coverage problem coverage in

    (* Compute the localisation and attach it to the problem *)
    let localisation = 
      f |> member "localisation" |> Localisation.from_json program tests coverage analysis
    in
    let problem = Problem.with_localisation problem localisation in

    (* Compute the donor pool *)
    let donor_pool =
      f |> member "donor_pool" |>
        DonorPool.from_json (Problem.analysis problem) coverage
    in
    let problem = Problem.with_donor_pool problem donor_pool in

    match !mode with
      | `Repair   ->
          repair f cfg problem cache threads seed output_dir output_file_name time_limit
      | `Diagnose ->
          diagnose f cfg problem
      | `Line ->
          find_stmts_at_stmt problem (!file_name) (!line_no)
      | `Crawl ->
          Crawler.crawl cfg problem threads
      | `Walk ->
          Walker.walk cfg problem threads time_limit
      | `Scratch ->
          scratch f cfg problem
      | `Analyse ->
          analyse f cfg problem
      | `Patch ->
          apply_patch f cfg problem !input_patch
      | `Evaluate ->
          evaluate_patch f cfg problem cache threads !input_patch
