open Solution
open Debug
open Utility
open Problem
open Test

class virtual cls_cache = object (self)
  method virtual search : Config.t -> Problem.t -> Solution.t -> Test.test_case -> Test.test_case_result option
  method virtual store : Config.t -> Problem.t -> Solution.t -> Test.test_case -> Test.test_case_result -> unit
  method virtual save : unit
end
type t = cls_cache

module Ghost = struct
  type z = t
  class cls_ghost = object (self)
    inherit cls_cache as super
    method search cfg p s t = None
    method store cfg p s t r = ()
    method save = ()
  end
  type t = cls_ghost
  let make () = new cls_ghost
  let downcast t = t
end

module Live = struct
  type z = t
  class cls_live file contents = object (self)
    inherit cls_cache as super

    method search cfg p s t = 
      let sign = Solution.signature s cfg p in
      let key = (sign, t) in
      match Hashtbl.mem contents key with
      | false -> None
      | true ->
          Some(Hashtbl.find contents key)

    method store cfg p s t r =
      let sign = Solution.signature s cfg p in
      let key = (sign, t) in
        Hashtbl.replace contents key r

    method save : unit =
      debug "writing results cache to file: %s...\n" file;
      let chan = open_out_bin file in
        Marshal.to_channel chan contents [Marshal.No_sharing];
        close_out chan;
        debug "- finished writing results cache to file.\n"
  end
  type t = cls_live

  let empty fn =
    new cls_live fn (Hashtbl.create 10000)

  let load fn =
    debug "reading results cache from file: %s...\n" fn;
    let cache =
      if Sys.file_exists fn then begin
        let chan = open_in_bin fn in
        let contents = Marshal.from_channel chan in
          close_in chan;
          debug "- finished reading results cache from file.\n";
          new cls_live fn contents
      end else begin
        debug "- no cache file found, generating empty cache instead.\n";
        empty fn
      end
    in
      cache

    let downcast cache = cache
end

let store cache cfg p s t r =
  cache#store cfg p s t r

let search cache cfg p s t =
  cache#search cfg p s t

let save cache =
  cache#save
