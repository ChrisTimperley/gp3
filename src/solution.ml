open Printf
open Utility
open Config
open Representation
open PatchRepresentation
open Problem
open Test

(** Stores the next UID *)
let next_uid : int ref = ref 0

(** Generates the next UID. *)
let generate_uid () : int =
  let uid = !next_uid in
    next_uid := uid + 1;
    uid

(**
 * This data structure is used to store information about a candidate repair
 * for a given problem.
 *
 * I suppose that we could remove "representation" and simply
 * sub-type solution.
 *)
type solution =
  {
    (** The unique ID for this solution. *)
    uid                 : int;

    (** A list holding the unique IDs of the parents of this solution. *)
    parents             : int list;

    (** Holds the representation of this solution. *)
    representation      : representation;

    (** A flag indicating whether this solution is ideal. *)
    mutable ideal       : bool;

    (** Holds the results for this solution on each test case in a map,
     *  indexed by the name of the test case. *)
    mutable results     : test_case_result StringMap.t;

    (** The signature for the program represented by this solution *)
    mutable signature   : Program.Signature.t option;

    (** The fitness of this solution.
     *  For now this is simply an optional floating point, but in the
     *  future I'll change this to be an arbitary data-type, allowing
     *  multiple-objective schemes to be implemented more easily.
     *)
    mutable fitness     : float option;

    (** The path to the directory where this solution has been compiled to. *)
    mutable compiled_to : string option;

    (** Number of seconds taken to compile this solution *)
    mutable compile_time : float option;

    (** Flag indicating whether this solution was compiled successfully *)
    mutable compile_success : bool option;

    (** Records the execution time for each of test case executions by this
     *  candidate solution *)
    mutable execution_times : float StringMap.t;
  }

type t = solution

let results sol = sol.results
let fitness sol = sol.fitness
let compiled_to sol = sol.compiled_to
let compile_time sol = sol.compile_time
let compile_success sol = sol.compile_success

let failed_to_compile sol = match sol.compile_success with
  | Some(f) -> (not f)
  | None -> false

let was_compiled_successfully sol = match sol.compile_success with
  | Some(true) -> true
  | _ -> false

let execution_times sol = sol.execution_times
let representation sol = sol.representation

let edits sol = Representation.fixes sol.representation

let num_negatives_passed sol =
  StringMap.fold (fun t r c ->
    match ((Test.test_case_from_name t), (Test.passed r)) with
    | (Negative(_), true) -> c + 1
    | _ -> c
  ) sol.results 0

(** Determines whether this solution passes all of its covering positive tests
 *  and that it compiled successfully *)
let passes_all_positives sol =
  if (failed_to_compile sol) then
    false
  else
    StringMap.for_all (fun t r ->
      match (Test.test_case_from_name t) with
      | Negative(_) -> true
      | Positive(_) -> (Test.passed r)
    ) sol.results

(** Returns the size of this solution *)
let size sol =
  Representation.size sol.representation

(** Returns a list of the SIDs modified by this solution *)
let modified_statement_ids sol =
  Representation.modified_statement_ids sol.representation

(**
 * Returns the result of a given test case execution for this solution, if
 * known. If this result isn't known, None is returned.
 *)
let test_result sol test =
  let name = Test.test_case_name test in
  match StringMap.mem name sol.results with
  | false -> None
  | true ->
      let res = StringMap.find name sol.results in
        Some(Test.passed res)

(**
 * Returns the set of tests covered by the changes in this solution, with
 * respect to its associated problem. If no coverage information is known
 * for the given problem, then the set of all tests will be returned. *)
let covered_tests sol prob =
  match (Problem.has_coverage prob) with
  | false -> Problem.tests prob
  | true -> begin
      let coverage = Problem.coverage prob in
      let modified_sids = modified_statement_ids sol in
        Coverage.tests_covered_by_statements coverage modified_sids
  end

(**
 * Returns the set of tests for which a test case result is not known for
 * a given solution. Excludes redundant tests.
 *)
let remaining_tests sol prob =
  let tests = covered_tests sol prob in
  let executed = List.map fst (StringMap.bindings (results sol)) in
  let executed = List.map Test.test_case_from_name executed in
  let executed = Test.TestSet.of_list executed in
    Test.TestSet.diff tests executed

(** Adds the result of a test case execution to this solution *)
let add_result sol test_name res time =
  sol.execution_times <- StringMap.add test_name time sol.execution_times;
  sol.results <- StringMap.add test_name res sol.results

(** Determines whether a given solution is an ideal solution to the provided
 *  problem *)
let is_ideal prob sol =
  let covered_tests = covered_tests sol prob in
  if (TestSet.diff (Problem.negative_tests prob) covered_tests) != TestSet.empty then
    false
  else
    TestSet.for_all (fun t ->
      let tn = Test.test_case_name t in
      if not (StringMap.mem tn sol.results) then false
      else
        Test.test_case_passed (StringMap.find tn sol.results)
    ) covered_tests  

  (*
  (StringMap.cardinal sol.results) = (Problem.num_tests prob) &&
    StringMap.for_all (fun _ r -> (Test.passed r)) sol.results
  *)

(** Determines whether a given solution is a proto-offspring. *)
let is_proto_offspring s =
  s.results = StringMap.empty

(** Builds a new solution from a given representation, with a given list of
 *  the UIDs of its parents.
 *  TODO: REPLACE WITH MAKE *)
let new_solution (r : representation) (parents : int list) : solution =
  { uid = (generate_uid ());
    parents = parents;
    representation = r;
    ideal = false;
    signature = None;
    results = StringMap.empty;
    fitness = None;
    compiled_to = None;
    compile_time = None;
    compile_success = None;
    execution_times = StringMap.empty }

let make rep =
  new_solution rep []

(** Returns a short string describing the genome of a given solution. *)
let describe_genome (s : solution) : string =
  match s.representation with
  | Patch(fixes) -> patch_to_short_string fixes

(** Assigns a fitness value to a given candidate solution. *)
let assign_fitness (candidate : solution) (v : float) : unit =
  candidate.fitness <- Some v

(** Compiles a provided candidate solution for a given problem to a randomly
 *  generated temporary directory. Returns the path to the directory where the
   solution was compiled to. *)
let compile ?(force = false) solution problem =
  match (force, solution.compiled_to) with
  | (true, _) | (_, None) -> begin
    (*let _ = Debug.debug "compiling solution: %s\n" (describe_genome solution) in*)
    let dir = mktmpdir () in
    let (success, time) =
      Representation.compile solution.representation problem dir
    in
      solution.compile_success <- Some(success);
      solution.compile_time <- Some(time);
      solution.compiled_to <- Some(dir);
      dir
  end
  | (_, Some dir) -> dir

(** Treats a given solution as if it were never compiled; useful for
 * multi-line problems. *)
let uncompiled s =
  s.compiled_to <- None

(**
 * Checks whether a given solution has been compiled.
 *)
let is_compiled s =
  match s.compiled_to with
  | None -> false
  | Some _ -> true

(**
 * Called immediately before a given solution is removed the population.
 *)
let destroy s =
  match s.compiled_to with
  | None -> ()
  | Some dir ->
    let _ = system (Printf.sprintf "rm -rf %s" dir) in
      ()

(**
 * Compares two fitness values and returns an integer indicating which of the
 * two is better, or if they are equal.
 *
 * @return  If x is better than y, -1 is returned.
 *          If y is better than x, 1 is returned.
 *          If x and y are equal, 0 is returned.
 *)
let fitness_compare (x : float) (y : float) : int =
  if x == y then
    0
  else if x > y then
    -1
  else
    1

(**
 * Compares two solutions and returns an integer indicating which of the two
 * is better, or whether they are equal.
 *
 * @return  If x is better than y, -1 is returned.
 *          If y is better than x, 1 is returned.
 *          If x and y are equal, 0 is returned.
 *)
let solution_compare (x : solution) (y : solution) : int =
  let fx = match x.fitness with
    | Some f -> f
    | _ -> failwith "Failed to compare solutions: x has no fitness."
  in
  let fy  = match y.fitness with
    | Some f -> f
    | _ -> failwith "Failed to compare solutions: y has no fitness."
  in
    fitness_compare fx fy
let compare x y = solution_compare x y

(** Returns (one of) the best solution(s) from a list of candidates. *)
let best_solution (sl : solution list) : solution =
  let rec aux (best : solution) (rest : solution list) =
    match rest with
    | [] -> best
    | hd::tl -> begin
      let best = if (solution_compare best hd) == 1 then hd else best in
        aux best tl 
    end
  in
  match sl with
  | hd::tl -> aux hd tl
  | [] -> failwith "Failed to find best solution from list: empty list provided."

(** Returns a JSON description of a given fitness. *)
let fitness_to_json (f : float option) : string =
  match f with
  | Some x -> string_of_float x
  | None -> ""

let short_desc s =
  let f = match s.fitness with
    | Some x -> Printf.sprintf "%.2f" x
    | None -> "no fitness"
  in
  let c = match compile_success s with
    | Some(true) -> "compiled"
    | Some(false) -> "failed to compile"
    | None -> "uncompiled"
  in
    Printf.sprintf "[%s] (%s: %s)" (describe_genome s) c f

(** Returns a JSON document, describing a given solution *)
let to_json sol =
  let results_to_json results =
    let results =
      List.map (fun (test, res) ->
        let res = match res with
          | Passed -> `Bool(true)
          | Failed -> `Bool(false)
        in
          (test, res)
      ) (StringMap.bindings results)
    in
      `Assoc(results)
  in
  let fitness_to_json fitness =
    match fitness with
      | Some(x) -> `Float(x)
      | None -> `Null
  in
  let compile_time_to_json time =
    match time with
      | Some(t) -> `Float(t)
      | None -> `Null
  in
  let compile_success_to_json success =
    match success with
      | Some(b) -> `Bool(b)
      | None -> `Null
  in
  let execution_times_to_json times =
    let times =
      List.map (fun (tn, t) -> (tn, `Float(t))) (StringMap.bindings times)
    in
      `Assoc(times)
  in
    `Assoc([
      ("fitness", (fitness_to_json sol.fitness));
      ("genome", `String((describe_genome sol)));
      ("results", (results_to_json sol.results));
      ("compilation_time", (compile_time_to_json sol.compile_time));
      ("compilation_success", (compile_success_to_json sol.compile_success));
      ("execution_times", (execution_times_to_json sol.execution_times))
    ])

(** Returns a JSON description of a given set of results. *)
let results_to_json (results : test_case_result StringMap.t) : string =
  let flat = StringMap.fold (fun tn r flat ->
    let passed = test_case_passed r in
    (sprintf "'%s':%b" tn passed)::flat
  ) results [] in
    "{" ^ (String.concat "," flat) ^ "}"

(** Returns a JSON description of a given list of parents. *)
let parents_to_json (parents : int list) : string =
  "[" ^ (String.concat "," (List.map string_of_int parents)) ^ "]"

(**
 * Returns a string-based hash of a given solution, in its canonical form.
 * By first normalising the given solution, we reduce the number of results
 * cache misses.
 *)
let hash solution problem =
  Representation.hash solution.representation problem

(** Returns the program represented by this solution *)
let to_program solution problem =
  Representation.to_program solution.representation problem

(** Returns the signature for a given solution *)
let signature solution cfg problem =
  match solution.signature with
  | None -> begin
      let signature = Program.signature (to_program solution problem) cfg in
        solution.signature <- Some(signature);
        signature
    end
  | Some(signature) -> signature
