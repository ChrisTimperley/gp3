(**
 * This file implements the original GenProg SID-based operations on ASTs,
 * consisting of deletion, replacement and append.
 *
 * SIDs are stripped from the donor code, prior to insertion.
 *
 * Q: Why do we care so much about labels?
 *)
open Utility

(** Gets the file from a given program that an SID belongs to, using a provided
 *  analysis *)
let get_file analysis program sid =
  let info = Analysis.stmt_info analysis sid in
  let file = Analysis.Stmt.file_name info in
  let file = Program.file program file in
  match file with
    | Some(f) -> f
    | None -> failwith "failed to find program file for SID: %d\n" sid

(** Prepares a given donor statement, taken from the Analysis, for insertion
 *  into the program, by creating a deep clone, with ALL SIDs removed, including
 *  those of its children. *)
let prepare_donor donor =
  let open Cil in
  let donor = Utility.copy donor in
  let visitor = object
    inherit nopCilVisitor
    method vstmt stmt =
        stmt.sid <- -1;
        DoChildren
  end in
  let _ = visitCilStmt visitor donor in
    donor

let replace analysis program sid replacement =
  let open Cil in
  let replacement = prepare_donor replacement in
  let visitor = object
    inherit nopCilVisitor
    method vstmt stmt =
      if stmt.sid = sid then begin
        stmt.sid <- -1; (* does this correct the issue? *) 
        ChangeTo(replacement)
      end else DoChildren
  end in
  (* we could be even faster by fetching the function by its VID *)
  let file = get_file analysis program sid in
    ignore(Program.File.visit file visitor)
  
let append analysis program sid donor =
  let open Cil in
  let donor = prepare_donor donor in
  let visitor = object
    inherit nopCilVisitor
    method vstmt stmt =
      let replacement = mkStmt (Block(mkBlock [stmt; donor])) in
      let replacement = { replacement with sid = -1 } in
      if stmt.sid = sid then ChangeTo(replacement) else DoChildren
  end in
  (* we could be even faster by fetching the function by its VID *)
  let file = get_file analysis program sid in
    ignore(Program.File.visit file visitor)

let delete analysis program sid =
  let placeholder = Cil.mkEmptyStmt () in
    replace analysis program sid placeholder

(*
class insertionVisitor
  (update : (bool -> stmt -> stmt -> stmt) option) (do_append : bool) =
object
  inherit nopCilVisitor

  val write_to =
    fun lbls _ info -> lbls := StringSet.union !lbls info.decl_labels

  method vfunc fd =
    labels := StringSet.empty ;
    let _ = visitCilFunction (new labelVisitor reader (write_to labels)) fd in
      DoChildren

  method private prepare_to_insert_subtree old_s new_s =
    let get_stmt_labels s =
      let lbls = ref StringSet.empty in
      let _ = visitCilStmt (new labelVisitor reader (write_to lbls)) s in
        !lbls
    in
    let old_labels = get_stmt_labels old_s in
    let used_labels =
      if do_append then begin
        !labels
      end else
        StringSet.diff !labels old_labels
    in
    let new_s = visitCilStmt (new labelRenameVisitor used_labels) (copy new_s) in
    let new_s =
      match update with
      | Some(f) -> f do_append old_s new_s
      | _       -> new_s
    in
      if not do_append then begin
        let kept_labels =
          StringSet.fold (fun s lbls ->
            Label(s, get_stmtLoc old_s.skind, true) :: lbls
          ) (StringSet.diff old_labels (get_stmt_labels new_s)) []
        in
          new_s.labels <- (List.rev kept_labels) @ new_s.labels
      end ;
      new_s
end

(** Appends a given donor to a provided program after the statement
 *  occuring at a specified SID. *)
let append_at
  ?(update=None)
  (info : atom_info IntMap.t)
  (program : Program.t)
  (sid : int)
  (donor : Cil.stmt)
: unit =
  (* Find the file and function that the SID belongs to. *)
  let sinfo = IntMap.find sid info in
  let file = sinfo.in_file in
  let file = match Program.file program sinfo.in_file with
    | Some(f) -> f
    | None -> failwith (Printf.sprintf "failed to find matching file (%s) for SID %d" file sid)
  in

  (* Generate the visitor to perform the operation. *)
  let visitor = object
    inherit insertionVisitor update true as super
    method vstmt s =
      if sid <> s.sid then
        DoChildren
      else
        ChangeDoChildrenPost(s, fun s ->
          let donor = super#prepare_to_insert_subtree s donor in
          let both = Cil.mkStmt (Block(mkBlock [s; donor])) in
            both.sid <- 0;
            both
        )
  end in
    ignore(Program.File.visit file visitor) 

(** Replaces the statement at a given SID in a provided program a given
 *  donor statement. *)
let replace_at
  ?(update=None)
  (info : atom_info IntMap.t)
  (program : Program.t)
  (sid : int)
  (donor : Cil.stmt)
: unit =
  (* Find the file that the SID belongs to. *)
  (* TODO: Fold this into Analysis module *)
  let sinfo = IntMap.find sid info in
  let file = sinfo.in_file in
  let file = match Program.file program sinfo.in_file with
    | Some(f) -> f
    | None -> failwith (Printf.sprintf "failed to find matching file (%s) for SID %d" file sid)
  in

  (* Generate the visitor to perform the replacement. *)
  let visitor = object
    inherit insertionVisitor update false as super
    method vstmt s =
      if sid <> s.sid then
        DoChildren
      else begin
        let s' = super#prepare_to_insert_subtree s donor in
          ChangeTo(s')
      end
  end in
    ignore(Program.File.visit file visitor)

(** Destroys all statements at a given SID for a provided program.
 *  Rather than actually removing the SID from the program, the statement at
 *  that address is replaced with an empty statement. *)
let delete_at
  (info : atom_info IntMap.t)
  (program : Program.t)
  (sid : int)
: unit =
  ignore(replace_at info program sid (Cil.mkEmptyStmt ()))
*)
