(*
 * TODO: Make sure that channels are safely closed!
 *)
open Termination
open Problem
open Test
open Utility
open Debug

(** Define an arbitary ordering over test cases. *)
module OrderedCacheEntry =
struct
  type t = string * test_case

  let make solution test problem =
    let hashed = Solution.hash solution problem in
      (hashed, test)

  let compare (e1 : t) (e2 : t) =
    let (e1_name, e1_test) = e1 in
    let (e2_name, e2_test) = e2 in
    match compare e1_name e2_name with
    | 0 -> OrderedTest.compare e1_test e2_test
    | res -> res
end
module CacheEntrySet = Set.Make(OrderedCacheEntry)

type t = {
  mutable visited : CacheEntrySet.t; (** visited test case / genome pairs *)
  mutable visited_patches : StringSet.t;
  mutable num_visited : int; (** no. unique, visited sites *)
  mutable evaluations : int; (** no. unique, test case evaluations *)
  mutable cache_hits : int; (** no. cache hits *)
  limit : int option; (** max. no. unique test case evaluations *)

  cfg : Config.t;
  problem : Problem.t;

  (** When enforcing the evaluation limit, should we look at the number of
   *  visited sites, or the number of first-time test case evaluations (i.e.
   *  to exclude precached evaluations). *)
  count_precached : bool;

  in_parallel : int; (* how many jobs may be run in parallel? *)
  parallel_batches : bool; (* may different batches be executed in parallel? *)
  terminate_early : bool;
  strict : bool; (* terminates batch on first failure *)
  cache : ResultsCache.t;

  allow_extension : bool; (* should we extend a batch if all tests are successful? *)
}
type ev = t

(**
 * Batches are used to hold together groups of test cases for a single
 * candidate solution. Useful for determining when we should extend a partial
 * test suite evaluation to a full test suite evaluation for a particular
 * candidate.
 *)
module Batch = struct
  type t = {
    solution : Solution.t;
    tests : Test.test_case list;
    mutable processes : int list;
    mutable remaining : int;
    mutable passed : bool
  }

  let candidate batch = batch.solution
  let passed batch = batch.passed
  let remaining batch = batch.remaining
  let tests batch = batch.tests

  let name batch = Solution.describe_genome (candidate batch)

  (* Called after a process has finished *)
  let processed batch pid =
    batch.processes <- List.filter (fun p -> p != pid) batch.processes

  (* Called when a process has begun *)
  let on_process_start batch pid =
    batch.processes <- pid::batch.processes

  (** Called after a job has finished (in any way) *)
  let notify batch result =
    batch.passed <- batch.passed && result;
    batch.remaining <- batch.remaining - 1

  (** Called when a batch is started *)
  let start batch num_jobs =
    (*debug "** Starting batch: %s (%d jobs)\n" (name batch) num_jobs;*)
    batch.remaining <- num_jobs

  (** Called when a batch is extended *)
  let extend batch num_jobs =
    batch.remaining <- batch.remaining + num_jobs

  (* Cancels all jobs, running and queued, in the batch *)
  let cancel batch processes running =
    let running = running - (List.length batch.processes) in
    let processes =
      List.fold_left (fun processes pid ->
        IntMap.remove pid processes 
      ) processes batch.processes
    in
      (* kill and collect processes *)
      List.iter kill_process batch.processes;
      List.iter (fun pid -> ignore(Unix.waitpid [] pid)) batch.processes;
      batch.processes <- [];
      batch.remaining <- 0;
      batch.passed <- false; (* ensure! *)
      (processes, running)

  (* Constructs a new batch, automatically removing any redundant tests *)
  let make prob candidate tests =

    (* fetch the set of known results for this candidate and determine whether
     * all executed tests have thus far been successful *)
    let existing_results = StringMap.bindings (Solution.results candidate) in
    let (executed, passing) =
      List.fold_left (fun (executed, passing) (name, res) ->
        let executed = Test.TestSet.add (Test.test_case_from_name name) executed in
          (executed, (passing && (Test.passed res)))
      ) (Test.TestSet.empty, true) existing_results
    in

    (* remove previously executed tests from the batch *)
    let tests = List.filter (fun t -> not (Test.TestSet.mem t executed)) tests in

    (* remove redundant test case executions from the batch *)
    let covered_tests = Solution.covered_tests candidate prob in
    let tests = List.filter (fun t -> Test.TestSet.mem t covered_tests) tests in
      { solution = candidate;
        tests = tests;
        remaining = 0;
        processes = [];
        passed = passing }
end

(** Returns number of cache hits made by this evaluator *)
let cache_hits ev = ev.cache_hits

(**
 * Returns the number of evaluations performed by the evaluator, according to
 * its counting policy.
 *)
let evaluations ev =
  if ev.count_precached then ev.num_visited else ev.evaluations

(**
 * Determines whether all resources available to this evaluator have been
 * exhausted. If so, no further work will be done by the evaluator.
 *)
let exhausted ev running =
  match ev.limit with
  | None      -> false
  | Some(lim) -> ((evaluations ev) + running) >= lim

(**
 * Called whenever a particular (solution, test case) pair is visited by the
 * evaluator. Used to update the count and set of visited (canonical) pairs.
 *)
let visit ev stats cand test res prob =
  let cand_hash = (Solution.hash cand prob) in
  let entry = OrderedCacheEntry.make cand test prob in
  let _ = match StringSet.mem cand_hash ev.visited_patches with
    | true -> ()
    | false -> (ev.visited_patches <- StringSet.add cand_hash ev.visited_patches)
  in
  match CacheEntrySet.mem entry ev.visited with
  | true -> stats
  | false ->
      let stats = Test.Statistics.update stats test res in
        ev.num_visited <- ev.num_visited + 1;
        ev.visited <- CacheEntrySet.add entry ev.visited;
        stats

(**
 * An evaluation job represents the evaluation of a single test case for a
 * given candidate solution. Each job belongs to a batch.
 *)
module Job = struct
  type t = {
    batch : Batch.t;
    test : test_case;
    mutable start_t : float option
  }

  let batch job = job.batch
  let test job = job.test
  let candidate job = Batch.candidate (batch job)
  let name job =
    let genome = Solution.describe_genome (candidate job) in
    let test_name = Test.test_case_name (test job) in
      Printf.sprintf "%s [%s]" genome test_name

  (** Constructs a new job using the provided parameters *)
  let make batch test = { batch = batch; test = test; start_t = None }

  (* begin executing job, return process identifier *)
  let start job ev prob =
    let _ = job.start_t <- Some(Unix.gettimeofday ()) in
    let cand = candidate job in
    let compiled_to = Solution.compile cand prob in
    let exe_name = prob.executable_name in
    let cmd = Test.command (test job) ev.cfg exe_name compiled_to in
    (*debug ">> starting job: %s\n" (name job);*)
    let p =
      popen ~stdout:(UseDescr(dev_null)) ~stderr:(UseDescr(dev_null))
        "/bin/bash" ["-c"; cmd]
    in
    let pid = p.pid in
      Batch.on_process_start (batch job) pid;
      pid

  (* called when job result is found in cache, and after processing results of
   * command-line execution *)
  let cached job ev prob result =
    let time = match job.start_t with
      | Some(t) -> (Unix.gettimeofday ()) -. t
      | None -> 0.0
    in
    let test_name = test_case_name (test job) in
    let batch = batch job in
    let candidate = candidate job in
    let result_bool = match result with
      | Passed -> true
      | Failed -> false
    in
      (*debug "-- executed %s: %B\n" test_name result_bool;*)
      Solution.add_result candidate test_name result time;
      Batch.notify batch result_bool

  (* process results of job *)
  let finish job ev prob pid result =
    Batch.processed (batch job) pid;
    ResultsCache.store ev.cache ev.cfg prob (candidate job) (test job) result;
    cached job ev prob result
end

module Queue = struct
  type t = {
    batches : Batch.t list;
    jobs : Job.t list;
    processes : Job.t IntMap.t;
    running_jobs : int;
    running_batches : int;
    stats : Test.Statistics.t;
    evaluator : ev; (* hopefully this should work? *)
  } 

  let hit_job_limit q = q.running_jobs >= q.evaluator.in_parallel
  let hit_batch_limit q = q.evaluator.parallel_batches && q.running_batches > 0

  (** Checks if there are any pending jobs or batches in the queue *)
  let any_pending_jobs q = match (q.jobs, q.batches) with
    | ([], []) -> false
    | _ -> true

  (** Adds all jobs for a given batch to the job queue *)
  let start_batch q b =
    let cand = Batch.candidate b in
    let tests = Batch.tests b in

    (* transform each test into a job *)
    let jobs =
      List.map (fun t -> Job.make b t) tests
    in
    let num_jobs = List.length jobs in
    let _ = Batch.start b num_jobs in
      jobs

  (** Extends a given (running) batch with a list of additional tests *)
  let extend_batch q b tests =
    let jobs = List.map (fun t -> Job.make b t) tests in
    let _ = Batch.extend b (List.length tests) in
      jobs

  (** Called after all jobs within a batch have finished executing *)
  let on_batch_finished q batch prob =
    (*debug "finished batch: %s\n" (Batch.name batch);*)
    let candidate = Batch.candidate batch in
    let extension = Solution.remaining_tests candidate prob in
    let extension = Test.TestSet.elements extension in
    match ((Batch.passed batch), extension, q.evaluator.terminate_early) with
    | (true, [], true) ->
        ({ q with running_batches = q.running_batches - 1}, Some(FoundAcceptableSolution(candidate)))
    | (true, ext, _) ->
	(*debug "extending batch...\n";*)
        ({ q with jobs = ((extend_batch q batch ext) @ q.jobs) }, None)
    | _ -> (* make sure to "uncompile" the candidate if necessary *)
        if not q.evaluator.parallel_batches then (Solution.uncompiled candidate);
        ({ q with running_batches = q.running_batches - 1 }, None)

  (** Called after a job has finished, whether through the return of a result
   * from the command line, or a hit in the results cache. *)
  let on_job_finished q job result =
    let batch = Job.batch job in
    let candidate = Job.candidate job in

    (*debug "finished job: %s\n" (Job.name job);*)
    (*debug "-- remaining: %d\n" (Batch.remaining batch);*)

    let q = { q with
      stats = visit q.evaluator q.stats candidate (Job.test job) result q.evaluator.problem
    } in
    match ((Batch.remaining batch), (Batch.passed batch), q.evaluator.strict) with

    (* has the batch finished executing? *)
    | (0, _, _) -> on_batch_finished q batch q.evaluator.problem

    (* was the test a failure, and if so, should we cancel the batch? *)
    | (_, false, true) ->
      (*debug "DITCH THE BATCH: %s\n" (Batch.name batch);*)
      let (processes, running_jobs) =
        Batch.cancel batch q.processes q.running_jobs
      in
      let q = { q with
        running_jobs = running_jobs;
        processes = processes;
        running_batches = q.running_batches - 1
      } in
        (q, None)

    (* no need to do anything special *)
    | _ -> (q, None)


  (** Executes next job in the queue, returning the remaining queue *)
  let next q =
    match q.jobs with
    | [] -> (q, None)          (* no jobs left *)
    | job::rst -> begin        (* execute next job *)
      let q = { q with jobs = rst } in
      let candidate = Job.candidate job in
      let batch = Job.batch job in
      let test = Job.test job in
      let lookup =
        ResultsCache.search q.evaluator.cache q.evaluator.cfg q.evaluator.problem candidate test
      in
      match ((exhausted q.evaluator q.running_jobs), lookup, (Batch.passed batch), q.evaluator.strict) with

      (* evaluation limit; don't "terminate" here - still jobs running
       * NOTE: we implement a HARD evaluation limit here, despite the fact there
       *  may still be jobs we can execute, due to their results residing in the
       *  cache. *)
      | (true, _, _, _) -> ({ q with jobs = [] }, None)

      (* batch abandoned *)
      | (_, _, false, true) -> (q, None)

      (* cache hit *)
      | (_, Some(result), _, _) ->
        (*debug "cache hit!\n";*)
        let _ = Job.cached job q.evaluator q.evaluator.problem result in
        let _ = q.evaluator.cache_hits <- q.evaluator.cache_hits + 1 in
          on_job_finished q job result

      (* execute next job; ensure solution is compiled *)
      | _ ->
        let _ = Solution.compile candidate q.evaluator.problem in
        match Solution.compile_success candidate with
        | None | Some(false) -> begin
            let _ = Debug.debug "failed to compile, cancelling batch: %s\n" (Solution.describe_genome candidate) in
            let (processes, running_jobs) =
              Batch.cancel batch q.processes q.running_jobs
            in
            let q =
              { q with  running_jobs = running_jobs;
                        processes = processes;
                        running_batches = q.running_batches - 1 }
            in
              (q, None)
          end
        | Some(true) -> begin
          let pid = Job.start job q.evaluator q.evaluator.problem in
          let _ = q.evaluator.evaluations <- q.evaluator.evaluations + 1 in
            ({ q with
                running_jobs = q.running_jobs + 1;
                processes = IntMap.add pid job q.processes
              }, None)
          end
    end

  (** Runs all jobs in the queue until termination *)
  let rec run q terminate =
    match terminate with
    | Some(t) -> (Some(t), q.stats)
    | None -> 
      begin match (q.jobs, q.batches, q.running_jobs) with
      (* have we finished processing all the jobs in the queue, and do we have
	 no jobs left running? *)
      | ([], [], 0) ->
          let terminate =
            if exhausted q.evaluator 0 then Some(ReachedEvaluationLimit) else None
          in
            (terminate, q.stats)

      (* are there any jobs left on the queue, can we run another in parallel? *)
      | (nxt_job::rst_jobs, _, _) when not (hit_job_limit q) ->
          (*debug "Let's get another job going!\n";*)
          let (q, terminate) = next q in
            run q terminate

      (* if we've run out of jobs for this batch, can we get another batch going? *)
      (* don't increase the number of running batches if there are no jobs, otherwise
       * the code will wait for a job that doesn't exist to finish executing *)
      | ([], nxt_batch::rst_batch, _) when not (hit_batch_limit q) ->
        let jobs = start_batch q nxt_batch in
        let batch_increment = match jobs with
          | [] -> 0
          | _ -> 1
        in
        let q = { q with
            jobs = jobs;
            batches = rst_batch;
            running_batches = q.running_batches + batch_increment
        } in
          run q terminate

      (* wait for the next job to finish executing *)
      | _ -> begin
        (*Debug.debug "(%d, %d, %d)\n" (List.length q.jobs) (List.length q.batches) q.running_jobs;*)
        (*debug "waiting for job to finish\n";*)
        match Unix.wait () with
        | (pid, status) ->
          let job = IntMap.find pid q.processes in
          let result = Test.read_test_case_result status in
          let _ = Job.finish job q.evaluator q.evaluator.problem pid result in
          let q = { q with
            processes = IntMap.remove pid q.processes;
            running_jobs = q.running_jobs - 1
          } in
          let (q, terminate) = on_job_finished q job result in
            run q terminate
        end
    end

  (** Runs a process queue until termination *)
  let execute q = run q None

  (** Constructs an evaluation queue from a given list of candidates *)
  let make ev stats sol_tests =
    let batches =
      List.map (fun (sol, tests) -> Batch.make ev.problem sol tests) sol_tests
    in
      {
        batches = batches;
        jobs = [];
        processes = IntMap.empty;
        running_jobs = 0;
        running_batches = 0;
        stats = stats;
        evaluator = ev
      } 
end

let num_patches ev =
  StringSet.cardinal ev.visited_patches

let limit ev = ev.limit

let reset ev =
  ev.evaluations <- 0;
  ev.cache_hits <- 0;
  ev.num_visited <- 0;
  ev.visited <- CacheEntrySet.empty;
  ev.visited_patches <- StringSet.empty

let evaluate ev stats sol_tests =
  let q = Queue.make ev stats sol_tests in
    Queue.execute q

let summarise ev =
  `Assoc([
    ("patches", `Int(num_patches ev));
    ("evaluations", `Int(ev.evaluations));
    ("visited", `Int(ev.num_visited));
    ("cache_hits", `Int(ev.cache_hits));
    ("pre_cache_hits", `Int(ev.num_visited - ev.evaluations))
  ])

let make cfg problem limit in_parallel terminate_early count_precached strict allow_extension cache =
  { evaluations = 0;
    problem = problem;
    cfg = cfg;
    num_visited = 0;
    cache_hits = 0;
    allow_extension = allow_extension;
    visited = CacheEntrySet.empty;
    visited_patches = StringSet.empty;
    limit = limit;
    in_parallel = in_parallel;
    parallel_batches = Problem.multi_file problem;
    cache = cache;
    terminate_early = terminate_early;
    count_precached = count_precached;
    strict = strict }

let from_json cfg problem cache threads def =
  debug "-- constructing evaluator\n";
  let open Yojson.Basic.Util in
  let def = match def with
    | `Null -> `Assoc([])
    | _ -> def
  in
  let terminate_early = match def |> member "terminate_early" with
    | `Bool(b) -> b
    | _ -> true
  in
  let limit = match def |> member "limit" with
    | `Int(n) -> Some(n)
    | _ -> None
  in
  let count_precached = match def |> member "count_precached" with
    | `Bool(b) -> b
    | _ -> true
  in
  let strict = match def |> member "strict" with
    | `Bool(b) -> b
    | _ -> false
  in
  let allow_ext = match def |> member "allow_extension" with
    | `Bool(b) -> b
    | _ -> true
  in
    make cfg problem limit threads terminate_early count_precached strict allow_ext cache
