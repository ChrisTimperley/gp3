open Utility

type t = Solution.t list

let members pop = pop

let make members = members

let empty () = []

let to_json pop =
  `List(List.map Solution.to_json (members pop))

let debug pop =
  List.iter (fun s ->
    Printf.printf "%s\n" (Solution.short_desc s)
  ) pop

let rec clones base num =
  let rec aux pop num =
    match num with
    | 0 -> pop
    | _ -> aux ((copy base)::pop) (num - 1)
  in
    aux (empty ()) num

let add pop ind = ind::pop

let mutate pop mut = make (mut pop)

let crossover pop crs = make (crs pop)

let select pop sel num = make (Selection.select sel pop num)

let replace pop repl offspring =
  make (Replacement.replace repl pop offspring)

let destroy pop =
  List.iter Solution.destroy (members pop)
