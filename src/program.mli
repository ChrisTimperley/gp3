type t

(** Used to perform compilation of programs *)
module Compiler : sig
  type z = t
  type t

  (** Compiles a given program using this compiler and returns the success of
   *  the process, together with the number of seconds taken to complete, as
   *  a tuple. *)
  val compile : t -> z -> string -> (bool * float)

  (** Loads a compiler from a JSON configuration *)
  val from_json : Config.t -> Yojson.Basic.json -> t

  (** Implements the compilation behaviour of the older versions of GenProg *)
  module Legacy : sig
    type w = t
    type t
    val make : string -> string -> string -> string -> Config.t -> t
    val from_json : Config.t -> Yojson.Basic.json -> t
    val downcast : t -> w
  end

  (** Implements a newer, simpler compilation behaviour, where compilation is
   *  taken care of by a single script, which is given the location of the
   *  source code directory
  module Simple : sig
    type t
    val from_json : Config.t -> Yojson.json -> t
  end
  *)
end

(** Represents a single file within a given program *)
module File : sig
  type t

  (** Constructs a file from a string containing its relative path and a given
   *  CIL file *)
  val make : string -> Cil.file -> t

  (** Returns the relative path for this file *)
  val relative_path : t -> string

  (** Returns this file as a Cil.file *)
  val as_cil_file : t -> Cil.file

  (** Returns a list of all the statements within this file *)
  val stmts : t -> Cil.stmt list

  (** Returns a list of all the variables within this file *)
  val variables : t -> Cil.varinfo list

  (** Returns a list of all the functions within this file *)
  val functions : t -> Cil.fundec list

  (** Returns the starting and ending line number for the body of a given
   *  function within this file *)
  val function_bounds : t -> Cil.fundec -> int * int

  (** Returns the function to which a given line belongs *)
  val function_of_line_no : t -> int -> Cil.fundec

  (** Returns a list of all statements belonging to a given function *)
  val stmts_at_function : t -> Cil.fundec -> Cil.stmt list

  (** Returns a list of all statements at a given line no *)
  val stmts_at_line_no : t -> int -> Cil.stmt list

  (** Visits the CIL file for this file, keeping the same globals. Returns
   *  the same file. *)
  val visit : t -> Cil.cilVisitor -> t

  (** Writes the contents of this file to a specified location on disk. Ensures
   *  that all parent directories exist. *)
  val write_to : t -> Config.t -> string -> unit

  (** Pretty-prints a given file to a string *)
  val to_s : t -> Config.t -> string

  (** Produces a deep clone of a given file *)
  val copy : t -> t
end

(** Program manifests are used to specify the location of a program, and their
 *  constituent source code files, relative to a base path. *)
module Manifest : sig
  type t

  (** Constructs a manifest from a given base path and a list of paths
   *  relative to that base for each source code file. *)
  val make : string -> string list -> t

  (** Constructs a manifest from a manifest file at a given location *)
  val read : Config.t -> string -> t

  (** Returns a count of the number of files within a given manifest. *)
  val num_files : t -> int

  (** Returns the base path of all files within the manifest. *)
  val base_path : t -> string

  (** Returns a list of the file paths within this manifest, relative to its
   *  associated base path. *)
  val relative_paths : t -> string list

  (** Returns a list of the complete file paths, prefixed by the base path *)
  val full_paths : t -> string list
end

(** Program signatures are essentially hashes used to identify equivalent
 *  programs. Exploited by the caching system.*)
module Signature : sig
  type t

  (** Generates a program signature from a list of digests *)
  val make : Digest.t list -> t

  (** Returns a list of the digests comprising this signature *)
  val digests : t -> Digest.t list
  
  (** Produces a string form of this signature *)
  val to_s : t -> string
end

(** Used to pre-process C files *)
module Preprocessor : sig
  type t

  (** Builds a pre-processor given the name of the compiler, a string of
   *  compiler options, and a pre-processing command *)
  val make : string -> string -> string -> t

  (** Preprocesses a given source file to a specified location. Returns a flag
   *  indicating the success of the preprocessing. *)
  val preprocess : t -> Config.t -> string -> string -> bool

  (** Loads a compiler from a JSON configuration *)
  val from_json : Config.t -> Yojson.Basic.json -> t
end

(** Loads a program from a given manifest *)
val load : Manifest.t -> t

(** Returns a list of the files belonging to this program *)
val files : t -> File.t list

(** Attempts to return a file with a given relative path from a given program *)
val file : t -> string -> File.t option

(** Returns a list of the names of the files belonging to this program *)
val file_names : t -> string list

(** Returns a variant of a given program with a given set of files *)
val with_files : t -> File.t list -> t

(** Returns the base path of this program *)
val base_path : t -> string

(** Computes the signature for a given program *)
val signature : t -> Config.t -> Signature.t

(** Visits all files within this program using a given visitor *)
val visit : t -> Cil.cilVisitor -> unit

(** Returns a list of all the variables within this program *)
val variables : t -> Cil.varinfo list

(** Returns a list of all the functions within this program *)
val functions : t -> Cil.fundec list

(** Returns a list of all the statements within this program.
 *  Prefer Analysis.stmts where possible; no caching. *)
val stmts : t -> Cil.stmt list

(** Returns a list of the SIDs of all the statements within this program.
 *  Prefer Analysis.sids where possible; no caching *)
val sids : t -> int list

(** Returns the size of a given program, in terms of its number of statements *)
val size : t -> int

(** Produces a deep copy of a given program *)
val copy : t -> t

(** Writes the source code for this program to a given directory *)
val write_to : t -> Config.t -> string -> unit
