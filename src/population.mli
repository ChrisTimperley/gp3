type t

(** Returns a list of the members of this population *)
val members : t -> Solution.t list

(** Creates and returns a new empty population *)
val empty : unit -> t

(** Creates a new population, consisting of a given list of individuals *)
val make : Solution.t list -> t

(** Produces a JSON document describing a given population *)
val to_json : t -> Yojson.Basic.json

val debug : t -> unit

(** Generates a population containing N deep clones of a given individual *)
val clones : Solution.t -> int -> t

(** Non-destructively mutates all individuals in the population using a given
 *  mutator, generating a new population *)
val mutate : t -> Mutation.t -> t

(** Non-destructively crosses over all individuals in the population using a
 *  provided crossover method, generating a new population *)
val crossover : t -> Crossover.t -> t

(** Generates a new population by selecting a specified number of individuals
 * from the given population using a provided selection method *)
val select : t -> Selection.t -> int -> t

(** Non-destructively replaces the contents of this population with their
 *  offspring, according to a given replacement scheme. Returns both the
 *  surviving population and the discarded individuals *)
val replace : t -> Replacement.t -> t -> t * (Solution.t list)

(** Adds a provided individual to the given population *)
val add : t -> Solution.t -> t

(** Permanently destroys all individuals belonging to a given population *)
val destroy : t -> unit
