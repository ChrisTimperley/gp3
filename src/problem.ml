open Debug
open Utility
open Test

(**
 * This data structure is used to hold information about the problem being
 * solved, i.e. details about the program under repair.
 *)
type problem =
  {
    manifest            : Program.Manifest.t;
    program             : Program.t;

    (** A flag indicating whether the problem should be treated as if it were
     *  a multi-file problem. If this is the case, candidates won't be
     *  executed in parallel, and candidates will be decompiled at the end of
     *  their batch job. *)
    multi_file          : bool;

    (** A flag indicating whether failed sanity checks should be ignored; if
     *  so, all failing tests will be removed from the problem. *)
    ignore_failed_sanity : bool;

    (** The name of this problem *)
    name                : string;

    (** The path to the results cache file used by this problem *)
    cache_file_location : string;

    (** The name of the executable that the program should compile to. *)
    executable_name     : string;

    (** The set of all positive test cases within the test suite for this problem. *)
    positive_tests      : TestSet.t;

    (** The set of all the negative test cases within the test suite for this problem. *)
    negative_tests      : TestSet.t;

    donor_pool          : DonorPool.t option;
    coverage            : Coverage.t option;
    localisation        : Localisation.t option;
    analysis            : Analysis.t option;
    compiler            : Program.Compiler.t;
    preprocessor        : Program.Preprocessor.t
  }

type t = problem

let positive_tests p = p.positive_tests
let num_positive_tests p = TestSet.cardinal p.positive_tests
let with_positive_tests p pos = { p with positive_tests = pos }

let negative_tests p = p.negative_tests
let num_negative_tests p = TestSet.cardinal p.negative_tests
let with_negative_tests p neg = { p with negative_tests = neg }

(** Returns the set of all tests for a given problem *)
let tests p = Test.TestSet.union (positive_tests p) (negative_tests p)

let program p = p.program
let compiler p = p.compiler
let preprocessor p = p.preprocessor
let executable_name p = p.executable_name
let multi_file p = p.multi_file
let ignore_failed_sanity p = p.ignore_failed_sanity
let name p = p.name
let descriptor p = p.name

let analysis p = match p.analysis with
  | Some(analy) -> analy
  | None -> failwith "No analysis for this problem exists"
let coverage p = match p.coverage with
  | Some(cov) -> cov
  | None -> failwith "No coverage information for this problem exists"
let localisation p = match p.localisation with
  | Some(info) -> info
  | None -> failwith "No localisation information for this problem exists."
let donor_pool p = match p.donor_pool with
  | Some(pool) -> pool
  | None -> failwith "No donor pool for this problem exists"

let has_coverage p = match p.coverage with
  | Some(_) -> true
  | None -> false

let stmts p = Analysis.stmts (analysis p)
let sids p = Analysis.sids (analysis p)
let size p = Analysis.size (analysis p)

(** Returns the number of statements executed by the negative test case for
 *  a given problem *)
let num_executed p = Localisation.num_executed (localisation p)
  
(** Returns the number of tests specified by a given problem *)
let num_tests p = (num_positive_tests p) + (num_negative_tests p)

(** Returns the location of the results cache file for this problem. *)
let results_cache_file_location p = p.cache_file_location

(** Returns a modified form of this problem, with coverage attached *)
let with_coverage p cov = { p with coverage = Some(cov) }

(** Returns a modified form of this program, with analysis attached *)
let with_analysis p analy = { p with analysis = Some(analy) }

(** Returns a modified form of this problem, with localisation attached *)
let with_localisation p loc = { p with localisation = Some(loc) }

(** Returns a modified form of this problem, with a donor pool attached *)
let with_donor_pool p pool = { p with donor_pool = Some(pool) }

(**
 * Constructs a problem from a set of provided parameters.
 * Performs a sanity check before returning the constructed problem.
 *)
let make
  (cfg : Config.t)
  (manifest_fn : string)
  (cache_fn : string option)
  (name : string)
  (exe_name : string)
  (multi_file : bool)
  (ignore_failed_sanity : bool)
  (n_positive : int)
  (n_negative : int)
  (compiler : Program.Compiler.t)
  (preprocessor : Program.Preprocessor.t)
: problem =
  debug "Building problem: %s\n" manifest_fn;
  let manifest = Program.Manifest.read cfg manifest_fn in
  let program = Program.load manifest in
  let positive_tests =
    TestSet.of_list (List.map (fun i -> Positive i) (1 -- n_positive))
  in
  let negative_tests =
    TestSet.of_list (List.map (fun i -> Negative i) (1 -- n_negative))
  in
  let cache_fn = match cache_fn with
    | Some(f) -> f
    | None -> (exe_name ^ ".results.cache")
  in
  (* make the path absolute, if it isn't already *)
  let cache_fn = match Filename.is_relative cache_fn with
    | true -> Filename.concat (Program.Manifest.base_path manifest) cache_fn
    | false -> cache_fn
  in
    { manifest = manifest;
      name = name;
      multi_file = multi_file;
      ignore_failed_sanity = ignore_failed_sanity;
      program = program;
      positive_tests = positive_tests;
      negative_tests = negative_tests;
      analysis = None;
      localisation = None;
      coverage = None;
      donor_pool = None;
      compiler = compiler;
      cache_file_location = cache_fn;
      preprocessor = preprocessor;
      executable_name = exe_name }

(** Loads a problem from a provided JSON description. *)
let from_json cfg def =
  debug "- constructing problem\n";
  let open Yojson.Basic.Util in
  let compiler =
    Program.Compiler.from_json cfg (member "compiler" def)
  in
  let preprocessor =
    Program.Preprocessor.from_json cfg (member "preprocessor" def)
  in
  let cache_fn = match def |> member "cache_file" with
    | `String(s) -> Some(s)
    | `Null -> None
    | _ -> failwith "Illegal problem.cache_file parameter supplied"
  in
  let prog = match def |> member "program" with
    | `String s -> s
    | _ -> failwith "No program file specified." 
  in
  let name = match def |> member "name" with
    | `String(s) -> s
    | _ -> failwith "No problem name specified."
  in
  let exe_name  = match def |> member "executable_name" with
    | `String s -> s
    | _ -> failwith "No executable name specified." 
  in
  let pos_tests = match def |> member "positive_tests" with
    | `Int n -> n
    | _ -> failwith "Number of positive tests not specified." 
  in
  let neg_tests = match def |> member "negative_tests" with
    | `Int n -> n
    | _ -> failwith "Number of positive tests not specified." 
  in
  let multi_file = match def |> member "multi_file" with
    | `Bool b -> b
    | _ -> true
  in
  let ignore_failed_sanity = match def |> member "ignore_failed_sanity" with
    | `Bool b -> b
    | _ -> false
  in
    make cfg prog cache_fn name exe_name multi_file ignore_failed_sanity pos_tests neg_tests compiler preprocessor
