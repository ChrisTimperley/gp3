open Utility

(** Underlying type used by results cache instances *)
type t

(** Ghost results caches ignore all requests and do not write to file. Useful
 *  in situations where caching is not desirable (e.g. crawling). *)
module Ghost : sig
  type z = t
  type t

  (** Creates a new Ghost cache *)
  val make : unit -> t

  val downcast : t -> z
end

(** Live results caches function as you would expect a results cache to;
 *  i.e. they respond to all requests and read and write to an associated cache
 *  file. *)
module Live : sig
  type z = t
  type t

  (** Generates and returns an empty cache, with an associated file name *)
  val empty : string -> t

  (** Creates a new cache from the contents of a specified file on disk; if that
   * file is does not exist, an empty cache is created instead. *)
  val load : string -> t

  val downcast : t -> z
end

(** Attempts to retrieve the results of a particular test case execution for
 *  a given candidate from a provided cache. *)
val search : t -> Config.t -> Problem.t -> Solution.t -> Test.test_case -> Test.test_case_result option

(** Adds the results for a given test case execution to the cache, overwriting
 *  any previous entry. *)
val store : t -> Config.t -> Problem.t -> Solution.t -> Test.test_case -> Test.test_case_result -> unit

(** Saves the contents of a given cache to an associated cache, if one exists *)
val save : t -> unit
