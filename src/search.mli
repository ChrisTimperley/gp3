(** Underlying type used by all search algorithms *)
type t

(** Runs a given search algorithm with a given RNG seed till termination *)
val run : t -> Problem.t -> Yojson.Basic.json -> int -> string option -> string option -> unit

(** Produces a descriptor for a given search algorithm setup *)
val descriptor : t -> string

(** Constructs a search algorithm from a JSON definition *)
val from_json : Config.t -> Problem.t -> ResultsCache.t -> int -> float option -> Yojson.Basic.json -> t
