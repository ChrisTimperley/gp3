open Utility
open Debug

(* Base class used by all replacement methods *)
class virtual base_cls = object (self)
  method virtual candidates : Solution.t list -> Solution.t list -> Solution.t list
  method virtual replace : Solution.t list -> Solution.t list -> Solution.t list * Solution.t list
  method virtual descriptor : string
end
type t = base_cls

(* Module type used by all replacement  methods *)
module type Scheme = sig
  type t
  val from_json : Yojson.Basic.json -> int -> base_cls
end

(** Replaces population with its offspring *)
module Generational : Scheme = struct
  class cls = object (self)
    inherit base_cls as super
    method descriptor = "generational"
    method candidates parents offspring = offspring
    method replace parents offspring = (offspring, parents)
  end
  type t = cls
  let make () = new cls 
  let from_json def pop_size = make ()
end

(** Adds the offspring of the population to the population itself; shifts
 *  selection to the process of selecting parents *)
module KeepBoth : Scheme = struct
  class cls = object (self)
    inherit base_cls as super
    method descriptor = "keep-both"
    method candidates parents offspring = parents @ offspring
    method replace parents offspring = (parents @ offspring, [])
  end
  type t = cls
  let make () = new cls
  let from_json def pop_size = make ()
end

(** Replaces the population with its mu best offspring *)
module MuCommaLambda : Scheme = struct
  class cls mu = object (self)
    inherit base_cls as super
    method descriptor = "mu-comma-lambda"
    method candidates _ offspring = offspring
    method replace parents offspring =
      let surviving = shuffle offspring in
      let surviving = List.sort Solution.compare surviving in
      let (surviving, killed) = first_nth_with_rest surviving mu in
      let killed = parents @ killed in
        (surviving, killed)
  end
  type t = cls
  let make pop_size = new cls pop_size
  let from_json def pop_size = make pop_size
end

(** Replaces the population with the mu best individuals taken from the union
 *  of itself with its offspring *)
module MuPlusLambda : Scheme = struct
  class cls mu = object (self)
    inherit base_cls as super
    method descriptor = "mu-plus-lambda"
    method candidates parents offspring = parents @ offspring
    method replace parents offspring =
      let surviving = parents @ offspring in
      let surviving = shuffle surviving in
      let surviving = List.sort Solution.compare surviving in
      let (surviving, killed) = first_nth_with_rest surviving mu in
        (surviving, killed)
  end
  type t = cls
  let make pop_size = new cls pop_size
  let from_json def pop_size = make pop_size
end

let candidates repl parents offspring =
  repl#candidates parents offspring

let replace repl parents offspring =
  repl#replace parents offspring

let descriptor repl = repl#descriptor

let from_json pop_size def =
  debug "-- constructing replacement operator\n";
  let open Yojson.Basic.Util in
  match def with
  | `Null -> Generational.from_json def pop_size
  | _ -> begin
    match member "type" def with
    | `String("mu_plus_lambda") ->
        MuPlusLambda.from_json def pop_size
    | `String("mu_comma_lambda") ->
        MuCommaLambda.from_json def pop_size
    | `String("keep_all") ->
        KeepBoth.from_json def pop_size
    | `String("generational") ->
      Generational.from_json def pop_size
    | _ ->
        failwith "Unrecognised replacement scheme in definition"
  end
