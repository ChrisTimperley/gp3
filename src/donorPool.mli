type t

(** Constructs a new donor pool *)
val build : Analysis.t -> Coverage.t -> bool -> bool -> (int * int) option -> t

(** Constructs a new donor pool from a JSON description *)
val from_json : Analysis.t -> Coverage.t -> Yojson.Basic.json -> t

(** Returns a list of the SIDs of the statements belonging to this donor pool *)
val contents : t -> int list
