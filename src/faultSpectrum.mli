type t

(** Used to store details for a particular statement in the program *)
module Row : sig
  type t
  val to_json : t -> Yojson.Basic.json
end

(** Used to implement different suspiciousness metrics *)
module Metric : sig
  type t = GenProg | Tarantula | Ample | Jaccard

  (** Computes a suspiciousness value from a fault spectrum entry using a
   *  given metric *)
  val suspiciousness : t -> Row.t -> float
end

(** Builds the fault spectrum for a program from its coverage and test suite *)
val build : Program.t -> Coverage.t -> Test.TestSet.t -> t

(** Attempts to return the row for a statement with a given SID from the
 *  provided fault spectrum *)
val row : t -> int -> Row.t

(** Transforms a fault spectrum into a JSON object *)
val to_json : t -> Yojson.Basic.json
