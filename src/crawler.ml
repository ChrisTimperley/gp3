open Utility

(** Used as an interface to the more search-algorithm centric Evaluator
 *  module. Evaluates a given edit on its minimal test suite and returns its
 *  results. *)
module EditEvaluator = struct
  type t = {
    cfg       : Config.t;
    stats     : Test.Statistics.t;
    problem   : Problem.t;
    coverage  : Coverage.t;
    tests     : Test.test_case list;
    evaluator : Evaluator.t;
  }

  let make cfg problem threads coverage cache =
    let tests = Test.TestSet.elements (Problem.tests problem) in
    let evaluator =
      Evaluator.make cfg problem None threads false false false false cache
    in
      { cfg = cfg;
        stats = Test.Statistics.make (Problem.tests problem);
        problem = problem;
        coverage = coverage;
        tests = tests;
        evaluator = evaluator }

  let evaluate ev edit =
    let solution = Solution.new_solution (Representation.Patch([edit])) [] in
    let _ =
      Evaluator.evaluate ev.evaluator ev.stats [(solution, ev.tests)]
    in
    let res = Solution.to_json solution in
      Solution.destroy solution;
      res
end

let crawl cfg problem threads =
  let cache = ResultsCache.Ghost.make () in
  let donors = DonorPool.contents (Problem.donor_pool problem) in
  let analysis = Problem.analysis problem in
  let coverage = Problem.coverage problem in
  
  (** Find the SID of each of the statements executed by a failing test case *)
  let sids = Coverage.stmts_covered_by_failure coverage in
  let sids = IntSet.elements sids in

  (** Find the size of the search space *)
  Printf.printf "Computing number of possible edits...\n";
  let num_edits = List.fold_left (fun num sid ->
      num + (List.length (Analysis.viable_edits analysis sid donors))
    ) 0 sids
  in

  (** Extract the results for each edit, building the output JSON as we go along *)
  let count = ref 0 in
  let evaluator =
    EditEvaluator.make cfg problem threads coverage (ResultsCache.Ghost.downcast cache)
  in
  let open Yojson.Basic in
  let results =
    List.fold_left (fun results sid ->
      let results = List.fold_left (fun results edit ->
        let desc = Edit.to_s edit in
        let _ = count := (!count) + 1 in
        let _ = print_endline (Printf.sprintf "Evaluating: %s (%d/%d)" desc (!count) num_edits) in
          (desc, (EditEvaluator.evaluate evaluator edit))::results
      ) results (Analysis.viable_edits analysis sid donors) in
        results
    ) [] sids
  in
  (* Write the results to a file *)
  let results = `Assoc(results) in
  let out = open_out "fixes.json" in
    Yojson.Basic.pretty_to_channel out results; 
    close_out out;
    Printf.printf "Wrote fix analysis to fixes.json\n"
