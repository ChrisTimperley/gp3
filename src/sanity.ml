open Debug
open Utility

let check_exn cfg prob cache threads =

  (* Compile the original program to the sanity directory, if it hasn't been
   * compiled already. *)
  let prog = Problem.program prob in
  let compiler = Problem.compiler prob in
  let dir = Filename.concat (Config.dir cfg) "sanity" in
  if not (Sys.file_exists dir) then begin
    let _ = debug "compiling original program for sanity check.\n" in
    let _ = Unix.mkdir dir 0o755 in
    let (res, _) = Program.Compiler.compile compiler prog dir in
    if not res then begin
      rm_rf dir;
      failwith "Failed to build executable for original program (sanity).\n"
    end
  end;

  debug "compiled program for sanity check.\n";

  (* Perform sanity checking tests using a modified parallel evaluator *)
  let null_sol =
    Solution.new_solution (Representation.Patch([])) []
  in
  let ev =
    Evaluator.make cfg prob None threads false false false false cache
  in
  let tests = Problem.tests prob in
  let stats = Test.Statistics.make tests in
  let tests = Test.TestSet.elements tests in
  let _ =
    Evaluator.evaluate ev stats [(null_sol, tests)]
  in
  let results = Solution.results null_sol in

  (* Write the results to the cache file *)
  let _ = ResultsCache.save cache in

  (* Find all failed sanity checks *)
  let failed_positives = Test.TestSet.filter (fun t ->
    let test_name = Test.test_case_name t in
    match StringMap.find test_name results with
    | Test.Failed -> true
    | _ -> false
  ) (Problem.positive_tests prob) in
  let failed_negatives = Test.TestSet.filter (fun t ->
    let test_name = Test.test_case_name t in
    match StringMap.find test_name results with
    | Test.Passed -> true
    | _ -> false
  ) (Problem.negative_tests prob) in
  let failed_tests = Test.TestSet.union failed_negatives failed_positives in
  let failed_tests = Test.TestSet.elements failed_tests in
  let failed_tests = List.map Test.test_case_name failed_tests in

  if (Test.TestSet.is_empty failed_positives) && (Test.TestSet.is_empty failed_negatives) then
    prob
  else if Problem.ignore_failed_sanity prob then begin
    (* Compute the remaining positive and negative tests *)
    let remaining_positives =
      Test.TestSet.diff (Problem.positive_tests prob) failed_positives
    in
    let remaining_negatives =
      Test.TestSet.diff (Problem.negative_tests prob) failed_negatives
    in

    (* Warn the user about the removed tests *)
    let failed_tests =
      List.map (fun tn -> Printf.sprintf "WARNING: removed test case %s\n" tn) failed_tests
    in
    let _ = List.iter print_string failed_tests in

    (* Return a modified form of the problem, with the tests which fail
     * sanity checks removed *)
    let prob = Problem.with_positive_tests prob remaining_positives in
    let prob = Problem.with_negative_tests prob remaining_negatives in
      prob
  end else begin
    let failed_tests =
      List.map (fun tn -> Printf.sprintf "ERROR: failed sanity check %s\n" tn) failed_tests
    in
    let _ = List.iter print_string failed_tests in
      failwith "FATAL ERROR: failed sanity checking stage\n"
  end
