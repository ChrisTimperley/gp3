(** Batches are used to specify which test cases should be evaluated for a given
 *  candidate solution *)
module Batch =
struct
  type t = {
    solution : Solution.t;
    tests : Test.test_case list
  }

  let solution batch = batch.solution
  let tests batch = batch.tests
  let make solution tests =
    { solution = solution; tests = tests}
end

(** TODO: where should we do responsibility checking? *)

(** Jobs represent the execution of a particular candidate solution, test case pair *)
module Job =
struct
  type t = {
    solution : Solution.t;
    test : Test.test_case
  }

  let make solution test =
    { solution = solution; test = test }
end

module Queue =
struct
  type t = {
    (* flag indicating whether jobs from different batches may be run in
     * parallel *)
    multi_batch : bool;

    (* limit to the number of jobs that may be run in parallel *)
    job_limit : int;

    (* the number of jobs that are currently executing *)
    running : int;

    (* queue of jobs waiting to be executed *)
    jobs : Job.t list;

    (* queue of batches waiting to be processed *)
    batches : Batch.t list
  }

  (* checks the cache for the results of a job execution *)
  let check_cache job =

  (* starts the next job on the queue *)
  let start_next_job q =
    let (job::remaining_jobs) = q.jobs in

  (* starts processing the next batch in the queue *)
  let start_next_batch q =
    let (batch::remaining_batches) = q.batches in
    let solution = Batch.solution batch in
    let tests = Batch.tests batch in
    (* FILTER REDUNDANT TESTS *)
    let batch_jobs = List.map (fun t -> Job.make solution t) tests in

    (* compile the program - ONLY IF NECESSARY *)

  let run q =
    match (q.jobs, q.batches, q.running) with
    (* if there are no jobs or batches left, terminate *)
    | ([], []) -> ()

    (* if there are no jobs left on the queue, start the next batch *)
    | ([], _::_) -> start_next_batch q

    (* if there are jobs left on the queue, and we haven't hit our job limit, start another *)
    | (_::_, []) -> start_next_job q

    (* if the queue is full, *)
end

module Evaluator =
struct
  type t = {
    (* flag used to specify whether evaluation should cease upon first successful
     * solution *)
    terminate_early : bool;
  }

  (** Evaluates a sequence of batches using a given evaluator *)
  let evaluate ev stats batches =

end
