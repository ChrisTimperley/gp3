open Utility

type t

(** Used to hold function-specific analyses *)
module Function : sig
  type t

  (** Returns the VID of this function *)
  val id : t -> int

  (** Returns the name of the file this function belongs to *)
  val file_name : t -> string

  (** Returns the VIDs of the globals visible from this function *)
  val globals : t -> IntSet.t

  (** Returns the VIDs of the locals within this function *)
  val locals : t -> IntSet.t

  (** Returns the VIDs of all variables visible within this function *)
  val visible_vars : t -> IntSet.t

  (** Returns the CIL declaration of this function *)
  val dec : t -> Cil.fundec

  (** Returns a JSON description of this function analysis *)
  val to_json : t -> Yojson.Basic.json
end

(** Used to hold analyses for individual statements *)
module Stmt : sig
  type t

  (** Returns the SID of the associated statement *)
  val sid : t -> int

  (** Returns the name of the file that a given statement belongs to *)
  val file_name : t -> string

  (** Returns the location of the corresponding statement *)
  val location : t -> Cil.location

  (** Returns information about the function this statement belongs to *)
  val func_info : t -> Function.t

  (** Returns the statement that this analysis belongs to *)
  val stmt : t -> Cil.stmt

  (** Returns the number of the line that a statement appears on *)
  val line_no : t -> int

  (** Returns the CIL skind data for this statement *)
  val skind : t -> Cil.stmtkind

  (** Checks whether this statement is an empty statement *)
  val is_empty : t -> bool
  
  (** Checks whether this statement may be modified *)
  val is_modifiable : t -> bool

  (** Returns a set of SIDs of the statements live before this statement *)
  val live_before : t -> IntSet.t

  (** Returns a set of SIDs of the statements live after this statement *)
  val live_after : t -> IntSet.t

  (** Returns a set of the VIDs of the variables visible from this statement *)
  val visible_vars : t -> IntSet.t

  (** Checks whether two statements are syntatically equivalent *)
  val syntatically_equivalent : t -> t -> bool

  (** Returns a set of the VIDS of the variables used by this statement *)
  val used_vars : t -> IntSet.t

  (** Prints this statement to a string and returns it *)
  val to_s : t -> string

  (** Returns a JSON description of this statement analysis *)
  val to_json : t -> Yojson.Basic.json
end

(** Constructs an analysis of a given program using settings provided by a
 *  collection of flags *)
val build : Program.t -> bool -> bool -> bool -> bool -> bool -> bool -> bool -> t

(** Constructs an analysis of a given program, using settings provided by a
 *  JSON object *)
val from_json : Program.t -> Yojson.Basic.json -> t

(** Returns the statement at a given SID from the program associated with
 *  this analysis *)
val stmt : t -> int -> Cil.stmt

(** Returns a map containing information about each statement in the associated
 *  program, indexed by statement SID *)
val stmt_infos : t -> Stmt.t IntMap.t

(** Returns a list of the statements in the associated program *)
val stmts : t -> Cil.stmt list

(** Returns a sub-set of a statement info map, containing all the statements
 *  whose keys belong to a provided list of SIDs *)
val stmt_info_subset : Stmt.t IntMap.t -> int list -> Stmt.t IntMap.t

(** Returns a list of the SIDs of statements in the associated program *)
val sids : t -> int list

(** Returns the analysis of a statement, identified by its associated SID *)
val stmt_info : t -> int -> Stmt.t

(** Returns the analysis of a function, identified by its associated VID *)
val func_info : t -> int -> Function.t

(** Returns the CIL varinfo for a variable, identified by its VID *)
val var_info : t -> int -> Cil.varinfo

(** Returns the size of the analysed program, in terms of the number of statements *)
val size : t -> int

(** Returns the name of a variable with a given VID within this program *)
val var_name : t -> int -> string

(** Returns true if scope checking is enabled *)
val scope_checking : t -> bool

(** Returns true if syntatic equality checking is enabled *)
val syntatic_equality_checking : t -> bool

(** Returns true if empty statement checking is enabled *)
val empty_checking : t -> bool

(** Returns true if enclosing loop checking is enabled *)
val enclosing_loop_checking : t -> bool

(** Returns true if label checking is enabled *)
val label_checking : t -> bool

(** Returns true if untyped returns checking is enabled *)
val untyped_returns_checking : t -> bool

(**
 * Determines if a given statement can be inserted after another given
 * statement and still yield a valid solution (i.e., a C program that can be
 * compiled). Currently, the following checks are performed:
 *
 * (1) Do not insert a 'break' or 'continue' into a scope with no
 * enclosing while loops, because that yields an invalid C program that
 * gcc will reject (fitness 0). 
 *
 * (2) Do not insert 'label_X:' into a scope that already has 'label_X:'
 * because duplicate labels yield an invalid C program that gcc will
 * reject.  
 *
 * (3) If --ignore-untyped-returns is set, do not insert "return value;"
 * into a function with static return type void.
 *
 * (4) If --ignore-dead-code is set, do not insert "X=1" if "X" is dead
 * at the given location.
 *
 * The before parameter is used to specify whether the donor statement is to
 * be inserted before or after the destination statement. 
 *)
val can_insert_at_with : t -> int -> int -> bool -> bool

(**
 * Determines the atom at a specific SID within the given fault space can
 * be safely deleted without introducing (statically avoidable) semantic errors.
 *
 * Rules:
 * 1. Do not allow empty statements to be deleted; equivalent to original.
 *)
val can_delete_at : t -> int -> bool

(** Computes a list of viable replacements at a given SID. Added a rule to
 *  prevent the replacement of empty statements (equivalent to append). *)
val viable_replacements : t -> int -> int list -> int list

(** Computes a list of viable appends at a given SID from a list of options *)
val viable_appends : t -> int -> int list -> int list

(** Computes a list of viable edits at a given SID from a list of donor SIDS *)
val viable_edits : t -> int -> int list -> Edit.t list

(** Transforms the information contained within this analysis into a JSON
 *  object *)
val to_json : t -> Yojson.Basic.json
