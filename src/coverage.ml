open Utility
open Debug

(*type t = (int IntMap.t) Test.TestMap.t*)
type t = {
  (* describes the statements covered by each test *)
  test : (int IntMap.t) Test.TestMap.t;
  (* describes the tests covered by each statement *)
  statement : (int, Test.TestSet.t) Hashtbl.t
}

(** Reduces the coverage to include only the negative test cases *)
let negative_test_coverage test_cov =
  Test.TestMap.filter (fun t _ -> match t with
    | Test.Negative _ -> true
    | _ -> false
  ) test_cov

let read_file fn =
  debug "-- reading coverage file: %s\n" fn;

  (* Generate an empty set of coverage information using the provided map. *)
  let coverage = ref IntMap.empty in
  
  (* Used to process a single line in the coverage file. *)
  let process_line line_no line =
    try begin
      let sid = int_of_string line in
      let times_visited =
        if IntMap.mem sid !coverage then IntMap.find sid !coverage
        else 1
      in
      let times_visited = times_visited + 1 in
        coverage := IntMap.add sid times_visited !coverage
    end with Failure("int_of_string") -> begin
      debug "warning: failed to convert coverage line to an integer (%s:%d): %s\n" fn line_no line
    end
  in

  (* Open up the coverage file and process it using a line stream.
   * Ensures the channel is closed before returning the resulting coverage
   * information, or raising an exception. *)
  let chan = open_in fn in
  let line_no = ref 0 in
  let line_stream : string Stream.t = line_stream_of_channel chan in
  try
    Stream.iter (fun line ->
      incr line_no;
      process_line !line_no line
    ) line_stream;
    close_in chan;
    !coverage
  with e ->
    debug "-- coverage file reading failed at: %s:%d\n" fn !line_no;
    close_in chan;
    raise e

let is_executed cov sid =
  let tests = Hashtbl.find cov.statement sid in
    not (Test.TestSet.is_empty tests)

let is_executed_by_negative cov sid =
  let tests = Hashtbl.find cov.statement sid in
    Test.TestSet.exists (fun t ->
      match t with
      | Test.Positive(_) -> false
      | Test.Negative(_) -> true
    ) tests

let stmts_covered_by_failure cov =
  let faulty_stmts_test t_cov = 
    let faulty = IntMap.filter (fun _ n -> n > 0) t_cov in
    let faulty = List.map fst (IntMap.bindings faulty) in
      IntSet.of_list faulty
  in
  let cov = negative_test_coverage cov.test in
    Test.TestMap.fold (fun _ t_cov stmts ->
      IntSet.union stmts (faulty_stmts_test t_cov)
    ) cov IntSet.empty

let tests_covered_by_statement cov sid =
  Hashtbl.find cov.statement sid

let negative_tests_covered_by_statement cov sid =
  let all = tests_covered_by_statement cov sid in
  let (_, negative) = Test.partition (Test.TestSet.elements all) in
    Test.TestSet.of_list negative

let tests_covered_by_statements cov sids =
  List.fold_left (fun visited sid ->
    Test.TestSet.union visited (tests_covered_by_statement cov sid)
  ) (Test.TestSet.empty) sids

let stmts_covered_by_all_failures cov =
  (* compute the set of negative tests *)
  let negative_tests = List.map fst (Test.TestMap.bindings cov.test) in
  let (_, negative_tests) = Test.partition negative_tests in
  let negative_tests = Test.TestSet.of_list negative_tests in

  (* checks whether a statement with a given SID is covered by all failures *)
  let check_one sid =
    let visited = negative_tests_covered_by_statement cov sid in
      Test.TestSet.equal visited negative_tests
  in

  (* find which statements are covered by all failures *)
  let sids = Hashtbl.fold (fun sid _ sids ->
      if check_one sid then sid::sids else sids
    ) cov.statement []
  in
    Utility.IntSet.of_list sids

let visits info test sid =
  let test_cov = info.test in
  let test_cov = Test.TestMap.find test test_cov in
    (IntMap.mem sid test_cov) && (IntMap.find sid test_cov) >= 1

(* Finds the set of tests that are covered by each of the statements in the program *)
let build_statement_coverage test_coverage sids =
  let tests_covered_by_stmt sid =
    Test.TestMap.fold (fun test stmts_covered res ->
      if ((IntMap.mem sid stmts_covered) && (IntMap.find sid stmts_covered) >= 1) then
        Test.TestSet.add test res
      else
        res
    ) test_coverage (Test.TestSet.empty)
  in
  let num_stmts = List.length sids in
  let stmt_tests = Hashtbl.create num_stmts in
  let _ =
    List.iter (fun sid ->
      Hashtbl.add stmt_tests sid (tests_covered_by_stmt sid)
    ) sids
  in
    stmt_tests

let build cfg program tests exe_name compiler multi_threaded unique directory =
  (* Build the coverage directory, and a temporary coverage file *)
  let directory = abspath directory in
  let tmp_fn = Filename.concat directory "cov.tmp" in
  let _ = Utility.rm_f tmp_fn in
  let _ = Debug.debug "-- coverage temp file: %s\n" tmp_fn in

  (* instrument and compile the program *)
  let program = Instrument.instrument cfg program tmp_fn multi_threaded unique in
  let _ = Program.Compiler.compile compiler program directory in

  (* generate the test coverage *)
  let test_coverage = 
    Test.TestSet.fold (fun t info ->
      (* try to find an existing coverage file *)
      let cov_fn = Printf.sprintf "%s.cov" (Test.test_case_name t) in
      let cov_fn = Filename.concat directory cov_fn in
      if not (Sys.file_exists cov_fn) then begin
        let _ = touch tmp_fn in
        let _ = Debug.debug "-- running coverage for: %s\n" (Test.test_case_name t) in
        let (_, res) = Test.run t cfg exe_name directory in
        let _ = res () in
          Unix.rename tmp_fn cov_fn
      end;
      let cov = read_file cov_fn in
      let _ = Utility.rm_f tmp_fn in
        Test.TestMap.add t cov info
    ) tests Test.TestMap.empty
  in

  (* generate the statement coverage *)
  let sids = Program.sids program in
  let statement_coverage = build_statement_coverage test_coverage sids in

  (* put them both together *)
    { test = test_coverage; statement = statement_coverage }

let from_json cfg program tests exe_name compiler jsn =
  let open Yojson.Basic.Util in
  let jsn = match jsn with
    | `Null -> `Assoc([])
    | `Assoc(_) -> jsn
    | _ -> failwith "Illegal coverage parameter format"
  in
  let directory = match jsn |> member "directory" with
    | `String(s) -> s
    | `Null -> Filename.concat (Config.dir cfg) "coverage"
    | _ -> failwith "Illegal 'coverage.directory' parameter: expected string or blank"
  in
  let multi_threaded = match jsn |> member "multi_threaded" with
    | `Bool(b) -> b
    | `Null -> false
    | _ -> failwith "Illegal 'coverage.multi_threaded' parameter"
  in
  let unique = match jsn |> member "unique" with
    | `Bool(b) -> b
    | `Null -> true
    | _ -> failwith "Illegal 'coverage.unique' parameter"
  in
    build cfg program tests exe_name compiler multi_threaded unique directory
