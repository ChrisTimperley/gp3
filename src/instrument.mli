(** Instruments a given program to write its coverage information to a file at
 *  a given path. Non-destructive. *)
val instrument : Config.t -> Program.t -> string -> bool -> bool -> Program.t

(** Prepares the VA table look-up function, which is later used to instrument
 *  programs *)
val setup_va_table_function : Config.t -> Program.Preprocessor.t -> unit
