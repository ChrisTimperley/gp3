(**
 * This function is responsible for performing sanity checking for a given
 * problem, by executing each of the test cases within the test suite on the
 * unmodified program and ensuring they exhibit the correct behaviour (implied
 * by whether the test case is positive or negative).
 *)
val check_exn : Config.t -> Problem.t -> ResultsCache.t -> int -> Problem.t
