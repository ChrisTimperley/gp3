open Utility

module Row = struct
  type t = {
    mutable ep : int;
    mutable ef : int;
    mutable np : int;
    mutable nf : int;
  }
  let make ep ef np nf = { ep = ep; ef = ef; np = np; nf = nf }
  let to_json row = 
    `Assoc([
        ("ep", `Int(row.ep));
        ("np", `Int(row.np));
        ("ef", `Int(row.ef));
        ("nf", `Int(row.nf));
      ])
end

module Metric = struct
  type t = GenProg | Tarantula | Ample | Jaccard
  let suspiciousness metric row =
    let open Row in
    let (ep, np, ef, nf) =
      ((float row.ep), (float row.np), (float row.ef), (float row.nf))
    in
    match metric with
    | GenProg -> begin match (ep, ef) with
        | (_, 0.0) -> 0.0
        | (0.0, _) -> 1.0
        | _ -> 0.1
      end
    | Tarantula -> begin
        let numerator = ef /. (ef +. nf) in
        let denominator = (ep /. (ep +. np)) +. (ef /. (ef +. nf)) in
          numerator /. denominator
      end
    | Jaccard -> (ef /. (ef +. nf +. ep))
    | Ample -> 
        abs_float ((ef /. (ef +. nf)) -. (ep /. (ep +. np)))
end

type t = Row.t IntMap.t

let row spectrum sid = IntMap.find sid spectrum

let build program cov tests =
  let (positives, negatives) =
    Test.TestSet.fold (fun t (positives, negatives) ->
      match t with
      | Test.Positive _ ->
          ((Test.TestSet.add t positives), negatives)
      | Test.Negative _ ->
          (positives, (Test.TestSet.add t negatives))
    ) tests (Test.TestSet.empty, Test.TestSet.empty)
  in
  let num_pos = Test.TestSet.cardinal positives in
  let num_neg = Test.TestSet.cardinal negatives in
  let spectrum = List.fold_left (fun spectrum sid ->
    let ep =
      Test.TestSet.filter (fun t -> Coverage.visits cov t sid) positives
    in
    let ep = Test.TestSet.cardinal ep in
    let ef =
      Test.TestSet.filter (fun t -> Coverage.visits cov t sid) negatives
    in
    let ef = Test.TestSet.cardinal ef in
    let (np, nf) = (num_pos - ep, num_neg - ef) in
    let entry = Row.make ep ef np nf in
      IntMap.add sid entry spectrum
  ) (IntMap.empty) (Program.sids program) in
    spectrum

let to_json s =
  let open Yojson.Basic.Util in
  let entries = IntMap.fold (fun sid entry entries ->
      ((string_of_int sid), (Row.to_json entry))::entries
  ) s []
  in
    `Assoc(entries) 
