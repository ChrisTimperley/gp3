(**
 * - Separate offspring and population in Genetic logger
 *)
open Debug
open Config
open Utility
open Solution
open Problem
open Representation
open PatchRepresentation
open Termination

module Rand = Random

(** Container for storing ideal solutions found during search *)
module IdealSolutions = struct
  type t = problem * StringSet.t

  let make problem = (problem, StringSet.empty)

  let update container candidates =
    let (prob, ideals) = container in
    let candidates = List.filter (Solution.is_ideal prob) candidates in
    let candidates = List.map (Solution.describe_genome) candidates in
    let candidates = StringSet.of_list candidates in
    let ideals = StringSet.union candidates ideals in
      (prob, ideals)

  let add container candidate =
    let (prob, ideals) = container in
    let candidate = Solution.describe_genome candidate in
    let ideals = StringSet.add candidate ideals in
      (prob, ideals)

  let to_json container =
    let (_, ideals) = container in
    let ideals = StringSet.elements ideals in
      `List(List.map (fun s -> `String(s)) ideals)
end

(* Base class used by all search algorithms *)
class virtual base_cls = object (self)
  method virtual run : Test.Statistics.t -> Yojson.Basic.json * IdealSolutions.t * Test.Statistics.t
  method virtual descriptor : string
end

(** Underlying type of all search algorithms *)
type t = base_cls

(* Module type used by all search algorithms *)
module type Scheme = sig
  type t
  val from_json : Config.t -> Problem.t -> ResultsCache.t -> int -> float option -> Yojson.Basic.json -> base_cls
end

(** *)
module Exhaustive : Scheme = struct
  class cls cfg problem tests cache ev tlimit = object(self)
    inherit base_cls as super

    (** Executes the search until termination *)
    method private step edits it stats ideals time_start =
      let running_time = (Sys.time ()) -. time_start in
      match (edits, tlimit, running_time) with
      (* no edits left *)
      | ([], _, _) ->
          (it, Termination.ExhaustedEdits, ideals, stats)

      (* check time limit *)
      | (_, Some(secs), running_time) when running_time > secs ->
          (it, Termination.ReachedTimeLimit, ideals, stats)
          
      (* evaluate next individual *)
      | (nxt::rst, _, _) -> begin
        let mutant = Solution.make (Patch([nxt])) in
        let _ = debug "- evaluating [%d]: %s\n" it (describe_genome mutant) in
        let (termination, stats) =
          Evaluator.evaluate ev stats [(mutant, tests)]
        in
        let _ = Solution.destroy mutant in
        match Solution.is_ideal problem mutant with
        | true ->
            let ideals = IdealSolutions.add ideals mutant in
              (it, (Termination.FoundAcceptableSolution mutant), ideals, stats)
        | false -> self#step rst (it + 1) stats ideals time_start
      end

    method descriptor = "exhaustive"

    method private summarise it termination : Yojson.Basic.json =
        `Assoc([
          ("iterations",  `Int(it));
          ("termination", (Termination.to_json termination));
          ("evaluations", `Int(Evaluator.evaluations ev));
          ("evaluator",   (Evaluator.summarise ev));
        ])

    (** Executes this search algorithm till termination *)
    method run stats =
      (* calculate the edits *)
      let localisation = Problem.localisation problem in
      let analysis = Problem.analysis problem in
      let donors = DonorPool.contents (Problem.donor_pool problem) in
      let sids = Localisation.eligible localisation in
      let _ = debug "-- fault localisation: %d statements\n" (List.length sids) in
      let edits =
        List.fold_left (fun edits sid -> edits @ (Analysis.viable_edits analysis sid donors)) [] sids
      in
      let edits = Utility.shuffle edits in 
      let _ = debug "-- fix space: %d fixes\n" (List.length edits) in

      (* run! *)
      let ideals = IdealSolutions.make problem in
      let _ = Evaluator.reset ev in
      let time_start = Sys.time () in
      let (it, termination, ideals, stats) =
        self#step edits 0 stats ideals time_start
      in
        ResultsCache.save cache;
        debug "Terminating search: %s\n" (Termination.to_s termination);
        ((self#summarise it termination), ideals, stats)
  end

  type t = cls

  let make cfg problem cache threads tlimit =
    let tests = Test.TestSet.elements (Problem.tests problem) in
    let ev = Evaluator.make cfg problem None threads true true true false cache in
      new cls cfg problem tests cache ev tlimit

  let from_json cfg problem cache threads tlimit def : base_cls =
    Debug.debug "- constructing exhaustive searcher\n";
      make cfg problem cache threads tlimit
end

(** Random searcher generates solutions through random walks *) 
module Random : Scheme = struct

  (** Used to perform logging for random search *)
  module Logger = struct
    type t = Yojson.Basic.json list

    let empty () = []

    let log_iteration log it candidate =
      (Solution.to_json candidate)::log
      
    let to_json log = `List(List.rev log)
  end

  class cls cfg problem tests cache null_sol ev mut iterations pstep dlimit tlimit desc = object(self)
    inherit base_cls as super

    (** Generates mutant *)
    method private generate_mutant =
      let rec mutate mutant size =
        if (size < dlimit) && ((Random.float 1.0) <= pstep) then
          mutate (mut mutant) (size + 1)
        else
          mutant
      in
      (* create a single edit mutant *)
      let mutant = mut (copy null_sol) in
        mutate mutant 1

    (** Executes the search until termination *)
    method private step it ideals log stats time_start =
      let running_time = (Sys.time ()) -. time_start in
      match (iterations, tlimit, running_time) with
      (* check time limit *)
      | (_, Some(secs), running_time) when running_time > secs ->
          (it, ReachedTimeLimit, ideals, log, stats)
         
      (* check iteration limit *)
      | (Some(lim), _, _) when lim = it ->
          (it, ReachedIterationLimit, ideals, log, stats)

      (* evaluate next individual *)
      | _ -> begin
        let mutant = self#generate_mutant in
        let (termination, stats) =
          Evaluator.evaluate ev stats [(mutant, tests)]
        in
        let log = Logger.log_iteration log it mutant in
        let ideals = IdealSolutions.update ideals [mutant] in
          debug "- evaluated [%d]: %s\n" it (describe_genome mutant); (* WRITE TO LOG *)
          Solution.destroy mutant;
          match termination with 
          | Some(t) -> (it + 1, t, ideals, log, stats)
          | None -> self#step (it + 1) ideals log stats time_start
      end

    method descriptor = desc

    method private summarise it termination log : Yojson.Basic.json =
      `Assoc([
        ("iterations",  `Int(it));
        ("termination", (Termination.to_json termination));
        ("evaluations", `Int(Evaluator.evaluations ev));
        ("evaluator",   (Evaluator.summarise ev));
        ("log",         (Logger.to_json log))
      ])

    (** Executes this search algorithm till termination *)
    method run stats =
      Evaluator.reset ev;
        let ideals = IdealSolutions.make problem in
      let time_start = Sys.time () in
      let (it, termination, ideals, log, stats) =
        self#step 0 ideals (Logger.empty ()) stats time_start
      in
        ResultsCache.save cache;
        debug "Terminating search: %s\n" (Termination.to_s termination);
        ((self#summarise it termination log), ideals, stats)
  end

  type t = cls

  (* TODO: Ensure the algorithm can actually terminate *)
  let make cfg problem cache terminate_early evaluations iterations threads pstep dlimit tlimit mut desc =
    let null_sol = Solution.make (Patch([])) in
    let mut sol = List.hd (mut [sol]) in (* convert pop to individual mutator *)
    let tests = Test.TestSet.elements (Problem.tests problem) in
    let ev = Evaluator.make cfg problem evaluations threads terminate_early true true false cache in
      new cls cfg problem tests cache null_sol ev mut iterations pstep dlimit tlimit desc

  let from_json cfg problem cache threads tlimit def : base_cls =
    Debug.debug "- constructing random searcher\n";
    let open Yojson.Basic.Util in
    let terminate_early = match def |> member "terminate_early" with
      | `Bool b -> b
      | _ -> true
    in
    let dlimit = match def |> member "depth_limit" with
      | `Int(d) -> d
      | `Null -> 1
      | _ -> failwith "Illegal depth_limit parameter: expected an int"
    in
    let pstep = match def |> member "prob_walk" with
      | `Float p -> p
      | _ -> 0.5
    in
    let mut = def |> member "mutation" |>
      Mutation.from_json cfg problem
    in
    let evaluations = match def |> member "evaluations" with
      | `Int(lim) -> Some(lim)
      | _ -> None
    in
    let iterations = match def |> member "iterations" with
      | `Int(s) -> Some(s)
      | _ -> None
    in
    let desc = match def |> member "descriptor" with
      | `String(s) -> s
      | `Null -> "random"
      | _ -> failwith "Illegal descriptor parameter: expected a string"
    in
      make cfg problem cache terminate_early evaluations iterations threads pstep dlimit tlimit mut desc
end

(*****************************************************************************)

module Greedy : Scheme = struct

  module SearchPool = struct
    module StmtSearchPool = struct
      type t = {
        sid : int;
        mutable delete : bool;
        mutable replace : int list;
        mutable append : int list
      }

      let make problem sid =
        let analysis = Problem.analysis problem in
        let donors = Problem.donor_pool problem in
        let donors = DonorPool.contents donors in
        let delete = Analysis.can_delete_at analysis sid in
        let replace = Analysis.viable_replacements analysis sid donors in
        let append = Analysis.viable_appends analysis sid donors in
          { sid = sid;
            delete = delete;
            replace = (shuffle replace);
            append = (shuffle append) }

      let is_empty pool =
        match (pool.append, pool.replace, pool.delete) with
        | ([], [], false) -> true
        | _ -> false

      let sample pool =
        let typ = [] in
        let typ = match pool.delete with
          | false -> typ
          | true -> (Edit.Kind.Delete)::typ
        in
        let typ = match pool.replace with
          | [] -> typ
          | _ -> (Edit.Kind.Replace)::typ
        in
        let typ = match pool.append with
          | [] -> typ
          | _ -> (Edit.Kind.Append)::typ
        in
        let typ = sample typ in
        match typ with
          | Edit.Kind.Delete -> begin
              pool.delete <- false;
              Edit.Delete(pool.sid)
          end
          | Edit.Kind.Replace -> begin
              let donor::rst = pool.replace in
                pool.replace <- rst;
                Edit.Replace(pool.sid, donor)
          end
          | Edit.Kind.Append -> begin
              let donor::rst = pool.append in
                pool.append <- rst;
                Edit.Insert(pool.sid, donor)
          end
    end

    type t = {
      mutable localisation: Localisation.t;
      mutable stmt_pools: StmtSearchPool.t IntMap.t;
    }

    let make problem =
      let localisation = Problem.localisation problem in
      let sids = Localisation.eligible localisation in
      let pool = List.fold_left (fun pool sid ->
        let stmt_pool = StmtSearchPool.make problem sid in
          IntMap.add sid stmt_pool pool
      ) (IntMap.empty) sids in
        {
          localisation = localisation;
          stmt_pools = pool
        }

    let is_empty pool =
      IntMap.is_empty pool.stmt_pools

    let sample pool =
      let sid = Localisation.sample pool.localisation in
      let stmt_pool = IntMap.find sid pool.stmt_pools in
      let edit = StmtSearchPool.sample stmt_pool in

      let (localisation, pools) =
        if (StmtSearchPool.is_empty stmt_pool) then
          let pools = IntMap.remove sid pool.stmt_pools in
          let localisation = Localisation.remove pool.localisation sid in
            (localisation, pools)
        else
          (pool.localisation, pool.stmt_pools)
      in
        pool.localisation <- localisation;
        pool.stmt_pools <- pools;
        edit
  end

  module PartialSolution = struct
    type t = {
      edits : Edit.t list;
      results : bool array
    }

    let edits partial = partial.edits
    let edits_string partial =
      let str = List.map Edit.to_s partial.edits in
        String.concat "; " str

    let size partial = List.length partial.edits

    let results partial = partial.results
    let results_string partial =
      let str = Array.to_list (results partial) in
      let str = List.map (fun r -> if r then "1" else "0") str in
        String.concat "" str

    let to_json partial =
      `Assoc([("results", `String(results_string partial));
              ("patch", `String(edits_string partial))])

    let sids partial =
      List.fold_left (fun sids e ->
        IntSet.add (Edit.sid e) sids
      ) (IntSet.empty) partial.edits

    let is_complete p =
      let n = Array.length p.results in
      let rec aux i =
        if i == n then true
        else if p.results.(i) then
          aux (i + 1)
        else false
      in
        aux 0

    let dominates p1 p2 =
      let n = Array.length p1.results in
      let rec aux i dominating =
        if i == n then dominating
        else match (p1.results.(i), p2.results.(i)) with
        | (true, true) | (false, false) -> aux (i + 1) dominating
        | (true, false) -> aux (i + 1) true
        | (false, true) -> false
      in
        aux 0 false

    let complements p1 p2 =
      let n = Array.length p1.results in
      let rec aux i =
        if i == n then false
        else if (p1.results.(i) && (not p2.results.(i))) then true
        else aux (i + 1)
      in
        aux 0

    let make prob sol = 
      let edits = Solution.edits sol in
      let results = Array.make (Problem.num_negative_tests prob) false in
      let _ = StringMap.iter (fun t r ->
        let t = Test.test_case_from_name t in
        match (t, r) with
        | (Test.Negative(tn), Test.Passed) ->
           Array.set results (tn - 1) true 
        | _ -> ()
      ) (Solution.results sol) in
        { edits = edits; results = results }
  end

  (*
   * - (A, B) partials
   * - only keep partials that pass all tests in A and B
   *)
  module SearchSpace = struct
    type t = {
      size_limit : int option;
      problem : Problem.t;
      pool : SearchPool.t;
      mutable partials : PartialSolution.t list;
      mutable combinations : (PartialSolution.t * PartialSolution.t) list
    }

    let is_empty space =
      (SearchPool.is_empty space.pool) && space.combinations == []

    let combine space p1 =
      let p1_sids = PartialSolution.sids p1 in
      let p1_size = PartialSolution.size p1 in
      let candidates = space.partials in
      let candidates = match space.size_limit with
        | Some(k) -> 
            let k = k - p1_size in
              List.filter (fun p2 -> (PartialSolution.size p2) <= k) candidates
        | None -> candidates
      in
      let candidates = List.filter (fun p2 ->
        let p2_sids = PartialSolution.sids p2 in
        let e = IntSet.empty in
        let intr = IntSet.inter p1_sids p2_sids in
          intr == e
      ) candidates in
      let candidates = List.filter (fun p2 ->
        PartialSolution.complements p2 p1
      ) candidates in
        List.map (fun p2 -> (p1, p2)) candidates

    let has_combination space =
      space.combinations != []

    let next_combination space =
      match space.combinations with
      | [] -> failwith "no combinations in search space\n"
      | nxt::rst ->
          space.combinations <- rst;
          nxt

    let partials space = space.partials

    let add_partial space partial =
      let _ = debug "Partial: %s\n" (PartialSolution.results_string partial) in
      let partials = partial::space.partials in
        space.combinations <- (combine space partial) @ (space.combinations);
        space.partials <- partials

    let sample space =
      SearchPool.sample space.pool

    let make problem size_limit =
      { problem = problem;
        size_limit = size_limit;
        pool = (SearchPool.make problem);
        combinations = [];
        partials = [] }
  end

  class cls cfg prob cache threads dlimit tlimit = object(self)
    inherit base_cls as super

    method descriptor = "greedy"

    val negative_tests =
      Test.TestSet.elements (Problem.negative_tests prob)
    val num_negative_tests =
      Problem.num_negative_tests prob
    val negative_ev =
      Evaluator.make cfg prob None threads false true false false cache

    val positive_tests =
      Test.TestSet.elements (Problem.positive_tests prob)
    val positive_ev =
      Evaluator.make cfg prob None threads false true true false cache

    (** Produces a JSON document summarising the run *)
    method private summarise it termination partials time_start : Yojson.Basic.json =
      let time_end = Unix.gettimeofday () in
      let running_time = time_end -. time_start in
      let partials = List.map PartialSolution.to_json partials in
      let partials = `List(List.rev partials) in
        `Assoc([
          ("time",        `Float(running_time));
          ("iterations",  `Int(it));
          ("partials",    partials);
          ("termination", (Termination.to_json termination))
        ])

    (* Determines whether a given mutant passes all of the positive tests *)
    method private passes_positives mutant stats =
      let (_, _) =
        Evaluator.evaluate positive_ev stats [(mutant, positive_tests)]
      in
      let _ = Solution.destroy mutant in
        Solution.passes_all_positives mutant

    method private has_reached_time_limit time_start =
      match tlimit with
      | None -> false
      | Some(t) ->
          let running_time = (Unix.gettimeofday ()) -. time_start in
            running_time > t

    (** Executes the search until termination *)
    method private step space it stats time_start =
      (* check time limit *)
      if (self#has_reached_time_limit time_start) then
        (it, Termination.ReachedTimeLimit, stats)

      else if (SearchSpace.is_empty space) then
        (it, Termination.ExhaustedEdits, stats)

      (* evaluate next individual *)
      else if (SearchSpace.has_combination space) then begin
        let (p1, p2) = SearchSpace.next_combination space in
        let patch = Patch((PartialSolution.edits p1) @ (PartialSolution.edits p2)) in
        let mutant = Solution.make patch in
        let _ = debug "- evaluating [%d]: %s\n" it (describe_genome mutant) in

        (* find which negatives are passed by this combination *)
        let (_, stats) =
          Evaluator.evaluate negative_ev stats [(mutant, negative_tests)]
        in

        (* if the combination is greater or equal to the sum of its parts,
         * record it, provided it still passes all the positive tests *)
        let p12 = PartialSolution.make prob mutant in
        let dominates_p1 = PartialSolution.dominates p12 p1 in
        let dominates_p2 = PartialSolution.dominates p12 p2 in

        match (dominates_p1, dominates_p2) with
        | (true, true) when (self#passes_positives mutant stats) ->
            Solution.destroy mutant;
            if (PartialSolution.is_complete p12) then
                (it, (Termination.FoundAcceptableSolution mutant), stats)
            else begin
              let _ = SearchSpace.add_partial space p12 in
                self#step space (it + 1) stats time_start
            end
        | _ ->
            Solution.destroy mutant;
            self#step space (it + 1) stats time_start

      (* sample a single-edit patch *)
      end else begin
        let edit = SearchSpace.sample space in
        let mutant = Solution.make (Patch([edit])) in
        let _ = debug "- evaluating [%d]: %s\n" it (describe_genome mutant) in

        (* does this edit pass any negatives? *)
        let (_, stats) =
          Evaluator.evaluate negative_ev stats [(mutant, negative_tests)]
        in
        let num_negatives_passed = Solution.num_negatives_passed mutant in

        if num_negatives_passed == 0 then begin
          Solution.destroy mutant;
          (* debug "- doesn't pass any negatives; boring.\n"; *)
          self#step space (it + 1) stats time_start

        (* if it passes negatives, check it ain't overfitting *)
        end else begin
          let (_, stats) =
            Evaluator.evaluate positive_ev stats [(mutant, positive_tests)]
          in
          let partial = PartialSolution.make prob mutant in
          let _ = Solution.destroy mutant in
          if not (Solution.passes_all_positives mutant) then
            self#step space (it + 1) stats time_start 
          else if num_negatives_passed == num_negative_tests then begin
              (it, (Termination.FoundAcceptableSolution mutant), stats)
          end else begin
            let _ = SearchSpace.add_partial space partial in
              self#step space (it + 1) stats time_start
          end 
        end
      end

    method run stats =
      let pool = SearchSpace.make prob dlimit in
      let _ = debug "Beginning greedy search...\n" in
      let time_start = Unix.gettimeofday () in
      let (it, termination, stats) = 
        self#step pool 0 stats time_start
      in
      let partials = SearchSpace.partials pool in
      let ideals = IdealSolutions.make prob in
      let ideals = match termination with
        | FoundAcceptableSolution(s) -> IdealSolutions.add ideals s
        | _ -> ideals
      in
        debug "Terminating search: %s\n" (Termination.to_s termination);
        ResultsCache.save cache;
        debug "Finished search.\n";
        ((self#summarise it termination partials time_start), ideals, stats)
  end

  type t = cls
  let run alg = alg#run

  let make cfg prob cache threads dlimit tlimit =
    new cls cfg prob cache threads dlimit tlimit

  let from_json cfg prob cache threads tlimit def =
    let open Yojson.Basic.Util in
    let def = match def with
      | `Null -> `Assoc([])
      | _ -> def
    in
    let dlimit = match def |> member "depth_limit" with
      | `Int(i) -> Some(i)
      | `Null -> None
      | _ -> failwith "Illegal parameter 'depth_limit'"
    in
    debug "- constructing greedy searcher\n";
    make cfg prob cache threads dlimit tlimit
end

(*****************************************************************************)

module Genetic : Scheme = struct

  (** Used to perform logging for genetic algorithms *)
  module Logger = struct
    type t = Yojson.Basic.json list

    let empty () = []

    let log_initialisation log pop =
      debug "Logging initialisation...\n";
      let entry = `Assoc([
        ("population", (Population.to_json pop))
      ]) in
        entry::log

    let log_generation log gen pop offspring =
      debug "Logging generation %d...\n" gen;
      let entry = `Assoc([
        ("population", (Population.to_json pop));
        ("offspring", (Population.to_json offspring))
      ]) in
        entry::log
      
    let to_json log = `List(List.rev log)
  end

  class cls
    cfg cache size num_offspring max_gens problem encoded selector crossover mutator repl evaluator test_s fitness_s desc max_uvs tlimit
  = object(self)
    inherit base_cls as super

    method descriptor = desc    

    (** Evaluates a given list of solutions, returning a pair containing an
     *  optional termination event and a list of the tests used to perform
     *  the evaluation *)
    method private evaluate sols stats =
      (* use the test scheme to select a bunch of tests to use for all of the
       * solutions *)
      let tests = TestScheme.choose test_s stats in
      let sol_tests = List.map (fun s -> (s, tests)) sols in
      let (terminate, stats) = Evaluator.evaluate evaluator stats sol_tests in
        FitnessScheme.compute fitness_s sol_tests;
        (terminate, stats)

    method private initialise ideals log stats =

      (* Create a null solution representing the unmodified input program. *)
      let null_solution = Solution.make encoded in

      (* Generate initial population.
       * Fill up (n-1) slots of the population with copies of the null solution
       * before performing mutation on them. Fill the remaining slot with a
       * copy of the original, unmodified program. *)
      let pop = Population.clones null_solution (size - 1) in
      let pop = Population.mutate pop mutator in
      let pop = Population.add pop null_solution in
      debug "Generated initial population.\n";

      (* Evaluate the initial population *)
      let (termination, stats) =
        self#evaluate (Population.members pop) stats
      in
      let log = Logger.log_initialisation log pop in
      let ideals = IdealSolutions.update ideals (Population.members pop) in
        (pop, termination, ideals, log, stats)

    method private execute gen pop terminate ideals log stats time_start =
      let running_time = (Unix.gettimeofday ()) -. time_start in
      let num_visited = Evaluator.num_patches evaluator in
      let max_visits = match max_uvs with
        | Some(i) -> (string_of_int i)
        | None -> "Inf"
      in
      let _ = debug "Solutions evaluated: %d / %s\n" num_visited max_visits in 
      match (terminate, gen, max_gens, max_uvs, tlimit) with
      | (Some(t), _, _, _, _) ->
          (pop, gen, t, ideals, log, stats)
      | (_, _, _, Some(mxuv), _) when num_visited >= mxuv ->
          (pop, gen, ReachedUniqueVisitLimit, ideals, log, stats)
      | (_, _, Some(mxg), _,  _) when mxg = gen ->
          (pop, gen, ReachedGenerationLimit, ideals, log, stats)
      | (_, _, _, _, Some(t)) when running_time > t ->
          (pop, gen, ReachedTimeLimit, ideals, log, stats)
      | _ ->
        let _ = debug "Beginning generation %d...\n" gen in

        (* DEBUG THE POPULATION *)
        let _ = Population.debug pop in

        (* generate offspring *)
        let offspring = Population.select pop selector num_offspring in
        let offspring = Population.crossover offspring crossover in
        let offspring = Population.mutate offspring mutator in

        (* find candidates for survival to next generation; evaluate *)
        let candidates = 
          Replacement.candidates repl (Population.members pop) (Population.members offspring)
        in

        let (terminate, stats) = self#evaluate candidates stats in

        (* log generation, record ideals *)
        let log = Logger.log_generation log gen pop offspring in
        let ideals =
          IdealSolutions.update ideals (Population.members offspring)
        in

        (* perform replacement *)
        let (pop, killed) = match terminate with
          | Some(_) ->
              (pop, (Population.members offspring))
          | _ ->
              let (pop, killed) = Population.replace pop repl offspring in
                (*FitnessScheme.compute fitness_s (Population.members pop);*)
                (pop, killed)
        in

        (* Destroy discarded (unique) solutions; i.e. don't destroy an
         * individual compiled to the same location as a surviving solution *)
        let compilation_set = 
          List.fold_left (fun set ind ->
            match Solution.compiled_to ind with
            | None -> set
            | Some(loc) -> StringSet.add loc set
          ) StringSet.empty (Population.members pop)
        in
        let killed =
          List.filter (fun ind ->
            match Solution.compiled_to ind with
            | None -> true
            | Some(loc) -> StringSet.mem loc compilation_set
          ) killed
        in
        let _ = List.iter Solution.destroy killed in
        let _ = debug "End of generation %d\n" gen in

        (* Proceed to the next generation *)
          self#execute (gen + 1) pop terminate ideals log stats time_start

    (** Produces a JSON document summarising the run *)
    method private summarise gens termination log : Yojson.Basic.json =
      `Assoc([
        ("generations", `Int(gens));
        ("evaluations", `Int(Evaluator.evaluations evaluator));
        ("evaluator",   (Evaluator.summarise evaluator));
        ("termination", (Termination.to_json termination));
        ("log",         (Logger.to_json log))
      ])

    (** Runs the algorithm until the termination criteria have been met. *)
    method run stats =
      Evaluator.reset evaluator;
      debug "Beginning search...\n";
      let ideals = IdealSolutions.make problem in
      let time_start = Unix.gettimeofday () in
      let (pop, termination, ideals, log, stats) =
        self#initialise ideals (Logger.empty ()) stats
      in
      let (pop, gen, termination, ideals, log, stats) =
        self#execute 1 pop termination ideals log stats time_start
      in
        debug "Terminating search: %s\n" (Termination.to_s termination);
        Population.destroy pop;
        ResultsCache.save cache;
        debug "Finished search.\n";
        ((self#summarise gen termination log), ideals, stats)
  end

  type t = cls

  let run alg = alg#run

  let make cfg prob cache size offspring gens encoded sel crs mut repl ev fitness_s desc max_uvs tlimit =
    new cls cfg cache size offspring gens prob encoded sel crs mut repl ev fitness_s desc max_uvs tlimit

  let from_json cfg prob cache threads tlimit def : base_cls =
    debug "- constructing genetic algorithm searcher\n";
    let open Yojson.Basic.Util in
    let def = match def with
      | `Null -> `Assoc([])
      | _ -> def
    in
    let size = match def |> member "size" with
      | `Int(i) -> i
      | _ -> 40
    in
    let offspring = match def |> member "offspring" with
      | `Int(i) -> i
      | _ -> size
    in
    let max_uvs = match def |> member "unique_visits" with
      | `Int(i) -> Some(i)
      | `Null -> None
      | _ -> failwith "illegal parameter value 'unique_visits'"
    in
    let gens = match (tlimit, max_uvs, (def |> member "generations")) with
      | (Some(_), _, _) | (_, Some(_), _) | (_, _, `Int(0)) -> None
      | (None, None, `Int(i)) -> Some(i)
      | _ -> failwith "illegal parameter value 'generations'"
    in

    (* Build the components *)
    let sel = def |> member "selection" |> Selection.from_json in
    let mut = def |> member "mutation" |> Mutation.from_json cfg prob in
    let crs = def |> member "crossover" |> Crossover.from_json in
    let test_s = def |> member "test_scheme" |>
      TestScheme.from_json prob
    in
    let ev = def |> member "evaluation" |>
      Evaluator.from_json cfg prob cache threads
    in
    let repl = def |> member "replacement" |>
      Replacement.from_json size
    in
    let fitness_s = def |> member "fitness_scheme" |>
      FitnessScheme.from_json prob
    in
    let desc = match def |> member "descriptor" with
      | `String(s) -> s
      | `Null -> "genetic"
      | _ -> failwith "Illegal descriptor provided: expected a string."
    in
    let encoded = Patch([]) in
      make cfg prob cache size offspring gens encoded sel crs mut repl ev test_s fitness_s desc max_uvs tlimit
end

let descriptor alg = alg#descriptor

let run alg problem definition seed dest_dir dest_file =

  (* Generate the statistics object for this run *)
  let stats = Test.Statistics.make (Problem.tests problem) in

  (* Seed the RNG and execute the algorithm *)
  let _ = Rand.init seed in
  let time_start = Unix.gettimeofday () in
  let (summary, ideals, stats) = alg#run stats in
  let time_end = Unix.gettimeofday () in
  let time_running = time_end -. time_start in

  (* Generate the JSON summary *)
  let timing = `Assoc([
      ("start", `Float(time_start));
      ("end", `Float(time_end));
      ("running", `Float(time_running))
    ])
  in
  let stats = Test.Statistics.to_json stats in
  let summary = match summary with
    | `Assoc(items) -> `Assoc([
        ("descriptor", `String((descriptor alg)));
        ("timing", timing);
        ("test_statistics", stats);
        ("seed", `Int(seed));
        ("ideal_solutions", IdealSolutions.to_json ideals);
        ("setup", definition)
      ] @ items)
    | _ -> failwith "Failed to generate a valid search summary.\n"
  in

  (* Determine the summary file output *)
  let dest_dir = match dest_dir with
    | None -> "./"
    | Some(dir) -> dir
  in
  let dest_file = match dest_file with
    | None ->
        (Problem.descriptor problem) ^ "_"
        ^ (descriptor alg) ^
        (Printf.sprintf ".%d.summary.json" seed)
    | Some(f) -> f
  in
  let dest_file = Filename.concat dest_dir dest_file in

  (* Write summary to file *)
  let _ = debug "writing summary to: %s\n" dest_file in
  let chan = open_out dest_file in
    Yojson.Basic.pretty_to_channel chan summary;
    close_out chan

let from_json cfg problem cache threads tlimit def =
  debug "- constructing searcher\n";
  let open Yojson.Basic.Util in
  match def with
  | `Assoc(_) -> begin
      match member "type" def with
      | `String("genetic") -> Genetic.from_json cfg problem cache threads tlimit def
      | `String("random") -> Random.from_json cfg problem cache threads tlimit def
      | `String("exhaustive") -> Exhaustive.from_json cfg problem cache threads tlimit def
      | `String("greedy") -> Greedy.from_json cfg problem cache threads tlimit def
      | _ -> failwith "Unrecognised algorithm type"
    end
  | `Null -> Genetic.from_json cfg problem cache threads tlimit def
  | _ -> failwith "Unrecognised algorithm property format"
