open Locate
open Utility
open Random
open Printf
open Problem
open Config
open Localisation
open PatchRepresentation
open Print

let () =
  Random.init 86715;
  if Array.length Sys.argv == 1 then
    printf "Error: No input repair target provided.\n"
  else if Array.length Sys.argv == 2 then
    printf "Error: No input patch file provided.\n"
  else if Array.length Sys.argv == 3 then
    printf "Error: No output directory provided.\n"
  else
    (* Use the default, global configuration. *)
    let cfg       = g_cfg in

    (* Read the command-line arguments. *)
    let file_list = Sys.argv.(1) in
    let patch_fn  = Sys.argv.(2) in
    let out_dir   = Sys.argv.(3) in

    (* Create fictional problem parameters. *)
    let pos_cases = 0 in
    let neg_cases = 0 in
    let exe_name = "test" in
    let fault_scheme = uniform_localisation in

    (* Build the problem. *)
    let prob =
      build_problem cfg file_list fault_scheme exe_name pos_cases neg_cases in
   
    (* Extract the program. *)
    let program = prob.fix_space.files in

    (* Read the patch file. *)
    let patch = load_patch_from_file patch_fn in

    (* Apply the patch to the program. *)
    let program = apply_patch patch program prob.fix_space.atoms in

      (* Print the program to the provided output directory. *)
      output_cil_files cfg out_dir program
