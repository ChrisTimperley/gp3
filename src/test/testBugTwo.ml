open Debug
open Locate
open Utility
open Random
open Printf
open Problem
open Config
open Localisation
open PatchRepresentation
open Representation
open Print
open FaultSpace
open FixSpace
open Locate

let () =
  Random.init 86715;
 
  (* Program and fix files. *)
  let file_list = "bugTwoFiles/gcd.c" in
  let patch_fn = "bugTwoFiles/p.patch" in 
  
  (* The fix that produces the bug. *)
  let bug_address   = [0;0;5;0;0;1;4] in
  let bug_sid       = 13 in
  let bug_fix_atom  = Replace(bug_address, bug_sid) in

  (* Use the default, global configuration. *)
  let cfg       = g_cfg in

  (* Create a test problem. *)
  let pos_cases = 10 in
  let neg_cases = 1 in
  let exe_name = "test" in
  let fault_scheme = uniform_localisation in
  debug "Building problem...\n";
  let prob =
    build_problem cfg file_list fault_scheme exe_name pos_cases neg_cases in
  debug "Built problem\n";

  (* Extract the fix space. *)
  let fix_space = prob.fix_space in

  (* Generate a patch representation from the patch file. *)
  let fixes = load_patch_from_file patch_fn in
  let patch : representation = Patch(fixes) in

  let fault_space = build_fault_space cfg prob patch in
  debug "Built fault space\n";

  (* How on earth did the bug fix ever get generated? *)
  (* Let's check the fault localisation information. *)
  let loc_info = fault_space.localisation in
  let loc_addresses, _ = List.split loc_info in

  (* Print the list of addresses. *)
  debug "\nFAULT SPACE ADDRESSES:\n%s\n"
    (String.concat "\n" (List.map string_of_address loc_addresses));

  (* Let's take a look at what resides at the bug address. *)
  debug "Attempting to locate atom at fault address...\n";
  let bug_atom = locate fault_space.files bug_address in
  debug "Found original atom at fault address\n";
  debug "Attempting to locate parent of atom at fault address...\n";
  let bug_parent = locate_parent fault_space.files bug_address in
  debug "Found parent of original atom at fault address\n";

  debug "Statement type: %s\n" (desc_stmt_type bug_atom);
  debug "%s\n" (cil_stmt_to_s bug_atom);

  debug "Parent type: %s\n" (desc_stmt_type bug_parent);
  debug "%s\n" (cil_stmt_to_s bug_parent);

  (* Append the generated fix and see if it actually works! *)
  let patch = List.rev (bug_fix_atom::(List.rev fixes)) in
  let patch_id = patch_to_short_string patch in
  assert (patch_id = "d 0.0.5.0.0.1;r 0.0.4.1 18;d 0.0.4.1.0;d 0.0.5.0.1;d 0.0.4.1;r 0.0.1 12;r 0.0.5.0.0.1.4 13");

  (* Try to build the program from the patch. *)
  let patched = apply_patch patch fix_space.files fix_space.atoms in
    ()
