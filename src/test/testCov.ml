open Spectrum
open Debug
open Locate
open Config
open Utility
open Printf
open Cil
open FixSpace 
open FaultSpace
open Problem
open Random
open Representation
open Localisation
open Instrument
open Test

let () =

  (* Build the problem. *)
  let cfg       = g_cfg in
  let file_list = "covFiles/gcd.c" in
  let exe_name  = "gcd" in
  let pos_cases = 10 in
  let neg_cases = 1 in
  let fault_scheme = uniform_localisation in

  (* Modify the location of the testing script. *)
  cfg.test_script <- "covFiles/test.sh";

  (* Build the problem. *)
  let prob =
    build_problem cfg file_list fault_scheme exe_name pos_cases neg_cases in
  let prog = prob.fix_space.files in

  (* Generate the coverage information. *)
  let coverage =
    compute_coverage ~f:true cfg prog "" exe_name pos_cases neg_cases 
  in
  debug "\nPrinting coverage information:\n\n%s\n"
    (describe_test_suite_coverage coverage);

  (* Build the fault localisation spectrum. *)
  debug "\nBuilding fault localisation spectrum...\n";
  let spectrum = compute_spectrum coverage in
  debug "\nAddress, Ep, Ef, Np, Nf\n"; 
  debug "%s\n" (describe_spectrum spectrum);

  (* Construct the localisation information using a spectrum-based technique. *)
  debug "\nBuilding fault localisation info (via Tarantula)...\n\n";
  let loc_info = spectrum_to_localisation spectrum similarity_tarantula in
  debug "%s\n" (describe_localisation_info loc_info)
