open Debug
open Utility
open Printf
open Config
open PatchRepresentation

let () =
  if Array.length Sys.argv == 1 then
    printf "Error: No input patch file provided.\n"
  else if Array.length Sys.argv == 2 then
    printf "Error: No fix index provided.\n"
  else
    (* Use the default, global configuration. *)
    let cfg       = g_cfg in

    (* Load the patch at the specified location. *)
    let patch_file = Sys.argv.(1) in
    let patch = load_patch_from_file patch_file in

    (* Print the patch in its current form. *)
    debug "ORIGINAL PATCH:\n\n%s\n\n" (patch_to_string patch);

    (* Print the patch after the undo operation. *)
    let index = int_of_string Sys.argv.(2) in
    let modified = undo_fix cfg patch index in 
    debug "MODIFIED PATCH:\n\n%s\n\n" (patch_to_string modified);
