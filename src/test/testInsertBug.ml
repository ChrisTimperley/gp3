open Locate
open Utility
open Random
open Printf
open Problem
open Config
open Localisation
open PatchRepresentation
open Representation
open Print
open FaultSpace
open FixSpace
open Locate

let () =
  Random.init 86715;

  (* The fix that produces the bug. *)
  let bug_address   = [0;0;5;0] in
  let bug_sid       = 22 in
  let bug_fix_atom  = Insert(bug_address, bug_sid) in

  (* Use the default, global configuration. *)
  let cfg       = g_cfg in

  (* Create a test problem. *)
  let file_list = "insertBugFiles/gcd.c" in
  let pos_cases = 10 in
  let neg_cases = 1 in
  let exe_name = "test" in
  let fault_scheme = uniform_localisation in
  let prob =
    build_problem cfg file_list fault_scheme exe_name pos_cases neg_cases in
 
  (* Extract the fix space. *)
  let fix_space = prob.fix_space in

  (* Generate a patch representation from the patch file. *)
  let fixes = load_patch_from_file "insertBugFiles/p.patch" in
  let patch : representation = Patch(fixes) in

  (* Build the fault space for the representation. *)
  let fault_space = build_fault_space cfg prob patch in

  (* Let's take a look at what resides at the bug address. *)
  let bug_atom = locate fault_space.files bug_address in
  let bug_parent = locate_parent fault_space.files bug_address in

  printf "Statement type: %s\n" (desc_stmt_type bug_atom);
  printf "%s\n" (cil_stmt_to_s bug_atom);

  printf "Parent type: %s\n" (desc_stmt_type bug_parent);
  printf "%s\n" (cil_stmt_to_s bug_parent);

  (* Ensure that the atom is not recognised as a block. *)
  assert (not (atom_is_a_block bug_atom));

  (* Find the viable insertion options at this address. *)
  let options =
    viable_insertions fault_space fix_space bug_address
  in
  assert (options <> []);

  (* Append the generated fix and see if it actually works! *)
  let patch = List.rev (bug_fix_atom::(List.rev fixes)) in
  let patch_id = patch_to_short_string patch in
  assert (patch_id = "r 0.0.5.0 2;r 0.0.2 18;i 0.0.5.0 22");

  (* Try to build the program from the patch. *)
  let patched = apply_patch patch fix_space.files fix_space.atoms in
    ()
