open Locate
open Utility
open Printf

let () =
  if Array.length Sys.argv = 1 then
    printf "Error: No query address provided.\n"
  else if Array.length Sys.argv = 2 then
    printf "Error: No parent address provided.\n"
  else if Array.length Sys.argv = 3 then
    printf "Error: No weakness parameter provided.\n"
  else
    let q_addr = address_of_string Sys.argv.(1) in
    let p_addr = address_of_string Sys.argv.(2) in
    let f_weak = Sys.argv.(3) = "true" in
    let s_weak = if f_weak then "weak" else "strong" in
      if is_sub_address q_addr p_addr f_weak then
        printf "%s is a (%s) sub-address of %s.\n"
          (string_of_address q_addr) (s_weak) (string_of_address p_addr)
      else
        printf "%s is not a (%s) sub-address of %s.\n"
          (string_of_address q_addr) (s_weak) (string_of_address p_addr)
