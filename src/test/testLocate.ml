open Locate
open Utility
open Random
open Printf
open Problem
open Config
open Localisation

let () =
  Random.init 86715;
  if Array.length Sys.argv == 1 then
    printf "Error: No input file provided.\n"
  else if Array.length Sys.argv == 2 then
    printf "Error: No address provided.\n"
  else
    (* Use the default, global configuration. *)
    let cfg       = g_cfg in

    (* Read the command-line arguments. *)
    let file_list = Sys.argv.(1) in
    let address   = address_of_string (Sys.argv.(2)) in

    (* Create fictional problem parameters. *)
    let pos_cases = 0 in
    let neg_cases = 0 in
    let exe_name = "test" in
    let fault_scheme = uniform_localisation in

    (* Build the problem. *)
    let prob =
      build_problem cfg file_list fault_scheme exe_name pos_cases neg_cases in
    
    (* Find the atom at the given location. *)
    let atom = locate prob.fix_space.files address in
      Printf.printf "Statement at address: %s\n" (cil_stmt_to_s atom)
