module Kind : sig
  type t =
    | Delete
    | Replace
    | Append
end

type t =
  | Delete  of int
  | Replace of int * int
  | Insert  of int * int

(** Returns the SID of the subject of the edit *)
val sid : t -> int

(** Returns a short string description of a given edit *)
val to_s : t -> string
