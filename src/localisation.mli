type t

(** Used to implement different localisation schemes *)
module Scheme : sig
  type k = t
  type t

  (** Uniform fault localisation assigns an equal weight to each (modifiable)
   *  statement in the program that is executed by a negative test case *)
  module Uniform : sig
    type z = t
    type t
    val downcast : t -> z
    val make : bool -> t
  end

  (** Spectrum-based fault localisation *)
  module Spectrum : sig
    type z = t
    type t
    val downcast : t -> z
    val make : FaultSpectrum.Metric.t -> bool -> t
  end

  (** Computes the fault localisation from a given coverage *)
  val compute : t -> Program.t -> Test.TestSet.t -> Coverage.t -> Analysis.t -> k
end

(** Transforms fault localisation information into a JSON object *)
val to_json : t -> Yojson.Basic.json

(** Generates the fault localisation for a given program using a provided
 *  localisation scheme, together with the coverage, analysis and test
 *  suite for the program *)
val build : Program.t -> Test.TestSet.t -> Coverage.t -> Analysis.t -> Scheme.t -> t

(** Generates the fault localisation for a given program, together with its
 *  associated coverage, analysis and test suite, using a fault localisation
 *  scheme described by a given JSON object. *)
val from_json : Program.t -> Test.TestSet.t -> Coverage.t -> Analysis.t -> Yojson.Basic.json -> t

(** Returns a variant of this fault localisation with a given statement
 *  removed from consideration *)
val remove : t -> int -> t

(** Returns a list of SIDs that are eligible for selection by this fault
 *  localisation *)
val eligible : t -> int list

(** Selects an SID from a given fault localisation at weighted random, based
 *  upon its implicit probability distribution. *)
val sample : t -> int

(** Returns the number of statements executed by a negative test *)
val num_executed : t -> int
