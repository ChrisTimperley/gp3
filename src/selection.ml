open Utility
open Debug

(* Base class used by all selection methods *)
class virtual base_cls = object (self)
  method virtual select : Solution.t list -> int -> Solution.t list
  method virtual descriptor : string
end
type t = base_cls

(** Selects a given number of individuals from a provided list, using a
 *  specified method *)
let select selector inds num =
  selector#select inds num
let __select = select

(**
 * Selects n solutions, with replacement, to serve as parents for the next
 * generation from a list of existing candidates.
 *
 * Performs n rounds of k-tournament to determine which candidates should be
 * selected.
 *)
module Tournament = struct
  let select inds num size =
    let rec aux buffer n = match n with
      | 0 -> buffer
      | _ ->
          aux (Solution.best_solution (sample_n inds size)::buffer) (n - 1)
    in
      aux [] num

  class cls size = object (self)
    inherit base_cls as super
    method descriptor = Printf.sprintf "select-tournament-%d" size
    method select inds num = select inds num size
  end

  type t = cls
  let make size = new cls size
  let from_json def =
    let open Yojson.Basic.Util in
    let def = match def with
      | `Null -> `Assoc([])
      | _ -> def
    in
    let size = match def |> member "size" with
      | `Int(sz) -> sz
      | _ -> 2
    in
      Debug.debug "--- building tournament selector\n";
      make size
  let downcast s : base_cls = s
end

(**
 * Selects only the individuals with the highest fitness within the population
 * to continue into the next generation.
 *)
module Cropping = struct
  class cls selector = object (self)
    inherit base_cls as super
    method descriptor = "select-cropping"

    method select inds num =
      (* returns a list of the best individuals within the population *)
      let rec crop q buffer =
        match (q, buffer) with
        | ([], _) -> buffer
        | (hd::tl, []) -> crop tl [hd]
        | (hd::tl, b1::_) -> begin
            match Solution.compare hd b1 with
            | 1 -> crop tl buffer
            | 0 -> crop tl (hd::buffer)
            | -1 -> crop tl [hd]
        end
      in
      let inds = crop inds [] in

      (* use the secondary selection mechanism to choose from amongst the
       * cropped individuals *)
        __select selector inds num
  end
  type t = cls
  let make selector =
    Debug.debug "--- building cropping selector\n";
    new cls selector
  let downcast s : base_cls = s
end

(**
 * Selects n solutions, with replacement, to serve as parents for the next
 * generation from a list of existing candidates.
 *
 * Performs n rounds of k-tournament to determine which candidates should be
 * selected.
 *)
module DoubleTournament = struct
  class cls k p = object (self)
    inherit base_cls as super
    method descriptor = Printf.sprintf "select-double-tournament-k%d-p%f" k p

    method select inds num =
      let parsimony_tournament () =
        let a = sample inds in
        let b = sample inds in
        let pick_small = ((p /. 2.0) < (Random.float 1.0)) in
          if (Solution.size a) < (Solution.size b) && pick_small then a
          else b
      in
      let select_one () =
          let rec aux buffer n = match n with
            | 0 -> buffer
            | _ -> aux ((parsimony_tournament ())::buffer) (n - 1)
          in
          let combatants = aux [] k in
            Solution.best_solution combatants
      in
      let rec aux buffer n = match n with
        | 0 -> buffer
        | _ ->
            aux ((select_one ())::buffer) (n - 1)
      in
        aux [] num
  end
  type t = cls
  let make k p = new cls k p
  let from_json def =
    let open Yojson.Basic.Util in
    let def = match def with
      | `Null -> `Assoc([])
      | _ -> def
    in
    let sp = match def |> member "sp" with
      | `Float(sp) -> sp
      | _ -> 1.5
    in
    let size = match def |> member "size" with
      | `Int(sz) -> sz
      | _ -> 2
    in
      Debug.debug "--- constructing double tournament (sp=%f; k=%d)\n" sp size;
      make size sp
  let downcast s : base_cls = s
end

module ParsimonyTournament = struct
  (** returns one of the individuals with the smallest size from a given
   *  list of individuals *)
  let smallest inds =
    let rec aux q sm = match q with
      | [] -> sm
      | hd::tl -> begin
        let sm =  if (Solution.size hd) < (Solution.size sm) then hd
                  else sm
        in
          aux tl sm
      end
    in
    match inds with
    | [] -> failwith "ParsimonyTournament#smallest: must be at least one individual to find smallest"
    | hd::tl -> aux tl hd
    
  let parsimony_tournament inds k =
    let inds = Utility.sample_n inds k in
      smallest inds

  class cls k = object (self)
    inherit base_cls as super
    method descriptor = "select-parsimony-tournament"
    method select inds num =
      let rec aux buffer num = match num with
        | 0 -> buffer
        | _ -> aux ((parsimony_tournament inds k)::buffer) (num - 1)
      in
        aux [] num
  end
  type t = cls
  let make k =
    Debug.debug "--- constructing parsimony tournament (k=%d)\n" k; 
    new cls k
  let from_json def =
    let open Yojson.Basic.Util in
    let size = match def |> member "size" with
      | `Int(i) -> i
      | `Null -> 2
      | _ -> failwith "Illegal size parameter: expected int or null"  
    in
      make size
  let downcast s : base_cls = s
end

(** Selects the first n solutions from the list of candidates, with or without
 *  ordering first; by default, ordered from best to worst *)
module Truncate = struct
  class cls ordered = object (self)
    inherit base_cls as super
    method descriptor = "truncate"
    method select inds num =
      let inds = if ordered then List.sort Solution.compare inds else inds in
        first_nth inds num
  end
  type t = cls
  let make ordered =
    Debug.debug "--- constructing truncate selection\n";
    new cls ordered
  let from_json def =
    let open Yojson.Basic.Util in
    let ordered = match def |> member "ordered" with
      | `Bool(b) -> b
      | _ -> true
    in
      make ordered
  let downcast s : base_cls = s
end

(**
 * Selects n solutions from a given list of candidates at random, with
 * replacement.
 *)
module RandomReplacement = struct
  let select inds num =
    let rec aux buffer n = match n with
      | 0 -> buffer
      | _ ->
          aux (sample inds::buffer) (n - 1)
    in
      aux [] num

  class cls = object (self)
    inherit base_cls as super
    method descriptor = Printf.sprintf "select-random"
    method select inds num = select inds num
  end

  type t = cls
  let make () =
    Debug.debug "--- constructing random replacement selection\n";
    new cls
  let from_json def = make ()
  let downcast s : base_cls = s
end

(** Selects from amongst the individuals that pass of their covering positive
 *  test cases at random, with replacement *)
module Neutral = struct
  class cls = object (self)
    inherit base_cls as super
    method descriptor = "select-neutral"
    method select inds num =
      let orig = Solution.make (Patch([])) in
      let inds = List.filter (fun i -> (Solution.passes_all_positives i)) inds in
      let inds = orig::inds in
        RandomReplacement.select inds num
  end
  type t = cls
  let make () = new cls
  let downcast s : base_cls = s
end

(** Performs a tournament selection on the *)
module NeutralNegative = struct

  (* hold a k-tournament on the number of negative test cases *)
  let select_one inds_negs =
    let (winner, mx)::inds_negs = inds_negs in
    let (winner, mx) = List.fold_left (fun (winner, mx) (ind, neg) ->
      if neg > mx then (ind, neg)
      else (winner, mx)
    ) (winner, mx) inds_negs in
      winner

  class cls orig size = object (self)
    inherit base_cls as super
    method descriptor = "select-neutral-negative"
    method select inds num =
      let inds = List.filter (fun i -> (Solution.passes_all_positives i)) inds in
      let inds_negs = List.map (fun i -> (i, (Solution.num_negatives_passed i))) inds in
      let inds_negs = (orig, 0)::inds_negs in
      let rec aux buffer n = match n with
        | 0 -> buffer
        | _ ->
            aux (select_one (sample_n inds_negs size)::buffer) (n - 1)
      in
        aux [] num
  end

  type t = cls
  let make size =
    let orig = Solution.make (Patch([])) in
      new cls orig size
  let from_json def =
    let open Yojson.Basic.Util in
    let size = match def |> member "size" with
      | `Int(k) -> k
      | _ -> 3
    in
      make size
 
  let downcast s : base_cls = s
end


let descriptor selector = selector#descriptor

let rec from_json def =
  debug "-- constructing selection method\n";
  let open Yojson.Basic.Util in
  let def = match def with
    | `Null -> `Assoc([])
    | _ -> def
  in
  match def |> member "type" with
  | `String("truncate") ->
      Truncate.downcast (Truncate.from_json def)
  | `String("random") ->
      RandomReplacement.downcast (RandomReplacement.make ())
  | `String("double_tournament") ->
      DoubleTournament.downcast (DoubleTournament.from_json def)
  | `String("parsimony_tournament") ->
      ParsimonyTournament.downcast (ParsimonyTournament.from_json def)
  | `String("neutral") ->
      Neutral.downcast (Neutral.make ())
  | `String("neutral_negative") ->
      NeutralNegative.downcast (NeutralNegative.from_json def)
  | `String "tournament" | `Null ->
      Tournament.downcast (Tournament.from_json def)
  | `String("cropping") -> begin
    let inner = match def |> member "selector" with
      | `Null -> RandomReplacement.downcast (RandomReplacement.make ())
      | x -> from_json x
    in
      Cropping.downcast (Cropping.make inner)
    end
  | _ -> failwith "unrecognised selector\n"
