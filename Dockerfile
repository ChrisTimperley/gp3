# Installs GenProg binary to /usr/local/genprog3
FROM ubuntu:16.04
MAINTAINER Chris Timperley "christimperley@googlemail.com"

# install OCaml, OPAM and m4
RUN apt-get update && \
    apt-get install -y opam build-essential jq m4 && \
    echo "yes" >> /tmp/yes.txt && \
    opam init -y < /tmp/yes.txt && \
    eval $(opam config env) &&  \
    opam install -y cil yojson
ADD src /tmp/genprog

# install GenProg, then remove OCaml and OPAM
ENV PATH "$PATH:/opt/genprog3"
RUN cd /tmp/genprog && \
    eval $(opam config env) &&  \
    make && \
    mkdir -p /opt/genprog3 && \
    mv genprog /opt/genprog3 && \
    apt-get purge -y opam ocaml m4 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
VOLUME /opt/genprog3
