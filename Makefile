all: build

build:
	docker build -t christimperley/genprog:latest .

push: build
	docker push christimperley/genprog:latest

test: build
	make -C test

.PHONY: build push test
