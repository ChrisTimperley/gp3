open Cil_types

(**
 * For each statement in the AST, calculate the impact of modifying that
 * statement on the rest of the program, i.e. the set of statements whose
 * result may be affected by a modification to the given statement.
 *)
class calculate_impact out = object
  inherit Visitor.frama_c_inplace

  method vstmt stmt =
    let impacted = (!Db.Impact.from_stmt) stmt in

    let impacted = List.length impact in
    let sid = stmt.sid in
      Printf.fprintf out "%d: %d\n" sid impacted;
      Cil.DoChildren
end

let run () =
  let ast = Ast.get () in
  (* Compute the impact for each statement in the program *)
  let chan = open_out "hello.out" in
    Visitor.visitFramacFileSameGlobals (new calculate_impact chan) ast;
    close_out chan

let () = Db.Main.extend run
