Configuration File Format
=========================

Top-Level Arguments
-------------------

* `seed`, gives the seed that should be used by the random number generator.
* `log_file`, specifies the location where the log file should be written to,
  relative to the directory of the log file (unless an absolute address is
  given); any existing log file at the given address will be destroyed. If no
  value is supplied, this argument will default to `run.log` within the current
  working directory.
* `test_script`, specifies the location of the testing script or executable used
  to evaluate a candidate program on a given test case, relative to the directory
  of the log file (unless an absolute address is provided). Defaults to `test.sh`
  if no value is provided.

    The testing script should accept the following arguments, in order:

    * *executable name*, the name of the executable file for the candidate
      under evaluation.
    * *test identifier*, the identifier for the test case that should be
      executed (e.g. p1, n2, p6).
    * *source location*, the root directory of the source files
      belonging to this executable.
    * *port number*, the port that the executable should use when executing,
      in order to avoid colliding with other candidates (and other programs
      running on the system).

    If the candidate program successfully passes the given test, the script
    should exit with status $0$. Otherwise the script should exit with any
    other status code (usually 1).

`problem` section
-----------------

* `program`, specifies the location of the source code file (or index file) for
  the program under repair, relative to the directory of the log file (unless
  an absolute address is given). Index files, ending in `.txt`, rather than
  `.c`, give a line separated list of all the source code files within the
  program, relative to the index file.
* `executable_name`, the name of the executable file produced by compiling the
  program.
* `positive_tests`, gives the number of positive test cases for the problem.
* `negative_tests`, gives the number of negative test cases for the problem.
* `localisation_scheme`, gives details of the fault localisation scheme that
  should be used by the searcher.
