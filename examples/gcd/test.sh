#!/bin/bash

# Retrieve and store the provided command-line arguments.
EXECUTABLE=$1
TEST_ID=$2

# Find the directory that this test script belongs to.
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Check if this test script is being used to compute coverage information.
if [ $(basename $1) = "coverage" ]; then
  cov=1
else
  cov=0
fi

# Find the inputs for the given test.
case $TEST_ID in
  p1) i1=1071; i2=1029;;
  p2) i1=555; i2=666;;
  p3) i1=678; i2=987;;
  p4) i1=8767; i2=653;;
  p5) i1=16777216; i2=512;;
  p6) i1=16; i2=4;;
  p7) i1=315; i2=831;;
  p8) i1=513332; i2=91583315;;
  p9) i1=112; i2=135;;
  p10) i1=310; i2=55;;
  n1) i1=0; i2=55;;
esac

# Execute the test.
if [ $cov = 0 ]; then
  timeout 1 $EXECUTABLE $i1 $i2 |& diff "$DIR/output.$i1.$i2" -
else
  timeout 5 $EXECUTABLE $i1 $i2 |& diff $(DIR)/output.$(i1).$(i2) -
fi

# Return the result of the execution.
exit $?
